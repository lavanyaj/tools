#include <stdio.h>

#include <rte_common.h>
#include <rte_log.h>
#include <rte_malloc.h>
#include <rte_memory.h>
#include <rte_memcpy.h>
#include <rte_memzone.h>
#include <rte_eal.h>
#include <rte_per_lcore.h>
#include <rte_launch.h>
#include <rte_atomic.h>
#include <rte_cycles.h>
#include <rte_prefetch.h>
#include <rte_lcore.h>
#include <rte_per_lcore.h>
#include <rte_branch_prediction.h>
#include <rte_interrupts.h>
#include <rte_pci.h>
#include <rte_random.h>
#include <rte_debug.h>
#include <rte_ether.h>
#include <rte_ethdev.h>
#include <rte_ring.h>
#include <rte_mempool.h>
#include <rte_mbuf.h>

#define NB_MBUF   8192

#define MAX_PKT_BURST 32

/*
 * Configurable number of RX/TX ring descriptors
 */

#define RTE_TEST_TX_DESC_DEFAULT 512
static uint16_t nb_txd = RTE_TEST_TX_DESC_DEFAULT;
#define RTE_TEST_RX_DESC_DEFAULT 128
static uint16_t nb_rxd = RTE_TEST_RX_DESC_DEFAULT;

static const struct rte_eth_conf port_conf = {
  .rxmode = {
    .split_hdr_size = 0,
    .header_split   = 0, /**< Header Split disabled */
    .hw_ip_checksum = 0, /**< IP checksum offload disabled */
    .hw_vlan_filter = 0, /**< VLAN filtering disabled */
    .jumbo_frame    = 0, /**< Jumbo Frame Support disabled */
    .hw_strip_crc   = 0, /**< CRC stripped by hardware */
  },
  .txmode = {
    .mq_mode = ETH_MQ_TX_NONE,
  },
};

struct rte_mempool * l2fwd_pktmbuf_pool = NULL;

/* ethernet addresses of ports */
static struct ether_addr l2fwd_ports_eth_addr;
static struct rte_eth_dev_tx_buffer *tx_buffer;

uint64_t dropped;
uint64_t tx;

static int launch_one_lcore_send_one_packet(__attribute__((unused)) void *dummy) {
  unsigned lcore_id;
  lcore_id = rte_lcore_id();
  
  int sent;
  unsigned portid = 0;
  sent = rte_eth_tx_buffer_flush(portid, 0, tx_buffer);
  if (sent) tx += sent;
  printf("core %u: sent %lu packets.\n", lcore_id, tx);
  return 0;
}

// example ./build/helloworld -c 1
// -c COREMASK
// optional --use-device 
int main(int argc, char **argv) {
  uint8_t nb_ports;
  uint8_t portid;
  unsigned lcore_id;
  size_t tx_malloc_size;
  int socket;

  tx = 0;
  int ret = 0;
  
  struct rte_eth_dev_info dev_info;
  
  ret = rte_eal_init(argc, argv);

  // use port 0, dev 0 for send
  lcore_id = 0;
  portid = 0;
  socket = rte_eth_dev_socket_id(portid);  

  // create the mbuf pool, used to initialize one rx queue
  l2fwd_pktmbuf_pool = rte_pktmbuf_pool_create("mbuf_pool",
					       NB_MBUF, 32,
					       0, RTE_MBUF_DEFAULT_BUF_SIZE,
					       rte_socket_id());
  if (l2fwd_pktmbuf_pool == NULL)
    rte_exit(EXIT_FAILURE, "Cannot init mbuf pool\n");
  
  nb_ports = rte_eth_dev_count();
  if (nb_ports == 0)
    rte_exit(EXIT_FAILURE, "No Ethernet ports - bye\n");

  // not needed??
  printf("Getting device info for port %u... \n", (unsigned) portid);
  rte_eth_dev_info_get(portid, &dev_info);
  printf("Device driver is %s\n", dev_info.driver_name);

  if (rte_lcore_is_enabled(lcore_id) == 0)
    rte_exit(EXIT_FAILURE, "lcore 0 not enabled - bye\n");

  printf("Initializing port %u... \n", (unsigned) portid);
  ret = rte_eth_dev_configure(portid, 1, 1, &port_conf);

  if (ret < 0)
    rte_exit(EXIT_FAILURE, "Cannot configure device: err=%d, port=%u\n",
	     ret, (unsigned) portid);

  rte_eth_macaddr_get(portid,&l2fwd_ports_eth_addr);

  // have to initialize one rx queue before starting!!??
  ret = rte_eth_rx_queue_setup(portid, 0, nb_rxd,
			       rte_eth_dev_socket_id(portid),
			       NULL,
			       l2fwd_pktmbuf_pool);
  if (ret < 0)
    rte_exit(EXIT_FAILURE, "rte_eth_rx_queue_setup:err=%d, port=%u\n",
	     ret, (unsigned) portid);
  
  // initialize one TX queue.
  // nb_txd is # tx ring descriptors (??)
  printf("Initializing one tx queue on port %u... \n", (unsigned) portid);
  ret = rte_eth_tx_queue_setup(portid, 0, nb_txd,
			       rte_eth_dev_socket_id(portid),
			       NULL);
  if (ret < 0)
    rte_exit(EXIT_FAILURE,
	     "rte_eth_tx_queue_setup:err=%d, port=%u\n",
	     ret, (unsigned) portid);
  
  // initialize TX buffers
  tx_malloc_size = RTE_ETH_TX_BUFFER_SIZE(MAX_PKT_BURST);
  printf("Allocating %lu bytes of memory for tx_buffer on NUMA socket %d for port %u... \n",
	  tx_malloc_size, socket, (unsigned) portid);  
  
  tx_buffer = rte_zmalloc_socket
    ("tx_buffer",
     tx_malloc_size, 0, socket
     );
  
  if (tx_buffer == NULL)
    rte_exit(EXIT_FAILURE,
	     "Cannot allocate buffer for tx on port %u\n",
	     (unsigned) portid);

  printf("Initializing default values for tx_buffer for port %u... \n", (unsigned) portid);
  ret = rte_eth_tx_buffer_init(tx_buffer, MAX_PKT_BURST);

  if (ret < 0)
    rte_exit(EXIT_FAILURE, "Cannot initialize default values for "
	     "tx buffer on port %u\n", (unsigned) portid);
  
  printf("Setting error callback for tx buffer on port %u... \n", (unsigned) portid);
  ret = rte_eth_tx_buffer_set_err_callback
    (tx_buffer,
     rte_eth_tx_buffer_count_callback,
     &dropped);

  if (ret < 0)
    rte_exit(EXIT_FAILURE, "Cannot set error callback for "
	     "tx buffer on port %u\n", (unsigned) portid);

  // Start device
  printf("Starting device/ port %u... \n", (unsigned) portid);
  ret = rte_eth_dev_start(portid);
  if (ret < 0)
    rte_exit(EXIT_FAILURE,
	     "rte_eth_dev_start:err=%d, port=%u\n",
	     ret, (unsigned) portid);
  printf("done: \n");

  // Launch function to send one packet on core 0
  //rte_eal_remote_launch(launch_one_lcore_send_one_packet,
  //			NULL, 0);
  //rte_eal_wait_lcore(0);

  launch_one_lcore_send_one_packet(NULL);
  
  printf("sent %lu packets.\n", tx);

  printf("Stopping device/ port %u... \n", (unsigned) portid);
  // stop ports
  rte_eth_dev_stop(portid);
  rte_eth_dev_close(portid);
  return ret;
}

