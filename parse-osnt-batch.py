#line = "d855 c2d4 4b07 0000 f301 0000 ac18 4a1d"
import socket
import struct
import sys
# sudo tcpdump -nxxi nf0 -w mydump.pcap
# sudo tcpdump -nxxr mydump.pcap | grep ac18 | grep 0x0000: | awk -F":" '{print $2;}' > lines.txt
def process_line(line):
    words = line.split()
    assert(len(words) == 8)
    timestamp = "".join(words[:4])
    rx_index = "".join(words[4:6])
    src_ip = "".join(words[6:])
    #src_ip.reverse()

    int_timestamp = int(change_endian(timestamp),16)
    ns_timestamp = (float(int_timestamp)/2**32)*10**9
    print "timestamp", '{0:.15f}'.format((ns_timestamp)),
    print " rx_index", int(change_endian(rx_index), 16),
    print " src_ip", socket.inet_ntoa(struct.pack(">L", int("".join(src_ip), 16)))
    
def change_endian(hex_string):
    #print "change endian for ", str(hex_string)
    b = hex_string.decode('hex')
    c = b[::-1]
    ret = c.encode('hex')
    # num_words = len(words)
    # assert(all([len(word) == 4 for word in words]))
    # ret = ["".join(words[num_words-1-i][j] for j in [3,2,1,0]) for i in range(num_words)]
    # print "we get ", str(ret)
    return ret

def main():
    assert(len(sys.argv) > 1)
    print sys.argv
    in_file = sys.argv[1]
    print in_file

    f = open(in_file, "r")
    for line in f.readlines():
        l = line.rstrip()
        process_line(l)

main()
