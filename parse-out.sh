grep "\[Device: id=1\] RX" out.txt | awk '{print $(NF-5);}' | grep -v packets > rx1-rates.txt
grep "\[Device: id=0\] TX" out.txt | awk '{print $(NF-5);}' | grep -v packets > tx0-rates.txt
grep "for total" out.txt | awk '{if (NR>1) {print " "$2" "$(NF-2);}}' | grep -v "Mbit/s" | sed 's/nan/0/' > conf-total.txt
grep "for total" out.txt | awk '{print $2" "$5;}' | sed 's/nan/0/' | grep -v "Mb/s" | sed 's/,//' > conf-active.txt

Rscript rates.R
open txrx.png
