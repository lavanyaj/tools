import random
BITS_PER_BYTE = 8
max_rate = 8 * 1e9
time_flows = 200000 # 20 ms
next_start = 0
while next_start < time_flows * 20:
    num_flows = int(round(random.random()*20)+1)
    total_size = (max_rate * time_flows * 1e-6)/BITS_PER_BYTE
    size_per_flow = round(total_size/num_flows)
    for i in range(num_flows):
        print "%d %d" % (size_per_flow, next_start)
    next_start += 2 * time_flows

