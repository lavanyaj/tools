#include <stdint.h>
//#include <mutex>
//#include <condition_variable>
#include <vector>

struct swsr_array{
  unsigned int value;
  //   std::mutex mutex;
  //std::condition_variable cond;
  unsigned int arr[100];
  std::size_t n;
};

extern "C" {
  struct swsr_array* make_swsr_array(std::size_t n, unsigned int init){
        struct swsr_array *a = new swsr_array;
	if (n < 100) a->n = n; else a->n = 100;
	for (int i = 0; i < a->n; i++)
	  a->arr[i] = init;
	
        return a;
    }

  void swsr_array_write(struct swsr_array *a, unsigned int value, unsigned int index){
    if (index < a->n)
      a->arr[index] = value;
    }

  unsigned int swsr_array_read(struct swsr_array *a, unsigned int index){
    if (index < a->n)
      return a->arr[index];
    else return 0;
    }
    // void swsr_array_reinit(struct swsr_array *b, size_t n){
    //     std::unique_lock<std::mutex> lock{b->mutex};
    //     b->n = n;
    // }
}
