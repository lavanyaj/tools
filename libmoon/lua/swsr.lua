--- Barriers to synchronize execution of different tasks
local mod = {}

local ffi = require "ffi"


ffi.cdef [[
    struct swsr_array { };
    struct swsr_array* make_swsr_array(size_t n, unsigned int init);
    void swsr_array_write(struct swsr_array *a, unsigned int value, unsigned int index);
    unsigned int swsr_array_read(struct swsr_array *a, unsigned int index);
]]

local C = ffi.C

local swsr = {}
swsr.__index = swsr

--- @param n number of tasks
function mod:new(n, init)
    return C.make_swsr_array(n, init)
end

function swsr:write(value, index)
    C.swsr_array_write(self, value, index)
end

function swsr:read(index)
    return C.swsr_array_read(self, index)
end

ffi.metatype("struct swsr_array", swsr)

return mod
