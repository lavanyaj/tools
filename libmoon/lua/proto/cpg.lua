------------------------------------------------------------------------
--- @file cpg.lua
--- @brief (cpg) utility.
--- Utility functions for the cpg_header structs 
--- Includes:
--- - cpg constants
--- - cpg header utility
--- - Definition of cpg packets
------------------------------------------------------------------------

--[[
-- Use this file as template when implementing a new protocol (to implement all mandatory stuff)
-- Replace all occurrences of cpg with your protocol (e.g. sctp)
-- Remove unnecessary comments in this file (comments inbetween [[...]]
-- Necessary changes to other files:
-- - packet.lua: if the header has a length member, adapt packetSetLength; 
-- 				 if the packet has a checksum, adapt packetCreate (loop at end of function) and packetCalculateChecksums
-- - proto/proto.lua: add cpg.lua to the list so it gets loaded
--]]
local ffi = require "ffi"

require "utils"
require "proto.template"
local initHeader = initHeader

local ntoh, hton = ntoh, hton
local ntoh16, hton16 = ntoh16, hton16
local bor, band, bnot, rshift, lshift, bswap= bit.bor, bit.band, bit.bnot, bit.rshift, bit.lshift, bit.bswap
local istype = ffi.istype
local format = string.format


---------------------------------------------------------------------------
---- cpg constants 
---------------------------------------------------------------------------

--- cpg protocol constants
local cpg = {}
cpg.MAX_HOPS = 2
cpg.INFINITE_RATE = 0xffffffffffffffff


---------------------------------------------------------------------------
---- cpg header
---------------------------------------------------------------------------

cpg.headerFormat = [[
        uint8_t hop;
	uint8_t	linkID;
        uint64_t sumSat;
        uint32_t numSat;
        uint32_t numFlows;
]]

--- Variable sized member
--cpg.headerVariableMember = nil

--- Module for cpg_address struct
local cpgHeader = initHeader()
cpgHeader.__index = cpgHeader

--[[ for all members of the header: set, get, getString 
-- for set also specify a suitable default value
--]]
--- Set the Hop.
--- @param int hop of the cpg header as 8 bit integer.
function cpgHeader:setHop(int)
   int = int or 0
   self.hop = int
end

--- Set the LinkID.
--- @param int linkID of the cpg header as 8 bit integer.
function cpgHeader:setLinkID(int)
   int = int or 0
   self.linkID = int
end

--- Set the sumSat.
--- @param double sumSat of the cpg header as 64 bit double.
function cpgHeader:setSumSat(double)
   double = double or 0
   int = double*(2^32)
   self.sumSat = int
end

--- Set the numSat.
--- @param int numSat of the cpg header as 32 bit integer.
function cpgHeader:setNumSat(int)
   int = int or 0
   self.numSat = hton(int)
end

--- Set the numFlows.
--- @param int numFlows of the cpg header as 32 bit integer.
function cpgHeader:setNumFlows(int)
   int = int or 0
   self.numFlows = hton(int)
end


--- Retrieve the hop.
--- @return hop as 8 bit integer.
function cpgHeader:getHop()
	return self.hop
end

--- Retrieve the linkID.
--- @return linkID as 8 bit integer.
function cpgHeader:getLinkID()
   return self.linkID
end

--- Retrieve the sumSat.
--- @return sumSat as 64 bit double.
function cpgHeader:getSumSat()
   int = self.sumSat
   double = tonumber(int)/(2^32)
   return double
end

--- Retrieve the numSat.
--- @return numSat as 32 bit integer.
function cpgHeader:getNumSat()
   return ntoh(self.numSat)
end

--- Retrieve the numFlows.
--- @return numFlows as 32 bit integer.
function cpgHeader:getNumFlows()
   return ntoh(self.numFlows)
end

--- Retrieve the XYZ as string.
--- @return XYZ as string.
--function cpgHeader:getXYZString()
--	return nil
--end

--- Set all members of the cpg header.
--- Per default, all members are set to default values specified in the respective set function.
--- Optional named arguments can be used to set a member to a user-provided value.
--- @param args Table of named arguments. Available arguments: cpgXYZ
--- @param pre prefix for namedArgs. Default 'cpg'.
--- @code
--- fill() -- only default values
--- fill{ cpgXYZ=1 } -- all members are set to default values with the exception of cpgXYZ, ...
--- @endcode
function cpgHeader:fill(args, pre)
	args = args or {}
	pre = pre or "cpg"

	self:setHop(args[pre .. "cpgHop"])
	self:setLinkID(args[pre .. "cpgLinkID"])
	self:setSumSat(args[pre .. "cpgSumSat"])
	self:setNumSat(args[pre .. "cpgNumSat"])
	self:setNumFlows(args[pre .. "cpgNumFlows"])
end

--- Retrieve the values of all members.
--- @param pre prefix for namedArgs. Default 'cpg'.
--- @return Table of named arguments. For a list of arguments see "See also".
--- @see cpgHeader:fill
function cpgHeader:get(pre)
	pre = pre or "cpg"

	local args = {}
	args[pre .. "cpgHop"] = self:getHop()
	args[pre .. "cpgLinkID"] = self:getLinkID()
	args[pre .. "cpgSumSat"] = self:getSumSat()
	args[pre .. "cpgNumSat"] = self:getNumSat()
	args[pre .. "cpgNumFlows"] = self:getNumFlows() 

	return args
end

--- Retrieve the values of all members.
--- @return Values in string format.
function cpgHeader:getString()
	return "cpg " --.. self:getXYZString()
	   .. " hop: " .. (self:getHop() or 1234)
	   .. ", linkID: " .. (self:getLinkID() or 1234)
	   .. ", sumSat: " .. (self:getSumSat())
	   .. " Mb/s"
	   .. ", numSat: " .. (self:getNumSat() or 1234)
	   .. ", numFlows: " .. (self:getNumFlows() or 1234)
end

--- Resolve which header comes after this one (in a packet)
--- For instance: in tcp/udp based on the ports
--- This function must exist and is only used when get/dump is executed on 
--- an unknown (mbuf not yet casted to e.g. tcpv6 packet) packet (mbuf)
--- @return String next header (e.g. 'eth', 'ip4', nil)
function cpgHeader:resolveNextHeader()
   return nil
end	

--- Change the default values for namedArguments (for fill/get)
--- This can be used to for instance calculate a length value based on the total packet length
--- See proto/ip4.setDefaultNamedArgs as an example
--- This function must exist and is only used by packet.fill
--- @param pre The prefix used for the namedArgs, e.g. 'cpg'
--- @param namedArgs Table of named arguments (see See more)
--- @param nextHeader The header following after this header in a packet
--- @param accumulatedLength The so far accumulated length for previous headers in a packet
--- @return Table of namedArgs
--- @see cpgHeader:fill
function cpgHeader:setDefaultNamedArgs(pre, namedArgs, nextHeader, accumulatedLength)
   -- set length
      if not namedArgs[pre .. "Length"] and namedArgs["pktLength"] then
      namedArgs[pre .. "Length"] = namedArgs["pktLength"] - accumulatedLength
      return namedArgs
      end
end
------------------------------------------------------------------------
---- Metatypes
------------------------------------------------------------------------

cpg.metatype = cpgHeader


return cpg
