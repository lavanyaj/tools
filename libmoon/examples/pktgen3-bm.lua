--- A simple UDP packet generator
--- to benchmark time spent/ iteration (prepping, waiting, tota) and line rate
--- as a function of packet length, batch size.
-- We're sending 15 control packets (50% to special queue)
-- and 15 data packets and echo-ing control packets received on special queue (50% of 15).
local lm     = require "libmoon"
local device = require "device"
local stats  = require "stats"
local log    = require "log"
local memory = require "memory"
local arp    = require "proto.arp"
local ip    = require "proto.ip4"
local eth    = require "proto.ethernet"
--ifaddr = {"nf0": {"mac": "02:53:55:4d:45:00", "ip": "1.2.3.4"}

-- set addresses here
local DST_MAC = {}
DST_MAC[2] = "08:22:22:22:22:08" --"0c:c4:7a:b7:60:3d" --"02:53:55:4d:45:00" -- resolved via ARP on GW_IP or DST_IP, can be overriden with a string here
DST_MAC[1] = "08:11:11:11:11:08" --"0c:c4:7a:b7:60:3d" --"02:53:55:4d:45:00" -- resolved via ARP on GW_IP or DST_IP, can be overriden with a string here
-- not sure how this works
-- 0C:C4:7A:B7:60:3C eth0
-- 0C:C4:7A:B7:60:3D eth1

local SRC_IP        = "10.0.0.0" -- "10.0.0.10"
local SRC_IP_BASE   = 167772160 -- "10.0.0" -- "10.0.0.10"
local DST_IP        = "1.2.3.4" --"10.1.0.10"
local SRC_PORT_BASE = 1000 -- 1234 -- actual port will be SRC_PORT_BASE * random(NUM_FLOWS)
local DST_PORT      = 1234
local NUM_FLOWS     = 1
local CHECK_TIME_EVERY = 1000

local BATCH_SIZE = 15
local PKT_LEN       = 1500

local BATCH_SIZE2 = 15
local PKT_LEN2       = 128


-- local PKT_LEN       = 60
-- local SRC_IP        = "10.0.0.10"
-- local DST_IP        = "10.1.0.10"
-- local SRC_PORT_BASE = 1234 -- actual port will be SRC_PORT_BASE * random(NUM_FLOWS)
-- local DST_PORT      = 1234
-- local NUM_FLOWS     = 1000
-- used as source IP to resolve GW_IP to DST_MAC
-- also respond to ARP queries on this IP
local ARP_IP	= SRC_IP
-- used to resolve DST_MAC
local GW_IP		= DST_IP


-- the configure function is called on startup with a pre-initialized command line parser
function configure(parser)
	parser:description("Edit the source to modify constants like IPs and ports.")
	parser:argument("dev", "Devices to use."):args("+"):convert(tonumber)
	parser:option("-t --threads", "Number of threads per device."):args(1):convert(tonumber):default(1)
	parser:option("-r --rate", "Transmit rate in Mbit/s per device."):args(1)
	parser:flag("-a --arp", "Use ARP.")
	return parser:parse()
end

function master(args,...)
	log:info("Check out MoonGen (built on lm) if you are looking for a fully featured packet generator")
	log:info("https://github.com/emmericp/MoonGen")

	-- configure devices and queues
	local arpQueues = {}
	for i, dev in ipairs(args.dev) do
		-- arp needs extra queues
		local dev = device.config{
			port = dev,
			txQueues = args.threads*3 + (args.arp and 1 or 0),
			rxQueues = 3
		}
		args.dev[i] = dev
	
	end
	device.waitForLinks()

	-- start ARP task and do ARP lookup (if not hardcoded above)

	-- print statistics
	stats.startStatsTask{devices = args.dev}

	-- configure tx rates and start transmit slaves
	for i, dev in ipairs(args.dev) do
		for j = 1, args.threads do
		   local queue = dev:getTxQueue(j - 1)
		   local queue2 = dev:getTxQueue(j+1 - 1)
		   local txSpQueue = dev:getTxQueue(j+2 - 1)
		   local rxQueue = dev:getRxQueue(j - 1)
		   local rxSpQueue = dev:getRxQueue(j+1 - 1)

		   dev:fiveTupleFilter({			 
			 proto = ip.PROTO_ICMP
				       }, rxSpQueue)
		   if args.rate then
		      queue:setRate(args.rate / args.threads)
		   end
		   lm.startTask("txSlave", rxQueue, rxSpQueue, txSpQueue, queue, queue2, DST_MAC[3-i])
		end
	end
	lm.waitForTasks()
end

function txSlave(rxQueue, rxSpQueue, txSpQueue, txQueue, txQueue2, dstMac)
   print("txSlave using dstMac " .. dstMac)
	-- memory pool with default values for all packets, this is our archetype
	local mempool = memory.createMemPool(function(buf)
		buf:getUdpPacket():fill{
			-- fields not explicitly set here are initialized to reasonable defaults
			ethSrc = txQueue, -- MAC of the tx device
			ethDst = dstMac,
			ip4Src = SRC_IP,
			ip4Dst = DST_IP,
			udpSrc = SRC_PORT,
			udpDst = DST_PORT,
			pktLength = PKT_LEN
		}
	end)

	local mempool2 = memory.createMemPool(function(buf)
		buf:getUdpPacket():fill{
			-- fields not explicitly set here are initialized to reasonable defaults
			ethSrc = tx2Queue, -- MAC of the tx device
			ethDst = dstMac,
			ip4Src = SRC_IP,
			ip4Dst = DST_IP,
			udpSrc = SRC_PORT,
			udpDst = DST_PORT,
			pktLength = PKT_LEN2
		}
	end)

	-- a bufArray is just a list of buffers from a mempool that is processed as a single batch
	local bufs = mempool:bufArray(BATCH_SIZE)
	local bufs2 = mempool:bufArray(BATCH_SIZE2)
	local rxBufs = memory.bufArray()
	local fwdBufs = memory.bufArray()
	
	local num_sends = 0
	local prep_start = nil
	--	local prep_end = nil

	local num_fwd = 0
	local num_rx = 0
	local num_rx_sp = 0
	while num_sends < 100000 and lm.running() do -- check if Ctrl+c was pressed
	   local lm_time = lm.getTime()
	   if (num_sends % CHECK_TIME_EVERY == 0) then
	      prep_start = {next=prep_start, value=lm_time}
	   end

	   -- BATCH 1
	   -- this actually allocates some buffers from the mempool the array is associated with
	   -- this has to be repeated for each send because sending is asynchronous, we cannot reuse the old buffers here
	   bufs:alloc(PKT_LEN)
	   for i, buf in ipairs(bufs) do
	      -- packet framework allows simple access to fields in complex protocol stacks
	      local pkt = buf:getUdpPacket()
	      pkt.ip4:setProtocol(ip.PROTO_UDP)
	      --pkt.udp:setSrcPort(SRC_PORT_BASE + i%1000) --math.random(0, NUM_FLOWS - 1))
	      pkt.ip4:setSrc(SRC_IP_BASE + (i%255))
	   end
	   -- if (num_sends % CHECK_TIME_EVERY == 0) then
	   --    prep_end = {next=prep_end, value=lm.getTime()}
	   -- end
	   -- UDP checksums are optional, so using just IPv4 checksums would be sufficient here
	   -- UDP checksum offloading is comparatively slow: NICs typically do not support calculating the pseudo-header checksum so this is done in SW
	   --bufs:offloadUdpChecksums()
	   -- send out all packets and frees old bufs that have been sent
	   txQueue:send(bufs)

	   -- BATCH 2
	   if true then
	      bufs2:alloc(PKT_LEN2)
	      for i, buf in ipairs(bufs2) do
		 -- packet framework allows simple access to fields in complex protocol stacks
		 local pkt = buf:getUdpPacket()
		 if i%2 == 0 then
		    pkt.ip4:setProtocol(ip.PROTO_ICMP)
		 else
		    pkt.ip4:setProtocol(ip.PROTO_UDP)
		 end
		 
		 --pkt.udp:setSrcPort(SRC_PORT_BASE + i%1000) --math.random(0, NUM_FLOWS - 1))
		 pkt.ip4:setSrc(SRC_IP_BASE + (i%255))
	      end
	      -- if (num_sends % CHECK_TIME_EVERY == 0) then
	      --    prep_end = {next=prep_end, value=lm.getTime()}
	      -- end
	      -- UDP checksums are optional, so using just IPv4 checksums would be sufficient here
	      -- UDP checksum offloading is comparatively slow: NICs typically do not support calculating the pseudo-header checksum so this is done in SW
	      --bufs:offloadUdpChecksums()
	      -- send out all packets and frees old bufs that have been sent
	      txQueue2:send(bufs2)
	   end
	   	   -- RX BATCH
	   -- a bufArray is just a list of buffers that we will use for batched forwarding
	   -- local flow_start = {}
	   -- flow_start["test"] = 0
	   -- local flow_finish = {}
	   -- local flow_count = {}
	   -- receive 0 or more packets from the queue
	   if true then
	      local count = rxQueue:tryRecv(rxBufs)
	      num_rx = num_rx + count
	      for i = 1, count do
		 local buf = rxBufs[i]
		 local pkt = buf:getUdpPacket()
		 --local src_ip = pkt.ip4:getSrc()
		 --local flow_id = src_ip - SRC_IP_BASE
		 buf:free()
	      end
	   end

	   if true then
	      -- FORWARD BATCH
	      -- send out all received fwdBufs on the other queue
	      -- the fwdBufs are free'd implicitly by this function
	      -- receive 0 or more packets from the queue

	      local count = rxSpQueue:tryRecv(fwdBufs)
	      if count >= 1 then
		 num_rx_sp = num_rx_sp + 1
		 for i = 1, count do
		    local buf = fwdBufs[i]
		    local pkt = buf:getUdpPacket()
		    pkt.ip4:setProtocol(ip.PROTO_UDP)
		    pkt.eth:setDstString(dstMac)
		    num_fwd = num_fwd + 1
		    --local src_ip = pkt.ip4:getSrc()
		    --local flow_id = src_ip - SRC_IP_BASE
		 end
		 txSpQueue:sendN(fwdBufs, count)
	      end
	   end

	   num_sends = num_sends + 1
	end

	-- local tmp = prep_start
	-- local tmp2 = prep_end
	-- while tmp and tmp2 do
	--    log:info(tmp.value * 1e6 .. " us start prep, "
	-- 	       .. (((tmp.value - tmp2.value) * 1e6)/CHECK_TIME_EVERY)
	-- 	       .. " us per batch of " .. BATCH_SIZE .. " packets")
	--    tmp2 = tmp2.next
	--    tmp = tmp.next
	-- end
	local tmp = prep_start
	local prev = 0
	while tmp do
	   local avg_diff = (((tmp.value - prev) * 1e6)/CHECK_TIME_EVERY)
	   local mbps = ((BATCH_SIZE * PKT_LEN * 8) + (BATCH_SIZE2 * PKT_LEN2 * 8))/avg_diff
	   log:info(tmp.value * 1e6 .. " us start prep, "
		       .. avg_diff
		       .. " us per batch of " .. BATCH_SIZE .. " " .. PKT_LEN .. " B packets"
		       .. " plus ".. BATCH_SIZE2 .. " " .. PKT_LEN2 .. " B packets"
		    .. " for a rate of " .. mbps .. " Mb/s")
	   prev = tmp.value
	   tmp = tmp.next
	end
	log:info("Forwarded " .. num_fwd .. " packets")
	log:info("Received " .. num_rx .. " packets on regular rx queue")
	log:info("Received " .. num_rx_sp .. " packets on special rx queue")
	lm.stop()
	
end

-- Lab Notes

-- Yes when checking every 1000 sends, average (63*1500*(10000/9867)B)/76.78us to Gb/s is 9.979 Gb/s but not sure why negative us (oh cuz I'm reading list in reverse order). But when checking every send , more variation some says 39us and even 6us and some 80us mostly 78us.

-- Checking time to prepare vs time to return from send?
-- Actually looks like one of time to prepare is <0.005us and time to return from send is the remaining (cuz we can only fill up ring buffer as fast as it's being emptied).
--    So it looks like network is bottleneck by a lot.
--    Also rdtsc is ~ 30 ns (or maybe in my case it's faster and less than 5ns which is 6-7 cycles), so subtract that from measured time to prepare and time to prepare is negative. 64b timestamp increased every clock cycle (1.2GHz -> 0.83 ns .. but max 3.2GHz hmm)
-- Invariant that if sending at line rate: time to prepare plus time to send to queue non blocking should be transmit time of batch (right!!?) .. so if we're taking longer to prepare prob. time to send to queue non blocking will be smaller cuz it will have freed up more


-- What if we're sending smaller batches? ** 
-- Batch size is 15: checking every 1000 .. 18.25 to 18.29 (4:1) per batch .. 9837 - 9857 Mb/s ~ 9841 Mb/s d wt
-- Batch size is 31: checking every 1000 .. 36 to 37us per batch .. 9840 - 9850 Mb/s .. 37.76-37.80us wt 
-- Batch size is 63: checking every 1000 .. 75-76us per batch .. 9841 - 9846 Mb/s .. 75.73 to 76.81 wt

-- (RDTSC: "RDTSC is one of those complex instructions that consist of a sequence of micro-ops."  people say like 25-33ns on a 1.5-3 GHz machinea)

-- What if we're sending smaller packets? ***
-- Can't seem to get above 7.27 Mb/s hmm... extra padding? or software bottleneck? or network?
-- Batch size is 127: checking every 1000 .. 8.94 us per batch .. 7272.4 Mb/s (2047 etc. gives seg fault)
-- Batch size is 1023: checking every 1000 .. 752.019 us per batch .. 7272.66 Mb/s (2047 etc. gives seg fault)

-- Also do all the same experiments where we're checking time every batch.

-- What if we send one batch of small packets and one batch of large packets?
-- What if we're sending on two queues instead of one?
-- (Not sure what mempool is, if we need 2 or 1 ..)
-- -85.76 us per batch of 127 64 B packets plus 63 1500 B packets for a rate of -9572.59 Mb/s (9574 mostly)
-- -92.25 us per batch of 127 128 B packets plus 63 1500 B packets for a rate of -9604.44 Mb/s
-- -33.73 us per batch of 127 128 B packets plus 15 1500 B packets for a rate of -9191.49 Mb/s
-- -22.06 us per batch of 31 128 B packets plus 15 1500 B packets for a rate of -9594.31 Mb/s
-- -20.12 us per batch of 15 128 B packets plus 15 1500 B packets for a rate of -9707.62186 Mb/s etc.
-- ---
-- 36.59 us per batch of 15 1500 B packets plus 15 1500 B packets for a rate of -9837.28 Mb/s (4:1) (rate like 15, time per batch like 63)
-- 153.63 us per batch of 63 1500 B packets plus 63 1500 B packets for a rate of -9841.255 Mb/s (rate like 63, time per batch like 128?) etc.

-- [ fair sharing across queues so control traffic overhead will be (127 * 128 B)/(127(128 B + 15*1500 B) of total rate, if we're sending control packets in every batch but probably won't or maybe not 127 different packets?? if a round is 30 us say, we want to send up to 127 in each round -> but that's 4.3 Gb/s too much. But at least we know we can check every 33us. Maybe send control packets for flows that won't get sent in this batch.. more flows means longer rounds...]
-- ---

-- ok so smaller batches and sending some small control packets is a good way to get short rounds (though control traffic overhead will determine round length too e.g., 100 * 128B/ 30us means overhead at least 4 Gb/s. Given max overhead of say 1 Gb/s .. shortest Cpg round we can fundamentally get is 102 us (for 100 flows max and 50us for 50 flows max). And practically  if batch length is 20us .. and we send 15 control packets every batch .. for 100 control packets we'll take 130 us. Or if we send 30 by putting control info in data packet when possibl .. we'll take 60-ish us)
