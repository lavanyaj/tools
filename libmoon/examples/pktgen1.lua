--- A simple UDP packet generator
local lm     = require "libmoon"
local device = require "device"
local stats  = require "stats"
local log    = require "log"
local memory = require "memory"
local arp    = require "proto.arp"
local timer = require "timer"

--ifaddr = {"nf0": {"mac": "02:53:55:4d:45:00", "ip": "1.2.3.4"}

-- set addresses here
local DST_MAC       = "02:53:55:4d:45:00" -- resolved via ARP on GW_IP or DST_IP, can be overriden with a string here
local PKT_LEN       = 1500
local SRC_IP        = "10.0.0.0" -- "10.0.0.10"
local SRC_IP_BASE   = 167772160 -- "10.0.0" -- "10.0.0.10"
local DST_IP        = "1.2.3.4" --"10.1.0.10"
local SRC_PORT_BASE = 1000 -- 1234 -- actual port will be SRC_PORT_BASE * random(NUM_FLOWS)
local DST_PORT      = 1234
local NUM_FLOWS     = 1
-- local PKT_LEN       = 60
-- local SRC_IP        = "10.0.0.10"
-- local DST_IP        = "10.1.0.10"
-- local SRC_PORT_BASE = 1234 -- actual port will be SRC_PORT_BASE * random(NUM_FLOWS)
-- local DST_PORT      = 1234
-- local NUM_FLOWS     = 1000
-- used as source IP to resolve GW_IP to DST_MAC
-- also respond to ARP queries on this IP
local ARP_IP	= SRC_IP
-- used to resolve DST_MAC
local GW_IP		= DST_IP


-- the configure function is called on startup with a pre-initialized command line parser
function configure(parser)
	parser:description("Edit the source to modify constants like IPs and ports.")
	parser:argument("dev", "Devices to use."):args("+"):convert(tonumber)
	
	parser:option("-t --threads", "Number of threads per device."):args(1):convert(tonumber):default(1)
	parser:option("-n --num_intervals", "Number of intervals to run."):args(1):convert(tonumber):default(1)
	parser:option("-i --interval", "Length of each interval (us)."):args(1):convert(tonumber):default(1e6)
	parser:option("-r --rate", "Transmit rate in Mbit/s per device."):args(1)
	parser:flag("-a --arp", "Use ARP.")
	return parser:parse()
end

function master(args,...)
	log:info("Check out MoonGen (built on lm) if you are looking for a fully featured packet generator")
	log:info("https://github.com/emmericp/MoonGen")

	-- configure devices and queues
	local arpQueues = {}
	for i, dev in ipairs(args.dev) do
		-- arp needs extra queues
		local dev = device.config{
			port = dev,
			txQueues = args.threads + (args.arp and 1 or 0),
			rxQueues = args.arp and 2 or 1
		}
		args.dev[i] = dev
		if args.arp then
			table.insert(arpQueues, { rxQueue = dev:getRxQueue(1), txQueue = dev:getTxQueue(args.threads), ips = ARP_IP })
		end
	end
	device.waitForLinks()

	-- start ARP task and do ARP lookup (if not hardcoded above)
	if args.arp then
		arp.startArpTask(arpQueues)
		if not DST_MAC then
			log:info("Performing ARP lookup on %s, timeout 3 seconds.", GW_IP)
			DST_MAC = arp.blockingLookup(GW_IP, 3)
			if not DST_MAC then
				log:info("ARP lookup failed, using default destination mac address")
				DST_MAC = "01:23:45:67:89:ab"
			end
		end
		log:info("Destination mac: %s", DST_MAC)
	end

	-- get rates timeseries
	rates_ts = generateRateTimeseries(args)
	lm.startTask("setRates", args, rates_ts)
	-- ^THIS DOESN'T WORK IF I MOVE TO END, THEN CAN'T
	-- GET AT TXQUEUE IDK WHY
	
	-- print statistics
	stats.startStatsTask{devices = args.dev}
	print("args.dev")
	print(args.dev)
	-- configure tx rates and start transmit slaves
	for i, dev in ipairs(args.dev) do
		for i = 1, args.threads do
		   local queue = dev:getTxQueue(i - 1)
		   --print("queue")
		   --print(queue)
		   if args.rate then			   
		      queue:setRate(args.rate / args.threads)
		   end
		   lm.startTask("txSlave", queue, DST_MAC)
		end
	end

	
	lm.waitForTasks()
end

function generateRateTimeseries(args)
   local num_threads = args.threads or 0
   local num_intervals = args.num_intervals or 0
   local ts = {}
   local max_capacity = 9000.0
   local str = "timeseries"
   local index = 1
   for i=1,num_intervals do
      local step_capacity = i/num_intervals * max_capacity
      for j=1,num_threads do
	 rate = 1.0/num_threads * step_capacity
	 ts[i*num_threads + j] = rate
	 num_packets = ((rate * args.interval))/(1500 * 8)
	 str = str .. rate .. " Mb/s for " .. args.interval .. " us " .. "( " .. num_packets .. " packets from " .. index .. " to " .. index+num_packets - 1 .. " ), "
	 index = index + num_packets
      end
   end
   print(str)
   return ts
end


function setRates(args, rates_timeseries)
   args.dev = args.dev or {}
   args.interval = args.interval or {}
   args.threads = args.threads or 0
   args.num_intervals = args.num_intervals or 0
   args.interval = args.interval or 1e6
   local interval = args.interval * 1e-6
   
   local tm = timer.new{interval}
   local step = 1
   while step <= args.num_intervals do
      tm:busyWait()
      for i,dev in ipairs(args.dev) do
	 for q = 1, args.threads do
	    local queue = dev:getTxQueue(q - 1)
	    rate = rates_timeseries[step*args.threads + q]
	    log:info("Setting rate of queue %d to %f", q, rate)
	    queue.setRate(queue, rate)
	 end
      end
      tm:reset(interval)
      step = step + 1
   end
end

function txSlave(queue, dstMac)
	-- memory pool with default values for all packets, this is our archetype
	local mempool = memory.createMemPool(function(buf)
		buf:getUdpPacket():fill{
			-- fields not explicitly set here are initialized to reasonable defaults
			ethSrc = queue, -- MAC of the tx device
			ethDst = dstMac,
			ip4Src = SRC_IP,
			ip4Dst = DST_IP,
			udpSrc = SRC_PORT,
			udpDst = DST_PORT,
			pktLength = PKT_LEN
		}
	end)
	-- a bufArray is just a list of buffers from a mempool that is processed as a single batch
	local bufs = mempool:bufArray()
	local num_sends = 0
	while num_sends < 100000 and lm.running() do -- check if Ctrl+c was pressed
		-- this actually allocates some buffers from the mempool the array is associated with
		-- this has to be repeated for each send because sending is asynchronous, we cannot reuse the old buffers here
		bufs:alloc(PKT_LEN)
		for i, buf in ipairs(bufs) do
			-- packet framework allows simple access to fields in complex protocol stacks
			local pkt = buf:getUdpPacket()
			--pkt.udp:setSrcPort(SRC_PORT_BASE + i%1000) --math.random(0, NUM_FLOWS - 1))
			pkt.ip4:setSrc(SRC_IP_BASE + (i%255))
		end
		-- UDP checksums are optional, so using just IPv4 checksums would be sufficient here
		-- UDP checksum offloading is comparatively slow: NICs typically do not support calculating the pseudo-header checksum so this is done in SW
		--bufs:offloadUdpChecksums()
		-- send out all packets and frees old bufs that have been sent
		queue:send(bufs)
		num_sends = num_sends + 1
	end
end

