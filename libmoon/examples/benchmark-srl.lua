--- A simple UDP packet generator
local lm     = require "libmoon"
local device = require "device"
local stats  = require "stats"
local log    = require "log"
local memory = require "memory"
local arp    = require "proto.arp"
local filter    = require "filter"
local ceil = math.ceil

--ifaddr = {"nf0": {"mac": "02:53:55:4d:45:00", "ip": "1.2.3.4"}
-- eth2      Link encap:Ethernet  HWaddr 0c:c4:7a:b7:60:3c   dev 0
-- eth3      Link encap:Ethernet  HWaddr 0c:c4:7a:b7:60:3d   dev 1
-- set addresses here

local DST_MAC       = "08:22:22:22:22:08" -- "08:11:11:11:11:08" -- --"02:53:55:4d:45:00" --"0c:c4:7a:b7:60:3d"
local DST_MAC_NO = 8942694572552 -- 8869393797384 -- 0x81111111108 -- 8942694572552 --0x82222222208
local PKT_LEN       = 1500
local SRC_IP        = "10.0.0.0" -- "10.0.0.10"
local SRC_IP_BASE   = 167772160 -- "10.0.0" -- "10.0.0.10"
local DST_IP        = "1.2.3.4" --"10.1.0.10"
local SRC_PORT_BASE = 1000 
local DST_PORT      = 1234
local NUM_FLOWS     = 1
local ETH_DROP = 0x1234

-- the configure function is called on startup with a pre-initialized command line parser
function configure(parser)
	parser:description("Edit the source to modify constants like IPs and ports.")
	parser:argument("dev", "Devices to use."):args("+"):convert(tonumber)
	parser:option("-t --threads", "Number of threads per device."):args(1):convert(tonumber):default(1)
	return parser:parse()
end

function master(args,...)
   -- configure devices and queues
   -- dev 0 generates packets
   -- dev 1 receives packets and echoes them
   -- if args are 0 1 then both running
   -- if args is just 0 only dev 0 generating
   -- .. 1 only dev 1 receiving and echoing
   
   local arpQueues = {}
   
   for i, dev in ipairs(args.dev) do
	   -- arp needs extra queues
	   local dev = device.config{
	      port = dev,
	      txQueues = args.threads,
	      rxQueues = args.threads+1,
	      rssQueues = args.threads
	   }
	   args.dev[i] = dev
	   
   end
   device.waitForLinks()

   -- print statistics
   stats.startStatsTask{devices = args.dev}

   -- configure tx rates and start transmit slaves
   local setup = {}
   if (setup[0] ==  nil) then
      log:info("setup[0]: nil")
   end
   for i, dev in ipairs(args.dev) do
      local dev = args.dev[i]
      if (dev ==  nil) then
	 log:info("dev: nil")
      end
      print("dev", dev)
      print("dev.id", dev.id) 
      if (dev.id == 0 and setup[0] == nil) then
	 log:info("Setting up dev 0 to tx")
	 for i = 1, args.threads do
	    local queue = dev:getTxQueue(i - 1)
	    lm.startTask("txSlave", queue, DST_MAC, DST_MAC_NO)
	 end
	 setup[0] = true
      end
      if (dev.id == 1 and setup[1] == nil) then
	 -- log:info("Setting up dev 1 to tx")
	 -- for i = 1, args.threads do
	 --    local queue = dev:getTxQueue(i - 1)
	 --    lm.startTask("txSlave", queue, DST_MAC)
	 -- end
	 log:info("Setting up dev 1 to rx and echo")
	 dev:l2Filter(ETH_DROP, args.threads) -- last rx queue is discard queue
	 for i = 1, args.threads do
	    lm.startTask("forward", dev:getRxQueue(i - 1), dev:getTxQueue(i - 1))
	 end
	 setup[1] = true
      end
   end
   lm.waitForTasks()
end

function txSlave(queue, dstMac, dstMacNo)

   
   -- memory pool with default values for all packets, this is our archetype
   local mempool = memory.createMemPool(function(buf)
	 buf:getUdpPacket():fill{
	    -- fields not explicitly set here are initialized to reasonable defaults
	    ethSrc = queue, -- MAC of the tx device
	    ethDst = dstMac,
	    ip4Src = SRC_IP,
	    ip4Dst = DST_IP,
	    udpSrc = SRC_PORT,
	    udpDst = DST_PORT,
	    pktLength = PKT_LEN
				}
   end)

   local r = 0

   -- Set up timing wheel
   -- timing wheel slots carry either flow # or 0 for void packet
   -- say flow's min speed is 100 Mb/s - 120 us
   -- if each slot is 1.2us we need 100 slots and check up to 1 slot
   -- before we send a void packet and check up to O(N) until we find
   -- and empty slot to re-insert
   -- if each slot is 0.12us we need 1000 slots and check up to 10 slots
   -- before we send a void packet and check up to O(N) until we find an empty
   -- slot to re-insert
   -- and don't need to keep array of lists

   -- Here, we're assuming minimum speed we support is 10 Mb/s i.e.,
   -- some link could have >= 100 unsaturated flows (very unlikely)
   tw_num_slots = 100
   tw_slots = {}
   for i = 1, tw_num_slots do
      tw_slots[i] = 0
   end
   -- when indexing into array, always +1
   local tw_current_slot = 0
   -- smallest slot is 1/tw_max_rate, 1.2 us for 10 Gb/s
   local tw_max_rate = 10000

   -- Per flow state, for now, only one flow
   local gap_flow = 0
   local rate_flow = 0
   local nsent_flow = 0
   
   -- Insert first packet of flow 1 into current slot
   tw_slots[tw_current_slot+1] = 1
   
   local rates = {2000, 5000, 8000}
   local next_rate = rates[r+1]
   -- a bufArray is just a list of buffers from a mempool that is processed as a single batch
   local bufs = mempool:bufArray()
   local num_sends = 0
   --while num_sends < 1000 and lm.running do
   while lm.running() do -- check if Ctrl+c was pressed
      if (num_sends%100 == 0) then
	 rate_flow = next_rate
	 gap_flow = ceil(tw_max_rate/rate_flow)	 
	 r = (r + 1)%3
	 next_rate = rates[r+1]
      end
      bufs:alloc(PKT_LEN)
      for i, buf in ipairs(bufs) do
	 local pkt = buf:getUdpPacket()
	 
	 if (tw_slots[tw_current_slot+1] > 0) then
	    -- important to update source else will use value in buffer (garbage/ old)
	    pkt.eth:setSrc(0x253554d4500)
	    pkt.ip4:setSrc(SRC_IP_BASE + (1%255))
	    -- update timing wheel O(1)
	    tw_slots[tw_current_slot+1] = 0
	    local next_index = tw_current_slot + gap_flow
	    if (next_index >= tw_num_slots) then
	       next_index = next_index - tw_num_slots
	    end
	    -- why would tw_slots[next_index+1] be 1??
	    --assert(tw_slots[tw_current_slot+1] == 0)
	    --assert(tw_slots[next_index+1] == 0)
	    --assert(next_index < tw_num_slots)
	    --if (tw_slots[next_index+1] == 0) then
	    tw_slots[next_index+1] = 1
	    --end
	    nsent_flow = nsent_flow + 1
	 else	    
	    pkt.eth:setSrc(dstMacNo) -- so packet is dropped at switch
	 end
	 tw_current_slot = tw_current_slot + 1
	 if (tw_current_slot >= tw_num_slots) then
	    --assert(tw_current_slot == tw_num_slots)
	    tw_current_slot = 0
	 end
      end
      -- send out all packets and frees old bufs that have been sent
      queue:send(bufs)
      num_sends = num_sends + 1
   end
   log:info("sent %d packets out of ~%d", nsent_flow, (num_sends * 63))
end


function forward(rxQueue, txQueue)
	-- a bufArray is just a list of buffers that we will use for batched forwarding
	local bufs = memory.bufArray()
	while lm.running() do -- check if Ctrl+c was pressed
		-- receive one or more packets from the queue
	   local count = rxQueue:recv(bufs)
	   for i = 1, count do
	      local buf = bufs[i]
	      buf:free()
	   end
		-- send out all received bufs on the other queue
		-- the bufs are free'd implicitly by this function
		--txQueue:sendN(bufs, count)
	end
end

