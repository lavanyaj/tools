--- A simple UDP packet generator. Generates packets for given flow schedule.
--- Notion of Cpg round = (current time - given start time)/ xx us
--- Flows are added to active list every 4th Cpg round
--- Use circular array of tw_num_slots slots to schedule packets of flows
--- Send BATCH_SIZE packets every loop with (hopefully) few operations/ pkt
--- Current slot in tw_num_slots ticks with each packet in batch.. so if
--- packets are being sent at line rate, a slot = Line rate tx time of pkt.
--- A flow that hasn't finished in re-inserted n slots in the future where
--- n is inter-packet time (actually rounded up to nearest multiple of 1.2us)
--- for flow.
--- Jan 9th: load flow schedule from file. count flow stats at receiver.
---  update all active flow's rates to max_rate/N every 4th Cpg round.
---  log flow stats at Tx and Rx. simple rdtsc check time every iteration.

local lm     = require "libmoon"
local device = require "device"
local stats  = require "stats"
local log    = require "log"
local memory = require "memory"
local arp    = require "proto.arp"
local filter    = require "filter"
local open = io.open
local ceil = math.ceil

--ifaddr = {"nf0": {"mac": "02:53:55:4d:45:00", "ip": "1.2.3.4"}
-- eth2      Link encap:Ethernet  HWaddr 0c:c4:7a:b7:60:3c   dev 0
-- eth3      Link encap:Ethernet  HWaddr 0c:c4:7a:b7:60:3d   dev 1
-- set addresses here

local DST_MAC       = "08:22:22:22:22:08" -- "08:11:11:11:11:08" -- --"02:53:55:4d:45:00" --"0c:c4:7a:b7:60:3d"
local DST_MAC_NO = 8942694572552 -- 8869393797384 -- 0x81111111108 -- 8942694572552 --0x82222222208
local SRC_IP        = "10.0.0.0" -- "10.0.0.10"
local SRC_IP_BASE   = 167772160 -- "10.0.0" -- "10.0.0.10"
local DST_IP        = "1.2.3.4" --"10.1.0.10"
local SRC_PORT_BASE = 1000 
local DST_PORT      = 1234
local NUM_FLOWS     = 1
local ETH_DROP = 0x1234
local ROUND = 50 * 1e-6
-- checks every 15-20 us .. sends 15 packets every 15-20us enough to send all flows in this time.. and bw??

local PKT_LEN       = 1500
local BATCH_SIZE = 15
local CHECK_TIME_EVERY = 100

local INIT_GAP_FLOW = 10
local INIT_RATE_FLOW = 1000 --1000

-- the configure function is called on startup with a pre-initialized command line parser
function configure(parser)
	parser:description("Edit the source to modify constants like IPs and ports.")
	parser:argument("dev", "Devices to use."):args("+"):convert(tonumber)
	parser:option("-t --threads", "Number of threads per device."):args(1):convert(tonumber):default(1)
	return parser:parse()
end

function master(args,...)   
   for i, dev in ipairs(args.dev) do
      local dev = device.config{
	 port = dev,
	      txQueues = args.threads,
	      rxQueues = args.threads+1,
	      rssQueues = args.threads
      }
      args.dev[i] = dev
      
   end
   device.waitForLinks()

   -- print statistics
   stats.startStatsTask{devices = args.dev}

   local dev = args.dev[1]
   
   for i = 1, args.threads do
      local queue = dev:getTxQueue(i - 1)
      lm.startTask("txSlave", queue, DST_MAC, DST_MAC_NO)
   end

   dev = args.dev[2]
   for i = 1, args.threads do
      lm.startTask("rxSlave", dev:getRxQueue(i - 1))
   end
   lm.waitForTasks()
end

function txSlave(queue, dstMac, dstMacNo)
   local flow_start = {}
   local flow_finish = {}
   
   -- memory pool with default values for all packets, this is our archetype
   local mempool = memory.createMemPool(function(buf)
	 buf:getUdpPacket():fill{
	    -- fields not explicitly set here are initialized to reasonable defaults
	    ethSrc = queue, -- MAC of the tx device
	    ethDst = dstMac,
	    ip4Src = SRC_IP,
	    ip4Dst = DST_IP,
	    udpSrc = SRC_PORT,
	    udpDst = DST_PORT,
	    pktLength = PKT_LEN
				}
   end)

   local r = 0

   -- Workload info, by flow index (name, size in packet, start time in us)
   -- To be pre-calculated and filled in
   local wl_name = {}--12, 23, 34, 45, 56, 67, 78}
   local wl_size = {}--1400, 1500, 500, 1200, 500, 800, 900}
   local wl_start = {}--{100, 280, 350, 350, 500, 650, 800}
   local wl_cpg_start = {0, 0 , 0, 0, 0, 0, 0}
   local wl_cpg_finish = {0, 0 , 0, 0, 0, 0, 0}
   local wl_nsent = {0, 0 , 0, 0, 0, 0, 0}
   local wl_nflows = 0
   local next_new_flow = 1
   
   local file = open("examples/flows.txt")
   local shift_start_us = 1000
   local prev_start = 0
   if file then
      for line in file:lines() do
	 local words = {}
	 word_no = 1
	 for w in  string.gmatch(line, "%S+") do
	    words[word_no] = w
	    word_no = word_no + 1
	    end
	 local size_mtus = ceil(words[1]/1500.0)
	 local index = wl_nflows + 1
	 wl_name[index] = index
	 wl_size[index] = size_mtus
	 wl_start[index] = shift_start_us+words[2]
	 wl_nflows = wl_nflows + 1
	 log:info("flow " .. index .. " has size " .. size_mtus .. " starts at " .. wl_start[index] .. " ("
		     .. (wl_start[index]-prev_start) .. " us after previous flow)")
	 prev_start = wl_start[index]
      end
   end
   

   
   -- Set up timing wheel
   -- timing wheel slots carry either flow # or 0 for void packet
   -- say flow's min speed is 100 Mb/s - 120 us
   -- if each slot is 1.2us we need 100 slots and check up to 1 slot
   -- before we send a void packet and check up to O(N) until we find
   -- and empty slot to re-insert
   -- if each slot is 0.12us we need 1000 slots and check up to 10 slots
   -- before we send a void packet and check up to O(N) until we find an empty
   -- slot to re-insert
   -- and don't need to keep array of lists

   -- Here, we're assuming minimum speed we support is 100 Mb/s i.e.,
   -- some link could have >= 100 unsaturated flows (very unlikely)   
   tw_num_slots = 1000
   -- a tw_slot ~ xx us where 1/xx is the rate at which data packets are sent
   -- (currently assuming it's line rate)
   tw_slots = {}
   tw_num_free = 0
   -- If we reshedule in x slots, but slot is taken
   -- need to find smallest free slot bigger than curr+x
   -- For now, plan to step through ..
   -- When iterating through timeslots, we can go slot by slot
   -- instead of jumping.
   for i = 1, tw_num_slots do
      tw_slots[i] = 0
      tw_num_free = tw_num_free + 1
   end
   
   -- when indexing into array, always +1
   local tw_current_slot = 0
   -- smallest slot is 1/tw_max_rate, 1.2 us for 10 Gb/s
   local tw_max_rate = 10000

   -- Per flow state for data plane, only for active flows
   local max_flows = 100
   local gap_flow = {}
   local rate_flow = {}
   local nsent_flow = {}
   local name_flow = {}
   local size_flow = {}

   -- Doubly linked list of free flow state slots
   local next_free = {}
   local prev_free = {}
   local start_free = 1
   local num_free = 0
   for i = 1, max_flows do
      gap_flow[i] = INIT_GAP_FLOW
      rate_flow[i] = INIT_RATE_FLOW
      nsent_flow[i] = 0
      size_flow[i] = 0
      name_flow[i] = -1
      prev_free[i] = i-1
      next_free[i] = i+1
      if i == 1 then
	 prev_free[i] = max_flows
      elseif i == max_flows then
	 next_free[i] = 1
      end
      num_free = num_free + 1
   end

   
   -- Insert first packet of flow 1 into current slot
   -- tw_slots[tw_current_slot+1] = 1
   
   -- local rates = {2000, 5000, 10000}
   -- local next_rate = rates[r+1]
   -- a bufArray is just a list of buffers from a mempool that is processed as a single batch
   local bufs = mempool:bufArray(BATCH_SIZE)

   -- Cpg rounds start from nearest even millisecdon in the future
   -- TODO: Need to sync this start time across machines so wall clock time
   -- is same, also well as correct for drifts
   local now = lm.getTime()
   local start_time = now + 1e-3
   log:info("starting simulation at ", start_time)


   local num_active_flows = 0
   local expired_flows = nil
   
   local num_sends = 0
   local cpg_round = 0
   local prev_cpg_round = -100

   local prep_start = nil
   local cpg_round_check = nil
   
   while num_sends < 150000 and next_new_flow < 1000 and lm.running() do
   --while lm.running() do -- check if Ctrl+c was pressed
      local lm_time = lm.getTime()
      cpg_round = ceil((lm_time - start_time)/ROUND)
      
      -- code to benchmark time taken per batch
      if (num_sends > 0 and num_sends % CHECK_TIME_EVERY == 0) then
	 prep_start = {next=prep_start, value=lm_time}
      end

      -- check if cpg round changes
      if cpg_round > 0 and cpg_round > prev_cpg_round then
	 --log:info("Cpg round " .. cpg_round .. " at time " .. lm_time .. " (" .. (lm_time-start_time)*1e6 .. " us from start)")
	 prev_cpg_round = cpg_round
	 cpg_round_check = {next=cpg_round_check, value=lm_time, nflows=num_active_flows}
	 
	 if (cpg_round ~= 0 and cpg_round > prev_cpg_round+1) then
	    log:error("Cpg round jumped from "
			 .. prev_cpg_round .. " to "
			 .. cpg_round .. " in one iteration.")
	 end	   
	    
	 if (cpg_round%4 == 1) then
	    local old_num_active_flows = num_active_flows
	    
	    -- could avoid per-flow ops here
	    -- remove all expired flows
	    local curr = start_free
	    local tmp = next_free[start_free]

	    local ef  = expired_flows 
	    while (ef) do
	       num_active_flows = num_active_flows - 1
	       --log:info("Removing flow ".. ef.flow_index.. ": ".. name_flow[ef.flow_index])
	       wl_cpg_finish[name_flow[ef.flow_index]] = (cpg_round-1)
	       wl_nsent[name_flow[ef.flow_index]] = ef.nsent
	       name_flow[ef.flow_index] = -1
	       next_free[curr] = ef.flow_index
	       curr = ef.flow_index
	       num_free = num_free + 1
	       ef = ef.next
	    end
	    next_free[curr] = tmp
	    prev_free[tmp] = curr		   
	    expired_flows = nil
		   
	    -- add new flows, until no more free timeslots or flow state
	    local curr = start_free
	    local tmp = prev_free[start_free]
	    local tw_insert_at = tw_current_slot
		   
	    while next_new_flow < wl_nflows and
	       wl_start[next_new_flow] <= (cpg_round * ROUND * 1e6)
	    and num_free > 0 and tw_num_free > 0 do
	       num_active_flows = num_active_flows + 1
	       name_flow[curr] = wl_name[next_new_flow]
	       size_flow[curr] = wl_size[next_new_flow]
	       nsent_flow[curr] = 0
	       gap_flow[curr] = INIT_GAP_FLOW
	       rate_flow[curr] = INIT_RATE_FLOW
	       while tw_slots[tw_insert_at+1] > 0 and tw_num_free > 0 do
		  tw_insert_at = tw_insert_at+1
		  if (tw_insert_at == tw_num_slots) then
		     tw_insert_at = 0
		  end
	       end
	       -- insert first data packet/ SYN
	       -- (also need to update rates of other flows)
	       -- always leave room for first data packets.
	       assert(tw_slots[tw_insert_at+1] == 0)
	       tw_slots[tw_insert_at+1] = curr
	       tw_num_free = tw_num_free - 1
	       wl_cpg_start[wl_name[next_new_flow]] = cpg_round
	       --log:info("Adding new flow ".. curr.. ": ".. name_flow[curr].. " at "
	       --		  .. tw_insert_at
	       --		  .. " gap " .. gap_flow[curr] .. ", rate " .. rate_flow[curr]
	       --	  .. ", scheduled to start " .. wl_start[next_new_flow] .. " us after T=0")
		      
	       next_new_flow = next_new_flow + 1
		      
	       curr = next_free[curr]
	       num_free = num_free - 1
	    end
	    start_free = curr
	    prev_free[curr] = tmp
	    next_free[tmp] = curr
	 

	    -- update rates of active flows
	    local new_rate = tw_max_rate/(num_active_flows)
	    local new_gap = num_active_flows -- max_rate/new_rate, a little slower for headroom
	    -- TODO(lav): linked list of active flows's state?
	    for i=1, max_flows do
	       if (name_flow[i] ~= -1) then
		  gap_flow[i] = new_gap
		  rate_flow[i] = new_rate
	       end
	    end
	 end
      end


      if (cpg_round > 0) then
	 lm_time = lm.getTime()
	 -- Sending data packets
	 bufs:alloc(PKT_LEN)
	 for i, buf in ipairs(bufs) do
	    local pkt = buf:getUdpPacket()
	    local next_flow = tw_slots[tw_current_slot+1]
	    if (next_flow > 0) then
	       -- important to update source MAC
	       -- else will use value in buffer (garbage/ old)
	       -- and possibly drop packet
	       pkt.eth:setSrc(0x253554d4500)
	       pkt.ip4:setSrc(SRC_IP_BASE + (name_flow[next_flow]))
	       nsent_flow[next_flow] = nsent_flow[next_flow] + 1
	       if (nsent_flow[next_flow] == 1) then
		  flow_start[name_flow[next_flow]] = lm_time
	       end
	       -- update timing wheel O(1)
	       tw_slots[tw_current_slot+1] = 0
	       -- reinsert in next empty slot after gap slots
	       if (nsent_flow[next_flow] < size_flow[next_flow]) then	       
		  local next_index = tw_current_slot + gap_flow[next_flow]
		  if (next_index >= tw_num_slots) then
		     next_index = next_index - tw_num_slots
		  end
		  -- worst case will be put back in same slot
		  while (tw_slots[next_index+1] > 0) do
		     next_index = next_index + 1
		     if (next_index == tw_num_slots) then
			next_index = next_index - tw_num_slots
		     end
		  end
		  tw_slots[next_index+1] = next_flow
	       else -- if flow ended 
		  expired_flows = {next = expired_flows, flow_index=next_flow, nsent=size_flow[next_flow]}
		  flow_finish[name_flow[next_flow]] = lm_time
		  --log:info("Finished sending flow " .. name_flow[next_flow] .. " in round " .. cpg_round)
	       end

	    else
	       -- drop void packets
	       pkt.eth:setSrc(dstMacNo)
	       -- so packet is dropped at switch
	    end
	    
	    -- update current tick per packet
	    tw_current_slot = tw_current_slot + 1
	    if (tw_current_slot == tw_num_slots) then
	       assert(tw_current_slot == tw_num_slots)
	       tw_current_slot = 0
	    end
	 end
	 -- send out all packets and frees old bufs that have been sent
	 queue:send(bufs)
      
	 num_sends = num_sends + 1
      end
   end

   for i, name in ipairs(wl_name) do
      size = wl_size[i] or 0
      nsent = wl_nsent[name] or 0
      start = flow_start[name] or 0
      finish = flow_finish[name] or 0
      fct = ((8*nsent*PKT_LEN)/(finish-start))/1e6
      log:info("Flow # " .. i.. ": " .. name .. " 'sent' " .. nsent .. " packet with fct " .. fct .. " Mb/s "
		  .. " from " .. start*1e6 .. " us to " .. finish*1e6 .. " us")
   end

   -- local tmp = prep_start
   -- local prev = 0
   -- while tmp do
   --    local avg_diff = (((tmp.value - prev) * 1e6)/CHECK_TIME_EVERY)
   --    local mbps = ((BATCH_SIZE * PKT_LEN * 8))/avg_diff
   --    --+ (BATCH_SIZE2 * PKT_LEN2 * 8))/avg_diff
   --    log:info(tmp.value * 1e6 .. " us start prep, "
   -- 		  .. avg_diff
   -- 		  .. " us per batch of " .. BATCH_SIZE .. " "
   -- 		  .. PKT_LEN .. " B packets"
   -- 	       .. " for a rate of " .. mbps .. " Mb/s")
   --    -- .. " plus ".. BATCH_SIZE2 .. " " .. PKT_LEN2 .. " B packets"
   --    prev = tmp.value
   --    tmp = tmp.next
   -- end

   -- local tmp = cpg_round_check
   -- local prev = 0
   -- local i = 1
   -- while false and tmp do
   --    local diff = prev - tmp.value
   --    log:info("Cpg round check at " .. tmp.value*1e6 .. " us (" .. (diff*1e6) .. " us after last round check), num_active_flows in previous round " .. tmp.nflows .. " .")
   --    prev = tmp.value
   --    tmp = tmp.next
   --    i = i + 1
   -- end
end


function rxSlave(rxQueue)
	-- a bufArray is just a list of buffers that we will use for batched forwarding
   local flow_start = {}
   flow_start["test"] = 0
   local flow_finish = {}
   local flow_count = {}
   local bufs = memory.bufArray()
   while lm.running() do -- check if Ctrl+c was pressed
      -- receive one or more packets from the queue
      local lm_time = lm.getTime()
      local count = rxQueue:recv(bufs)
      for i = 1, count do
	 local buf = bufs[i]
	 local pkt = buf:getUdpPacket()
	 local src_ip = pkt.ip4:getSrc()
	 local flow_id = src_ip - SRC_IP_BASE
	 if flow_start[flow_id] == nil then
	    flow_start[flow_id] = lm_time
	    flow_count[flow_id] = 1
	 else
	    flow_finish[flow_id] = lm_time
	    flow_count[flow_id] = flow_count[flow_id] + 1
	 end
	 buf:free()
      end
      -- send out all received bufs on the other queue
      -- the bufs are free'd implicitly by this function
      --txQueue:sendN(bufs, count)
   end
   log:info("Packets received")
   for name, start in pairs(flow_start) do
      nsent = flow_count[name] or 0
      finish = flow_finish[name] or 0
      fct = ((8*nsent*PKT_LEN)/(finish-start))/1e6
      log:info("Flow " .. name .. ", received " .. nsent .. " packets with fct " .. fct .. " Mb/s"
		  .. " from " ..  (start*1e6) .. " us to " .. (finish*1e6))


   end
   
end

