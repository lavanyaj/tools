--- A simple UDP packet generator. Generates packets for given flow schedule.
--- Notion of Cpg round = (current time - given start time)/ xx us
--- Flows are added to active list every 4th Cpg round
--- Use circular array of tw_num_slots slots to schedule packets of flows
--- Send BATCH_SIZE packets every loop with (hopefully) few operations/ pkt
--- Current slot in tw_num_slots ticks with each packet in batch.. so if
--- packets are being sent at line rate, a slot = Line rate tx time of pkt.
--- A flow that hasn't finished in re-inserted n slots in the future where
--- n is inter-packet time (actually rounded up to nearest multiple of 1.2us)
--- for flow.
--- Jan 9th: load flow schedule from file. count flow stats at receiver.
---  update all active flow's rates to max_rate/N every 4th Cpg round.
---  log flow stats at Tx and Rx. simple rdtsc check time every iteration.
--- Jan 10th: updated workload to try steady flows, configure rates as C/N every 4 rnds. Logging.
--- Jan 11th: refactored tw and flow state updates, added my_flow and their_flow state for cpg
---   calculations. Added a bunch of code (in comments) for Cpg updates to test incrementally

local lm     = require "libmoon"
local device = require "device"
local stats  = require "stats"
local log    = require "log"
local memory = require "memory"
local arp    = require "proto.arp"
local ip    = require "proto.ip4"
local ethernet    = require "proto.ethernet"
local filter    = require "filter"
local open = io.open
local ceil = math.ceil
local ffi = require "ffi"

local clib = ffi.load("./libfour_bit.so")

ffi.cdef[[
uint8_t hi4(uint8_t x);
uint8_t lo4(uint8_t x);
uint8_t make8(uint8_t hi4, uint8_t lo4);
]]

--ifaddr = {"nf0": {"mac": "02:53:55:4d:45:00", "ip": "1.2.3.4"}
-- eth2      Link encap:Ethernet  HWaddr 0c:c4:7a:b7:60:3c   dev 0
-- eth3      Link encap:Ethernet  HWaddr 0c:c4:7a:b7:60:3d   dev 1
-- set addresses here

local DST_MAC_NO = {}
local DST_MAC = {}
DST_MAC_NO[2] = 8942694572552
DST_MAC[2] = "08:22:22:22:22:08" --"0c:c4:7a:b7:60:3d" --"02:53:55:4d:45:00" -- resolved via ARP on GW_IP or DST_IP, can be overriden with a string here
DST_MAC_NO[1] = 8869393797384
DST_MAC[1] = "08:11:11:11:11:08" --"0c:c4:7a:b7:60:3d" --"02:53:55:4d:45:00" -- resolved via ARP on GW_IP or DST_IP, can be overriden with a string here
--local DST_MAC       = "08:22:22:22:22:08" -- "08:11:11:11:11:08" -- --"02:53:55:4d:45:00" --"0c:c4:7a:b7:60:3d"
--local DST_MAC_NO = 8942694572552 -- 8869393797384 -- 0x81111111108 -- 8942694572552 --0x82222222208
local SRC_IP        = "10.0.0.0" -- "10.0.0.10"
local SRC_IP_BASE   = 167772160 -- "10.0.0" -- "10.0.0.10"
local DST_IP        = "1.2.3.4" --"10.1.0.10"
local SRC_PORT_BASE = 1000 
local DST_PORT      = 1234
local NUM_FLOWS     = 1
local ETH_DROP = 0x1234
local ROUND = 200 * 1e-6
local ONE_WAY_DELAY = 10 * 1e-6
-- checks every 15-20 us .. sends 15 packets every 15-20us enough to send all flows in this time.. and bw??

local PKT_LEN       = 1500
local BATCH_SIZE = 15

local PKT_LEN2       = 128
local BATCH_SIZE2 = 15

local CHECK_TIME_EVERY = 100

local INIT_GAP = 2
local INIT_RATE = 5000 --1000
local INIT_DEMAND = 20000

-- the configure function is called on startup with a pre-initialized command line parser
function configure(parser)
	parser:description("Edit the source to modify constants like IPs and ports.")
	parser:argument("dev", "Devices to use."):args("+"):convert(tonumber)
	parser:option("-t --threads", "Number of threads per device."):args(1):convert(tonumber):default(1)
	return parser:parse()
end

function master(args,...)   
   for i, dev in ipairs(args.dev) do
      -- 3 tx queue
      --  to send data
      --  to send control packets
      --  to echo Rnd 2 control packets at source of flow
      -- 2 rx queues
      --  to recv data and control packets
      --  to recv Rnd 2 control packets at source of flow
      local dev = device.config{
	 port = dev,
	      txQueues = 3,
	      rxQueues = 4,
	      rssQueues = 2	      
      }
      args.dev[i] = dev
      
   end
   device.waitForLinks()

   -- print statistics
   --stats.startStatsTask{devices = args.dev}

   local start_time = lm.getTime() + 1e-3
   log:info("Starting simulation at ", start_time)

   local my_filename = {}
   local their_filename = {}
   my_filename[1] = "examples/flows-small.txt"
   my_filename[2] = ""
   their_filename[1] = ""
   their_filename[2] = "examples/flows-small.txt"
   
   for i = 1, 2 do
      local dev = args.dev[i]
      local queue = dev:getTxQueue(0) -- sending data packets with possible control info
      local queue2 = dev:getTxQueue(1) -- sending extra control packets (for my flows' round 1/3, their flows' round 2/4)
      local txSpQueue = dev:getTxQueue(2) -- to echo- send my flows' round 2 control packets
      local rxQueue = dev:getRxQueue(2) -- receiving control packets (for my flows' round 4, their flows' round 1) 
      local rxSpQueue = dev:getRxQueue(3) -- receiving my flows' round 2 control packets to echo
       -- 0 receiving my flows' data packets and anything that's not my flow's round 4, their round1 
 
      dev:fiveTupleFilter({			 
        	    proto = ip.PROTO_DATA
        			  }, dev:getRxQueue(0))

      dev:fiveTupleFilter({			 
	    proto = ip.PROTO_CONTROL_1
			  }, rxQueue)
      -- dev:fiveTupleFilter({			 
      -- 	    proto = ip.PROTO_CONTROL_4
      -- 			  }, rxQueue)
      dev:fiveTupleFilter({			 
	    proto = ip.PROTO_CONTROL_2
			  }, rxSpQueue)   
      lm.startTask("txSlave", my_filename[i],
		   start_time, rxQueue, rxSpQueue, txSpQueue, queue, queue2, DST_MAC[3-i], DST_MAC_NO[3-i], DST_MAC_NO[i])
      lm.startTask("rxSlave", their_filename[i], start_time, dev:getRxQueue(0))
      
   end
   
   --    local queue = dev:getTxQueue(j - 1)
   --    local queue2 = dev:getTxQueue(j+1 - 1)
   --    local txSpQueue = dev:getTxQueue(j+2 - 1)
   --    local rxQueue = dev:getRxQueue(j - 1)
   --    local rxSpQueue = dev:getRxQueue(j+1 - 1)
   --    dev:fiveTupleFilter({			 
   -- 	    proto = ip.PROTO_ICMP
   -- 			  }, rxSpQueue)


   --    lm.startTask("txSlave", "", "examples/flows-small.txt", 
   -- 		   start_time, rxQueue, rxSpQueue, txSpQueue, queue, queue2,
   -- 		   DST_MAC[3-i], DST_MAC_NO[3-i], DST_MAC_NO[i])

   -- end
   lm.waitForTasks()
end

function loadWorkload(filename)
   local wl = {}
   wl["name"] = {}
   wl["size"] = {}
   wl["start"] = {}
   wl["nsent"] = {}
   wl["nflows"] = 0
   wl["next_flow"] = 0
   wl["removed"] = 0
   if filename == nil then
      log:info("Filename was nil")
      return wl
   end
   
   local file = open(filename)
   local shift_start_us = 1000
   local prev_start = 0
   if file then
      for line in file:lines() do
	 local words = {}
	 word_no = 1
	 for w in  string.gmatch(line, "%S+") do
	    words[word_no] = w
	    word_no = word_no + 1
	    end
	 local size_mtus = ceil(words[1]/1500.0)
	 local index = wl.nflows + 1
	 wl.name[index] = index
	 wl.size[index] = size_mtus
	 wl.start[index] = shift_start_us+words[2]
	 wl.nsent[index] = 0
	 wl.nflows = wl.nflows + 1
	 log:info("flow " .. index .. " has size " .. size_mtus .. " starts at " .. wl.start[index] .. " ("
		     .. (wl.start[index]-prev_start) .. " us after previous flow)")
	 prev_start = wl.start[index]
      end
   end
   if (wl.nflows > 0) then
      wl.next_flow = 1
      end
   return wl
end

function setupTimingWheel(tw_num_slots, default)
   local tw = {}
   tw["num_slots"] = tw_num_slots
   tw["slots"] = {}
   tw["num_free"] = 0
   tw["current_slot"] = 0
   tw["max_rate"] = default["max_rate"] or 10000
   tw["safe_max_rate"] = default["safe_max_rate"] or 8000
   for i = 1, tw.num_slots do
      tw.slots[i] = 0
      tw.num_free = tw.num_free + 1
   end
   return tw
end

function setupDoublyLinkedList(num_items)
   local start_free = 1
   local prev_free = {}
   local next_free = {}
   for i = 1, num_items do
      prev_free[i] = i-1
      next_free[i] = i+1
      if i == 1 then
	 prev_free[i] = num_items
      elseif i == max_flows then
	 next_free[i] = 1
      end
   end
   return prev_free, next_free, start_free
end


function tick(tw)
      tw.current_slot = tw.current_slot + 1
      if (tw.current_slot >= tw.num_slots) then
	 --assert(tw.current_slot == tw.num_slots)
	 tw.current_slot = 0
      end
end

function get_next_free(tw, n)
	       -- reinsert in next empty slot after gap slots
	       -- could take 1000 steps if timing wheel is full
	       -- average case I guess < 10 but check
	       -- TODO(lav): if hotspot, then use linked list
	       -- of free slots (easy to maintain order since
	       -- they are freed in order)
	       -- and binary search to find
	       -- next free slot after current + gap
   --assert(n < tw.num_slots)
   local next_index = tw.current_slot + n
   if (next_index >= tw.num_slots) then
      next_index = next_index - tw.num_slots
   end
   --assert(next_index < tw.num_slots)
   --assert(next_index >= 0)
   
   while (tw.slots[next_index+1] > 0) do
      next_index = next_index + 1
      if (next_index == tw.num_slots) then
	 next_index = next_index - tw.num_slots
      end
   end
   return next_index
end
function cpgResetTheirFlowState(tf)
   tf["num_flows"] = 0
   tf["next_flow"] = 0
   tf["next_hop"] = 0
   tf["last_update"] = 0
   return
end

-- TODO: setupTheirFlowState name, nhops also nflows,
-- and global vars like next_flow, next_hop, last_update
-- all cpg control related state
function setupTheirFlowState()
   local tf = {}
   tf["num_flows"] = 0
   tf["next_flow"] = 0
   tf["next_hop"] = 0
   tf["last_update"] = 0
   tf["flow_id"] = {}
   tf["nhops"] = {}
   return tf
end

-- TODO: add cpg state i.e., demand, stable, last_update
function setupMyFlowState(max_flows,default)
   local mf = {}
   mf["gap"] = {}
   mf["rate"] = {}
   mf["nsent"] = {}
   mf["name"] = {}
   mf["size"] = {}
   mf["flow_index"] = {}
   mf["max_flows"] = max_flows
   mf["num_active"] = 0
   
   -- cpg control state
   mf["demand"] = {}
   mf["stable"] = {}
   mf["last_update"] = {}
   mf["next_flow"] = 0
   mf["global_last_update"] = 0
   for i, field in ipairs(mf) do
      assert(default[field] ~= nil)
      for j = 1, max_flows do
	 mf[field][j] = default[field]
      end
   end
   mf["prev_free"],  mf["next_free"], mf["start_free"]
      = setupDoublyLinkedList(max_flows)
   mf["num_free"] = max_flows
   return mf
end

function removeExpiredFlows(expired_flows, mf, wl)
   -- could avoid per-flow ops here
   -- remove all expired flows
   local curr = mf.start_free
   local tmp = mf.next_free[mf.start_free]
   local ef  = expired_flows 
   while (ef) do
      mf.num_active = mf.num_active - 1
      log:info("Removing flow at my_flows[".. ef.flow_index.. "] with name ".. mf.name[ef.flow_index])
      wl.nsent[mf.name[ef.flow_index]] = ef.nsent
      mf.flow_index[mf.name[ef.flow_index]] = nil
      mf.name[ef.flow_index] = -1
      mf.next_free[curr] = ef.flow_index
      curr = ef.flow_index
      mf.num_free = mf.num_free + 1
      wl.removed = wl.removed + 1
      ef = ef.next
   end
   mf.next_free[curr] = tmp
   mf.prev_free[tmp] = curr		   
end

function addNewFlows(mf, wl, tw, cpg_round)
   -- add new flows, until no more free timeslots or flow state
   local curr = mf.start_free
   local tmp = mf.prev_free[mf.start_free]
   local tw_insert_at = tw.current_slot
		   
   while wl.next_flow <= wl.nflows and
      wl.start[wl.next_flow] <= (cpg_round * ROUND * 1e6)
   and mf.num_free > 0 and tw.num_free > 0 do
      mf.num_active = mf.num_active + 1
      mf.flow_index[wl.name[wl.next_flow]] = curr
      mf.name[curr] = wl.name[wl.next_flow]
      mf.size[curr] = wl.size[wl.next_flow]
      mf.nsent[curr] = 0
      mf.gap[curr] = INIT_GAP
      mf.rate[curr] = INIT_RATE
      mf.stable[curr] = 0
      mf.last_update[curr] = 0
      mf.demand[curr] = INIT_DEMAND
      while tw.slots[tw_insert_at+1] > 0 and tw.num_free > 0 do
	 tw_insert_at = tw_insert_at+1
	 if (tw_insert_at == tw.num_slots) then
	    tw_insert_at = 0
	 end
      end
      -- insert first data packet/ SYN
      -- (also need to update rates of other flows)
      -- always leave room for first data packets.
      --assert(tw.slots[tw_insert_at+1] == 0)
      tw.slots[tw_insert_at+1] = curr
      tw.num_free = tw.num_free - 1
      log:info("Adding flow with name " .. mf.name[curr] .. " in my_flows[" .. curr .. "]"
		  .. ": mf.last_update[".. curr .."] = " .. mf.last_update[curr]
		  .. " at tw.slots[" .. (tw_insert_at+1)
      		  .. " gap " .. mf.gap[curr] .. ", rate " .. mf.rate[curr])
      --	 .. ", scheduled to start " .. wl_start[next_new_flow] .. " us after T=0")
		      
      wl.next_flow = wl.next_flow + 1
      --log:info("updating curr mf slot " .. curr .. " to next free mf slot " .. mf.next_free[curr])
      curr = mf.next_free[curr]
      mf.num_free = mf.num_free - 1
   end
   mf.start_free = curr
   tmp = mf.prev_free[curr]
   mf.next_free[tmp] = curr
end

-- TO TEST
function fillControlPacketsForRound1(bufs2, mf, cpg_round)
   local i = 1
   while i < bufs2.size and mf.next_flow <= mf.max_flows do
      while(mf.next_flow <= mf.max_flows and (mf.last_update[mf.next_flow] == cpg or
					      mf.name[mf.next_flow] == -1)) do
	 mf.next_flow = mf.next_flow+1
      end
      if mf.next_flow <= mf.max_flows then
	 -- fill in demand, stable
	 i = i + 1
      else
	 mf.next_flow = 0
	 mf.global_last_update = cpg_round
      end		  
   end
   return i
end

-- TO TEST
function fillControlPacketsForRound3(bufs2, mf, cpg_round)
   local i = 1
   while i < bufs2.size and mf.next_flow <= mf.max_flows do
      while((mf.next_flow <= mf.max_flows and
		(mf.last_update[mf.next_flow] == cpg or
		 mf.name[mf.next_flow] == -1 or
		 mf.stable[mf.next_flow] == 1))) do
	 mf.next_flow = mf.next_flow+1
      end
      if mf.next_flow <= mf.max_flows then
	 -- fill in demand, stable
	 i = i + 1
      else
	 mf.next_flow = 0
	 mf.global_last_update = cpg_round
      end		  
   end
   return i
end

-- TO TEST
function fillControlPacketsForRound2(bufs2, tf, cpg_round)
   local i = 1
   while i < bufs2.size and tf.last_update < cpg_round do
      -- make a packet for tf.flow_id[tf.next_flow],
      -- and tf.next_hop and inc. next_hop
      if tf.next_hop == tf.nhops[tf.next_flow] then
	 tf.next_flow = tf.next_flow+1
	 tf.next_hop = 0
	 
	 if tf.next_flow > tf.num_flows then
	    tf.last_update = cpg_round
	    tf.next_flow = 0
	 end
	 
      end	       
      i = i + 1
   end
   return i
end

-- TO TEST
function fillControlPacketsForRound4(bufs2, tf, cpg_round)
   local i = 1
   while i < bufs2.size and tf.last_update < cpg_round do
      -- make a packet for tf.flow_id[tf.next_flow],
      tf.next_flow = tf.next_flow+1
      if tf.next_flow > tf.num_flows then
	 tf.last_update = cpg_round
	 tf.next_flow = 0
      end
      i = i + 1
   end
   return i
end

function txSlave(workload_file, start_time, rxQueue, rxSpQueue, txSpQueue, txQueue, txQueue2, dstMac, dstMacNo, myMacNo)
   
   -- Set up 2 memory pool and 4 buffer arrays
   
   -- memory pool with default values for all packets, this is our archetype
   local mempool = memory.createMemPool(function(buf)
	 buf:getIP4Packet():fill{
	    -- fields not explicitly set here are initialized to reasonable defaults
	    ethSrc = txQueue, -- MAC of the tx device
	    ethDst = dstMac,
	    ip4Src = SRC_IP,
	    ip4Dst = DST_IP,
	    pktLength = PKT_LEN
				}
   end)

   local mempool2 = memory.createMemPool(function(buf)
	 buf:getIP4Packet():fill{
	    -- fields not explicitly set here are initialized to reasonable defaults
	    ethSrc = tx2Queue, -- MAC of the tx device
	    ethDst = dstMac,
	    ip4Src = SRC_IP,
	    ip4Dst = DST_IP,
	    pktLength = PKT_LEN2
				}
   end)

   -- a bufArray is just a list of buffers from a mempool that is processed as a single batch
   local bufs = mempool:bufArray(BATCH_SIZE)
   local bufs2 = mempool:bufArray(BATCH_SIZE2)
   local rxBufs = memory.bufArray()
   local fwdBufs = memory.bufArray()

   -- Workload info, by flow index (name, size in packet,
   --  start time in us). Load from workload file.
   local wl = loadWorkload(workload_file)
   if wl.nflows > 0 then
      log:info("TxSlave on dev " .. txQueue.id .. ", queue " .. txQueue.qid .. " loaded workload with " .. wl.nflows .. " flows")
   end
   -- Set up timing wheel one slot = one packet
   local tw = setupTimingWheel(1000, {})
   -- Per flow state for active flows that start here
   local mf = setupMyFlowState(100,
				     {["gap"]= INIT_GAP,
					["rate"] = INIT_RATE,
					["nsent"] = 0,
					["name"] = -1,
					["size"] = 0,
					["demand"] = INIT_DEMAND,
					["stable"] = 0})
   local tf = setupTheirFlowState()
   
   local expired_flows = nil

   -- Per flow state for active flows that start there
   
   -- Cpg rounds start at start_time, tentatively 3ms from now
   -- TODO: Need to sync this across machines so wall clock time
   -- is same, also well as correct for drifts
   local cpg_round = 0
   local cpg_type = -1
   local cpg_left = ROUND
   local prev_cpg_round = -100


   -- for logging configured rates per Cpg round
   local common_gap = 0
   local common_rate = 0
   local prev_round_change_time = 0   
   local prep_start = nil
   local cpg_round_check = nil
   local num_sends = 0
   local flow_start = {}
   local flow_finish = {}
   local num_control_packets = 0

   local num_void = 0
   while lm.running() and wl.removed < wl.nflows
      or (wl.nflows == 0 and num_sends < 10000)
   do
   --while lm.running() do -- check if Ctrl+c was pressed
      local lm_time = lm.getTime()
      cpg_round = ceil((lm_time - start_time)/ROUND)
      cpg_type = cpg_round%4
      cpg_left = start_time + ((cpg_round+1) * ROUND) - lm_time
      -- code to benchmark time taken per batch
      if (num_sends > 0
	  and num_sends % CHECK_TIME_EVERY == 0) then
	 prep_start = {next=prep_start, value=lm_time}
      end

      -- check if cpg round changed
      -- TODO: when round is 1 mod 100 refresh all calculations? and update rates 0 [not in tw]
      if cpg_round > 0 and cpg_round > prev_cpg_round then
	 --log:info("Cpg round " .. cpg_round .. " at time " .. lm_time .. " (" .. (lm_time-start_time)*1e6 .. " us from start)")
	 prev_cpg_round = cpg_round
	 --cpg_round_check = {next=cpg_round_check, value=prev_round_change_time, nflows=mf.num_active, rate=common_rate}
	 prev_round_change_time = lm_time
	 if (cpg_round ~= 0 and cpg_round > prev_cpg_round+1) then
	    log:error("TxSlave " .. myMacNo .. ": Cpg round jumped from "
			 .. prev_cpg_round .. " to "
			 .. cpg_round .. " in one iteration.")
	 end	   
	    
	 if (cpg_round%4 == 1) then
	    -- test = test+1
	    -- tf["num_flows"] = 0
	    -- tf["next_flow"] = 0
	    -- tf["next_hop"] = 0
	    -- tf["last_update"] = 0
	    -- -- update tf.num_flows, tf.next_flow, tf.next_hop to 0 every round 1
	    cpgResetTheirFlowState(tf)
	 end
	    -- no change to mf.xx
	    
	 -- on reset, we update set of flows and set demand to inf., stable to false, last_update to 0
	 if (cpg_round%100 == 1) then
	    tw = setupTimingWheel(1000, {})
	    -- now we'll make this part of resetting Cpg calculatiosn
	    local old_num_active = mf.num_active
	    if (expired_flows ~= nil) then
	       removeExpiredFlows(expired_flows, mf, wl)
	       mf.global_last_update = 5
	       expired_flows = nil
	    end
	    if (wl.next_flow < wl.nflows) then
	       addNewFlows(mf, wl, tw, cpg_round)
	    end
	 end

	 -- TODO: INITIAL RATES AND HANDLING RATE 0	 
      end

      if (cpg_round > 0) then
	 -- Receiving data and control packets

	 lm_time = lm.getTime()
	 -- Sending data packets
	 bufs:alloc(PKT_LEN)
	 for i, buf in ipairs(bufs) do
	    local pkt = buf:getIP4Packet()
	    local next_flow = tw.slots[tw.current_slot+1]
	    if (next_flow > 0) then
	       -- Fill in data packet
	       local name = mf.name[next_flow]
	       local num_sent = mf.nsent[next_flow] + 1
	       local size = mf.size[next_flow]
	       -- important to update source MAC
	       -- else will use value in buffer (garbage/ old)
	       -- and possibly drop packet
	       pkt.eth:setSrc(0x253554d4500)
	       pkt.ip4:setSrc(SRC_IP_BASE + name)
	       mf.nsent[next_flow] = num_sent
	       if (num_sent == 1) then
		  flow_start[name] = lm_time
	       end
	       pkt.ip4:setProtocol(ip.PROTO_DATA)
	       --Add control info. TO TEST.
	       -- looks like it's recvd every 3 rounds (should be 4) for a 200us round
	       -- demand and stable match up what I fill
	       -- haven't cross-checked packet bytes with SCAPY tests
	       -- works with rx slave on other side
	       if ((cpg_type == 1 or
		       (cpg_type==3 and mf.stable[next_flow]==0))
		     and mf.last_update[next_flow] < cpg_round) then
--		   and cpg_left > ONE_WAY_DELAY) then
		  --log:info("Adding control info for "
		  -- 	      .. next_flow .. " found at slot "
		  -- 	      .. tw.current_slot .. " with "
		  -- 	      .. " flow "
		  -- 	      .. mf.name[next_flow])
		  num_control_packets = num_control_packets+1
		  if (cpg_type == 1) then		    
		     pkt.ip4:setProtocol(ip.PROTO_CONTROL_1)
		  else
		     pkt.ip4:setProtocol(ip.PROTO_CONTROL_3)
		  end
	       	  pkt.payload.uint64[0] = mf.demand[next_flow]
		  -- hop * 16 + stable = stable
		  pkt.payload.uint8[8] = mf.stable[next_flow]
	       	  mf.last_update[next_flow] = cpg_round
		  --buf:dump()
	       end
	    
	       
	       -- Update timing wheel O(1)
	       tw.slots[tw.current_slot+1] = 0
	       -- TODO: INITIAL RATES AND HANDLING RATE 0
	       assert(mf.gap[next_flow] ~= nil)
	       assert(mf.gap[next_flow] < tw.num_slots)
	       if (num_sent < size) then	       
		  local next_index = get_next_free(tw, mf.gap[next_flow])		  
		  tw.slots[next_index+1] = next_flow
	       else -- if flow ended 
		  expired_flows =
		     {next = expired_flows, flow_index=next_flow, nsent=size}
		  flow_finish[name] = lm_time
		  --log:info("Finished sending flow " .. name_flow[next_flow] .. " in round " .. cpg_round)
	       end
	       
	    else
	       -- Drop void packets
	       pkt.eth:setSrc(dstMacNo)
	       pkt.ip4:setSrc(SRC_IP_BASE)
	       -- so packet is dropped at switch
	    end
	    
	    -- update current tick per packet
	    --tick(tw)
	    tw.current_slot = tw.current_slot + 1
	    if (tw.current_slot == tw.num_slots) then
	       --assert(tw.current_slot == tw.num_slots)
	       tw.current_slot = 0
	    end
	 end
	 -- send out all packets and frees old bufs that have been sent
	 txQueue:send(bufs)


	 -- send control packets (how many?) TO TEST
	 -- if cpg_left > ONE_WAY_DELAY
	 --    and (
	 --       (cpg_type%2 == 1 and mf.global_last_update < cpg_round)
	 -- 	  or (cpg_type%2 == 0 and tf.global_last_update < cpg_round))
	 -- then
	 --    -- different criteria for each kind of round
	 --    bufs2:alloc(PKT_LEN2)
	 --    local count = 0
	 --    if (cpg_type == 1) then
	 --       count = fillControlPacketsForRound1(bufs2, mf, cpg_round)
	 --    elseif (cpg_type == 2) then
	 --       count = fillControlPacketsForRound2(bufs2, tf, cpg_round)
	 --    elseif (cpg_type == 3) then
	 --       count = fillControlPacketsForRound3(bufs2, mf, cpg_round)
	 --    elseif (cpg_type == 4) then
	 --       count = fillControlPacketsForRound4(bufs2, tf, cpg_round)
	 --    end
	 --    if count > 0 then
	 --       txQueue:sendN(bufs2, count)
	 --    end
	 -- end

	 -- recv control and data packets. TO TEST

	 local count = rxQueue:tryRecv(rxBufs, 10)
	 local data = 0
	 if count ~= nil and count > 0 then
	    for i = 1, count do
	       local buf = rxBufs[i]
	       local pkt = buf:getIP4Packet()
	       local proto = pkt.ip4:getProtocol()
	       local src_mac_no = pkt.eth:getSrc()
	       local src_ip = pkt.ip4:getProtocol()
	       local flow_id = src_ip - SRC_IP_BASE
	       if (proto == ip.PROTO_DATA) then
		  data = data + 1
		  end
	       if (proto == ip.PROTO_CONTROL_1 and src_mac_no ~= myMacNo) then
		  tf.num_flows = tf.num_flows+1
		  tf.flow_id[tf.num_flows] = flow_id
		  tf.nhops[tf.num_flows] = clib.hi4(pkt.payload.uint8[8])
	       elseif (proto == ip.PROTO_CONTROL_4 and src_mac_no == myMacNo) then
		  mf.stable[mf.flow_index[flow_id]] = pkt.payload.uint8[0]
	       end
	       buf:free()
	    end
	    log:info("TxSlave " .. myMacNo .. ": received " .. data .. " data packets.")
	 end

	 -- TODO: In debugging mode, assert that all
	 --  the right flows have been updated in each round
	 
	 -- round 2 echo, TO TEST
	 -- if cpg_round%2 == 0 then
	 --    local count = rxSpQueue:tryRecv(fwdBufs)
	 --    if count >= 1 then
	 --       for i = 1, count do
	 -- 	  local buf = fwdBufs[i]
	 --  	  local pkt = buf:getCpgPacket()
	 -- 	  local flowId = pkt.ip4:getSrc() - SRC_BASE_IP
	 -- 	  local rate = ip.LINK_CAPACITY
	 -- 	  local numUnsat = pkt.getNumFlows() - pkt.cpg.getNumSat()
	 -- 	  local sumSat = pkt.cpg.getSumSat() -- TODO int to float
	 -- 	  if numUnsat > 0 then
	 -- 	     rate = (ip.LINK_CAPACITY - sumSat)/numUnsat
	 -- 	  end
	 -- 	  -- TODO: float to int
	 -- 	  pkt.setSumSat(rate)
	 -- 	  local flow_index = mf.flow_index[flowId]
	 -- 	  if (rate < mf.demand[flow_index]) then
	 -- 	     mf.demand[flow_index] = rate
	 -- 	  end
	 --       end
	 --       txSpQueue:sendN(fwdBufs, count)
	 --    end
	 -- end 
	 num_sends = num_sends + 1
      end
   end

   for i, name in ipairs(wl.name) do
      size = wl.size[i] or 0
      nsent = wl.nsent[name] or 0
      start = flow_start[name] or 0
      finish = flow_finish[name] or 0
      fct = ((8*nsent*PKT_LEN)/(finish-start))/1e6
      log:info("TxSlave " .. myMacNo .. ": Flow # " .. i.. ": " .. name .. " 'sent' " .. nsent .. " packet with fct " .. fct .. " Mb/s "
		  .. " from " .. start*1e6 .. " us to " .. finish*1e6 .. " us")
   end
   log:info(num_control_packets .. " packets had control info")


   --log:info("number of valid packets received " .. valid)

   lm.stop()
   
   -- local tmp = prep_start
   -- local prev = 0
   -- while tmp do
   --    local avg_diff = (((tmp.value - prev) * 1e6)/CHECK_TIME_EVERY)
   --    local mbps = ((BATCH_SIZE * PKT_LEN * 8))/avg_diff
   --    --+ (BATCH_SIZE2 * PKT_LEN2 * 8))/avg_diff
   --    log:info(tmp.value * 1e6 .. " us start prep, "
   -- 		  .. avg_diff
   -- 		  .. " us per batch of " .. BATCH_SIZE .. " "
   -- 		  .. PKT_LEN .. " B packets"
   -- 	       .. " for a rate of " .. mbps .. " Mb/s")
   --    -- .. " plus ".. BATCH_SIZE2 .. " " .. PKT_LEN2 .. " B packets"
   --    prev = tmp.value
   --    tmp = tmp.next
   -- end

   -- local tmp = cpg_round_check
   -- local rev = nil
   -- while tmp do
   --    rev = {next=rev, value=tmp.value, nflows=tmp.nflows, rate=tmp.rate}
   --    tmp = tmp.next
   -- end

   -- tmp = rev
   -- while tmp do
   --    --log:info("Cpg round check at " .. tmp.value*1e6 .. " us (" .. (diff*1e6) .. " us after last round check), num_active_flows in previous round " .. tmp.nflows .. " .")
   --    log:info((tmp.value-start_time)*1e6 .. " us, num_active_flows " .. tmp.nflows .. ", each with rate "
   -- 	 .. tmp.rate .. " Mb/s for total " .. (tmp.nflows*tmp.rate) .. " Mb/s .")
   --    tmp = tmp.next
   -- end

end


function rxSlave(workload_file, start_time, rxQueue)
   -- a bufArray is just a list of buffers that we will use for batched forwarding
   local flow_start = {}
   local flow_finish = {}
   local flow_count = {}
   local bufs = memory.bufArray()
   local control_rx = nil
   local control_rx_size = 0
   local valid = 0

   local twl = loadWorkload(workload_file)
   if twl.nflows > 0 then
      log:info("rxSlave on dev " .. rxQueue.id .. ", queue " .. rxQueue.qid .. " loaded workload with " .. twl.nflows .. " flows")
   end
   local cpg_round = 0
   local cpg_left = ROUND
   local batch_times = nil
   while lm.running() do -- check if Ctrl+c was pressed
      -- receive one or more packets from the queue
      local lm_time = lm.getTime()
      batch_times = {next=batch_times, value=lm_time}
      local count = rxQueue:recv(bufs)
      if count > 0 then
	 for i = 1, count do
	    local buf = bufs[i]
	    local pkt = buf:getIP4Packet()
	    local src_ip = pkt.ip4:getSrc()
	    local flow_id = src_ip - SRC_IP_BASE
	    if flow_id > 0 then
	       valid = valid + 1
	    end
	    local proto = pkt.ip4:getProtocol()
	    if proto ~= ip.PROTO_DATA then
	       if (proto == ip.PROTO_CONTROL_1) then
		  local hop_stable = pkt.payload.uint8[8]
		  local hop = clib.hi4(hop_stable)
		  local stable = clib.lo4(hop_stable)
		  control_rx = {next=control_rx, proto=proto,
				demand=pkt.payload.uint64[0],
				hop=hop,
				stable=stable,
				time=lm.getTime()}
				
		  
	       elseif (proto == ip.PROTO_CONTROL_3) then
		  control_rx = {next=control_rx, proto=proto,
				demand=pkt.payload.uint64[0],
				stable=pkt.payload.uint8[8],
				time=lm.getTime()}
				
		  
	       end
	       
	       --log:info("Rx got a control packet from " .. flow_id)
	       --buf:dump()
	    end
	    if flow_start[flow_id] == nil then
	       flow_start[flow_id] = lm.getTime()
	       flow_count[flow_id] = 1
	    else
	       flow_count[flow_id] = flow_count[flow_id] + 1
	       if flow_count[flow_id] == twl.size[flow_id] then
		  flow_finish[flow_id] = lm.getTime()
	       end
	    end	 
	    buf:free()
	 end
      end
      -- send out all received bufs on the other queue
      -- the bufs are free'd implicitly by this function
      --txQueue:sendN(bufs, count)
   end

   -- local c = batch_times
   -- local b = nil
   -- while c do
   --    b = {next=b, value=c.value}
   --    c = c.next
   -- end
   
   -- local j = 0
   -- local prev = 0
   -- local diff = 0
   -- while(b and j < 100) do
   --    diff = ((b.value-prev)*1e6)
   --    log:info("Batch " .. j .. " took time " .. b.value .. " (gap: " .. diff .. " us)")
   --    prev = b.value
   --    b = b.next
   --    j = j + 1
   -- end
   
   local i = 0
   local tmp = control_rx
   local ordered = nil
   while tmp do
      ordered = {next=ordered,
		 proto=tmp.proto,
		 demand=tmp.demand,
		 hop=tmp.hop,
		 stable=tmp.stable,
		 time=tmp.time}
      tmp = tmp.next
   end
   --log:info("start time was " .. start_time .. ", ROUND is " .. ROUND)
   tmp = ordered
   while(tmp) do
      if (tmp.proto == ip.PROTO_CONTROL_1) then
	 local hop = tmp.hop or 1234
	 local stable = tmp.stable or 1234
	 log:info("proto: " .. tonumber(tmp.proto)
		     .. ", demand: " .. tonumber(tmp.demand)
		     .. ", hop: " .. tonumber(hop)
		     .. ", stable: " .. tonumber(stable)
		     .. ", time: " .. (tmp.time)
		     .. ", cpg_round: " .. ((tmp.time - start_time)/ROUND))
      elseif (tmp.proto == ip.PROTO_CONTROL_3) then
	 log:info("proto: " .. (tmp.proto)
		     .. ", demand: " .. tonumber(tmp.demand)
		     .. ", stable: " .. tonumber(tmp.stable)
		     .. ", time: " .. (tmp.time)
		     .. ", cpg_round: " .. ((tmp.time - start_time)/ROUND))
      else
	 log:info("proto: " .. (tmp.proto))
      end
      tmp = tmp.next
      i = i + 1
   end
   
   --log:info("Packets received")
   for name, start in pairs(flow_start) do
      nsent = flow_count[name] or 0
      finish = flow_finish[name] or 0
      fct = ((8*nsent*PKT_LEN)/(finish-start))/1e6
      log:info("Flow " .. name .. ", received " .. nsent .. " packets with fct " .. fct .. " Mb/s"
		  .. " from " ..  (start*1e6) .. " us to " .. (finish*1e6))
   end

   --log:info("number of valid packets received " .. valid)
   lm.stop()
end



-- Headers
-- class R1_control(Packet):
--    name = "R1_control"
-- fields_desc = [
--    BitField("demand", 0, 64)
--    BitField("hop", 0, 8),
--    BitField("stable", 0, 8),
-- 	      ]

-- class R2_rvs_control(Packet):
--    name = "R2_rvs_control"
-- fields_desc = [
--    BitField("hop", 0, 8),
--    BitField("linkID", 0, 8),
--    BitField("sumSat", 0, 64),
--    BitField("numSat", 0, 32),
--    BitField("numFlows", 0, 32)
-- 	      ]

-- class R2_fwd_control(Packet):
--    name = "R2_fwd_control"
-- fields_desc = [
--    BitField("hop", 0, 8),
--    BitField("linkID", 0, 8),
--    BitField("fairShareRate", 0, 64)
-- 	      ]

-- class R3_control(Packet):
--    name = "R3_control"
-- fields_desc = [
--    BitField("bottleneckRate", 0, 64)
--    BitField("stable", 0, 8),
-- 	      ]

-- class R4_control(Packet):
--    name = "R4_control"
-- fields_desc = [
--    BitField("stable", 0, 8)
-- 	      ]
