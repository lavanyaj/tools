--- A simple UDP packet generator
local lm     = require "libmoon"
local device = require "device"
local stats  = require "stats"
local log    = require "log"
local memory = require "memory"
local arp    = require "proto.arp"

--ifaddr = {"nf0": {"mac": "02:53:55:4d:45:00", "ip": "1.2.3.4"}
-- eth2      Link encap:Ethernet  HWaddr 0c:c4:7a:b7:60:3c   dev 0
-- eth3      Link encap:Ethernet  HWaddr 0c:c4:7a:b7:60:3d   dev 1
-- set addresses here
local DST_MAC       = "0c:c4:7a:b7:60:3d"
local PKT_LEN       = 1500
local SRC_IP        = "10.0.0.0" -- "10.0.0.10"
local SRC_IP_BASE   = 167772160 -- "10.0.0" -- "10.0.0.10"
local DST_IP        = "1.2.3.4" --"10.1.0.10"
local SRC_PORT_BASE = 1000 
local DST_PORT      = 1234
local NUM_FLOWS     = 1


-- the configure function is called on startup with a pre-initialized command line parser
function configure(parser)
	parser:description("Edit the source to modify constants like IPs and ports.")
	parser:argument("dev", "Devices to use."):args("+"):convert(tonumber)
	parser:option("-t --threads", "Number of threads per device."):args(1):convert(tonumber):default(1)
	return parser:parse()
end

function master(args,...)
   -- configure devices and queues
   -- dev 0 generates packets
   -- dev 1 receives packets and echoes them
   local arpQueues = {}
   for i, dev in ipairs(args.dev) do
	   -- arp needs extra queues
	   local dev = device.config{
	      port = dev,
	      txQueues = args.threads,
	      rxQueues = args.threads,
	      rssQueues = args.threads
	   }
	   args.dev[i] = dev
   end
   device.waitForLinks()

   -- print statistics
   stats.startStatsTask{devices = args.dev}

   -- configure tx rates and start transmit slaves
   dev = args.dev[1]
   for i = 1, args.threads do
      local queue = dev:getTxQueue(i - 1)
      lm.startTask("txSlave", queue, DST_MAC)
   end

   dev = args.dev[2]
   for i = 1, args.threads do
      lm.startTask("forward", dev:getRxQueue(i - 1), dev:getTxQueue(i - 1))
   end
   lm.waitForTasks()
end

function txSlave(queue, dstMac)

   
   -- memory pool with default values for all packets, this is our archetype
   local mempool = memory.createMemPool(function(buf)
	 buf:getUdpPacket():fill{
	    -- fields not explicitly set here are initialized to reasonable defaults
	    ethSrc = dstMac,--queue, -- MAC of the tx device
	    ethDst = dstMac,
	    ip4Src = SRC_IP,
	    ip4Dst = DST_IP,
	    udpSrc = SRC_PORT,
	    udpDst = DST_PORT,
	    pktLength = PKT_LEN
				}
   end)

   local r = 0
   local rates = {2000, 5000, 8000}
   local next_rate = rates[r+1]
   -- a bufArray is just a list of buffers from a mempool that is processed as a single batch
   local bufs = mempool:bufArray()
   local num_sends = 0
   while num_sends < 10 and lm.running() do -- check if Ctrl+c was pressed
      if (num_sends%1 == 0) then
	 -- if (next_rate == nil) then
	 --    log:info("time to update rate: next rate is nil")
	 -- else
	 --    log:info("time to update rate: next rate is %d Mb/s", next_rate)
	 -- end
	 -- assert(next_rate~=nil)
	 --log:info("setting rate to %d Mb/s", next_rate)
	 queue:setRate(next_rate)
	 r = (r + 1)%3
	 next_rate = rates[r+1]
	 -- if (next_rate == nil) then
	 --    log:info("just updated rate: next rate is nil")
	 -- else
	 --    log:info("just updated rate: next rate is %d Mb/s", next_rate)
	 -- end
      end
      bufs:alloc(PKT_LEN)
      for i, buf in ipairs(bufs) do
	 local pkt = buf:getUdpPacket()
	 pkt.ip4:setSrc(SRC_IP_BASE + (i%255))
      end
      -- send out all packets and frees old bufs that have been sent
      queue:send(bufs)
      num_sends = num_sends + 1
   end
end


function forward(rxQueue, txQueue)
	-- a bufArray is just a list of buffers that we will use for batched forwarding
	local bufs = memory.bufArray()
	while lm.running() do -- check if Ctrl+c was pressed
		-- receive one or more packets from the queue
		local count = rxQueue:recv(bufs)
		-- send out all received bufs on the other queue
		-- the bufs are free'd implicitly by this function
		txQueue:sendN(bufs, count)
	end
end

