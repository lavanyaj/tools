--- A simple UDP packet generator. Generates packets for given flow schedule.
--- Notion of Cpg round = (current time - given start time)/ xx us
--- Flows are added to active list every 4th Cpg round
--- Use circular array of tw_num_slots slots to schedule packets of flows
--- Send BATCH_SIZE packets every loop with (hopefully) few operations/ pkt
--- Current slot in tw_num_slots ticks with each packet in batch.. so if
--- packets are being sent at line rate, a slot = Line rate tx time of pkt.
--- A flow that hasn't finished in re-inserted n slots in the future where
--- n is inter-packet time (actually rounded up to nearest multiple of 1.2us)
--- for flow.
--- Jan 9th: load flow schedule from file. count flow stats at receiver.
---  update all active flow's rates to max_rate/N every 4th Cpg round.
---  log flow stats at Tx and Rx. simple rdtsc check time every iteration.
--- Jan 10th: updated workload to try steady flows, configure rates as C/N every 4 rnds. Logging.
--- Jan 11th: refactored tw and flow state updates, added my_flow and their_flow state for cpg
---   calculations. Added a bunch of code (in comments) for Cpg updates to test incrementally

local lm     = require "libmoon"
local device = require "device"
local stats  = require "stats"
local log    = require "log"
local memory = require "memory"
local arp    = require "proto.arp"
local ip    = require "proto.ip4"
local cpg = require "proto.cpg"
local ethernet    = require "proto.ethernet"
local filter    = require "filter"
local open = io.open
local ceil = math.ceil
local ffi = require "ffi"
local max = math.max

local clib = ffi.load("four_bit")

ffi.cdef[[
uint8_t hi4(uint8_t x);
uint8_t lo4(uint8_t x);
uint8_t make8(uint8_t hi4, uint8_t lo4);
uint32_t make32from16(uint16_t hi16, uint16_t lo16);
double makeDoublefrom16(uint16_t w1_16, uint16_t w2_16, uint16_t w3_16, uint16_t w4_16);
]]


--ifaddr = {"nf0": {"mac": "02:53:55:4d:45:00", "ip": "1.2.3.4"}
-- eth2      Link encap:Ethernet  HWaddr 0c:c4:7a:b7:60:3c   dev 0
-- eth3      Link encap:Ethernet  HWaddr 0c:c4:7a:b7:60:3d   dev 1
-- set addresses here

-- for every packet must set SRC_MAC SENDER/RECEIVER, SRC_IP to flow_id, DST_MAC to dstMac, DST_IP to SENDER/RECEIVER*256+protocol, proto
local DST_MAC_NO = {}
local DST_MAC = {}
DST_MAC_NO[2] = 8942694572552
DST_MAC[2] = "08:22:22:22:22:08" --"0c:c4:7a:b7:60:3d" --"02:53:55:4d:45:00" -- resolved via ARP on GW_IP or DST_IP, can be overriden with a string here
DST_MAC_NO[1] = 8869393797384
DST_MAC[1] = "08:11:11:11:11:08" --"0c:c4:7a:b7:60:3d" --"02:53:55:4d:45:00" -- resolved via ARP on GW_IP or DST_IP, can be overriden with a string here
--local DST_MAC       = "08:22:22:22:22:08" -- "08:11:11:11:11:08" -- --"02:53:55:4d:45:00" --"0c:c4:7a:b7:60:3d"
--local DST_MAC_NO = 8942694572552 -- 8869393797384 -- 0x81111111108 -- 8942694572552 --0x82222222208
local SRC_IP        = "10.0.0.0" -- "10.0.0.10"
local SRC_IP_BASE   = 167772160 -- "10.0.0" -- "10.0.0.10"
local DST_IP        = "1.2.3.4" --"10.1.0.10"
local SRC_PORT_BASE = 1000 
local DST_PORT      = 1234
local NUM_FLOWS     = 1
local ETH_DROP = 0x1234
local ROUND = 2e-3
local ONE_WAY_DELAY = 6e-4
-- checks every 15-20 us .. sends 15 packets every 15-20us enough to send all flows in this time.. and bw??

local TIMESTAMP_FOLLOWS = 0xbadcab
local CPG_ROUND_FOLLOWS = 0xdabada
local UINT32_MAX = 4294967295
local PKT_LEN       = 1500
local BATCH_SIZE = 15
-- BATCH_SIZE 31 gives line rate data but control packet RTTs can be in seconds!? and many packets out of round
-- BATCH_SIZE 15 gives 8.4 Gb/s rate data and more packets in correct round than not

local PKT_LEN2       = 128
local BATCH_SIZE2 = 15

local CHECK_TIME_EVERY = 100

local INIT_GAP = 60
local INIT_RATE = 177 --5000 --3333 --1000
local INIT_DEMAND = 20000
local LINK_CAPACITY = 8000

local MAC_FROM_SENDER = 0xaabbccddeeff
local MAC_FROM_RECEIVER = 0x112233445566
local FROM_SENDER = 5
local FROM_RECEIVER = 6

local kind = {}
kind[200] = "ip.PROTO_CONTROL_1"
kind[201] = "ip.PROTO_CONTROL_2_rvs"
kind[202] = "ip.PROTO_CONTROL_2_fwd"
kind[203] = "ip.PROTO_CONTROL_3"
kind[204] = "ip.PROTO_CONTROL_4"
kind[205] = "ip.PROTO_CONTROL_STATS"
kind[206] = "ip.PROTO_DATA"

	
-- the configure function is called on startup with a pre-initialized command line parser
function configure(parser)
	parser:description("Edit the source to modify constants like IPs and ports.")
	parser:argument("dev", "Devices to use."):args("+"):convert(tonumber)
	parser:option("-t --threads", "Number of threads per device."):args(1):convert(tonumber):default(1)
	return parser:parse()
end

function master(args,...)   
   for i, dev in ipairs(args.dev) do
      -- 3 tx queue
      --  to send data
      --  to send control packets
      --  to echo Rnd 2 control packets at source of flow
      -- 2 rx queues
      --  to recv data and control packets
      --  to recv Rnd 2 control packets at source of flow
      local dev = device.config{
	 port = dev,
	      txQueues = 4,
	      rxQueues = 4,
      }
      args.dev[i] = dev
      
   end
   device.waitForLinks()

   -- print statistics
   -- stats.startStatsTask{devices = args.dev}

   local start_time = getTimeFast() + ROUND
   log:info("Starting simulation at ", start_time)

   local lm_time_start = lm.getTime()
   local mt_start = getMonotonicTime()
   local offset_start = lm_time_start-mt_start
   log:info("current clock time " .. mt_start .. " seconds")
   log:info("current+1 time " .. lm_time_start .. " seconds")
   log:info("offset " .. offset_start .. " seconds")

   local my_filename = {}
   local their_filename = {}
   my_filename[1] = "examples/flows-small.txt"
   my_filename[2] = ""
   their_filename[1] = ""
   their_filename[2] = "examples/flows-small.txt"
   
   for i = 1, 2 do
      local dev = args.dev[i]
      local queue = dev:getTxQueue(0) -- sending data packets with possible control info
      local queue1 = dev:getTxQueue(1) -- sending control packets for my flows' round 1/3
      local queue2 = dev:getTxQueue(2) -- sending control packets for my flows round 2
      
      local queue4 = dev:getTxQueue(3) -- sending control packets for their flows' rounds 2/ 4 by rxSlave

      local rxQueue1 = dev:getRxQueue(1) -- receiving control packets for my flows' round 4
      local rxQueue2 = dev:getRxQueue(2) -- receiving control packets for my flows' round 2

      local rxQueue3 = dev:getRxQueue(3) -- receiving control packets for flows' round 1 and 2
      local rxQueue = dev:getRxQueue(0) -- receiving data packets
      -- and control packets for their flows' round 1 (by rxSlave)
      -- TODO: could have separate queues for control and data by putting protocol in src_ip and adding a prio 2 filter that
      --  ignores dst_ip, matches only on src_ip. Also means to get flow ID, we must get lower 8 bits of dst_ip.

      -- RX queues handled by txSlave, to receive my flows' control packets
      assert(UINT32_MAX ~= nil)
      ok1 = dev:fiveTupleFilter({dstIp=bswap(SRC_IP_BASE+FROM_RECEIVER*256+ip.PROTO_CONTROL_4), dstIpMask=UINT32_MAX}, rxQueue1)
      ok2 = dev:fiveTupleFilter({dstIp=bswap(SRC_IP_BASE+FROM_RECEIVER*256+ip.PROTO_CONTROL_2_rvs), dstIpMask=UINT32_MAX}, rxQueue2)
      ok3 = dev:fiveTupleFilter({dstIp=bswap(SRC_IP_BASE+FROM_SENDER*256+ip.PROTO_CONTROL_1), dstIpMask=UINT32_MAX}, rxQueue3)
      ok4 = dev:fiveTupleFilter({dstIp=bswap(SRC_IP_BASE+FROM_SENDER*256+ip.PROTO_CONTROL_2_fwd), dstIpMask=UINT32_MAX}, rxQueue3)

      assert(ok1)
      assert(ok2)
      assert(ok3)
      assert(ok4)
      -- all other packets received go to default queue 0, to be handled
      --  by rx slave (their flows' control packets for round 1, 2, 3 [ignore] and their flows' data packets)
      log:info("dumping filters for dev " .. i)
      dev:dumpFilters()
      math.randomseed(os.time())
      run_id = math.ceil(math.random()*UINT32_MAX)

      log:info("Run_id " .. run_id)
      lm.startTask("txSlave", my_filename[i],
		   start_time, rxQueue1, rxQueue2, queue, queue1, queue2, DST_MAC[3-i], DST_MAC_NO[3-i], DST_MAC_NO[i], i*100+10, run_id)
      
      lm.startTask("rxSlave1", their_filename[i], start_time, rxQueue, nil, DST_MAC[3-i], DST_MAC_NO[3-i], DST_MAC_NO[i], i*100+20, run_id)
      lm.startTask("rxSlave1", their_filename[i], start_time, rxQueue3, queue4, DST_MAC[3-i], DST_MAC_NO[3-i], DST_MAC_NO[i], i*100+30, run_id)

   end

   lm.waitForTasks()
end

function getTimeFast()
   return getMonotonicTime()
end

function loadWorkload(filename)
   local wl = {}
   wl["name"] = {}
   wl["size"] = {}
   wl["start"] = {}
   wl["nsent"] = {}
   wl["nflows"] = 0
   if filename == nil then
      log:info("Filename was nil")
      return wl
   end
   
   local file = open(filename)

   local prev_start = 0
   if file then
      for line in file:lines() do
	 local words = {}
	 word_no = 1
	 for w in  string.gmatch(line, "%S+") do
	    words[word_no] = w
	    word_no = word_no + 1
	    end
	 local size_mtus = ceil(words[1]/1500.0)
	 local index = wl.nflows + 1
	 wl.name[index] = index
	 wl.size[index] = size_mtus
	 wl.start[index] = tonumber(words[2])
	 wl.nsent[index] = 0
	 wl.nflows = wl.nflows + 1
	 log:info("flow " .. index .. " has size " .. size_mtus .. " starts at " .. wl.start[index] .. " ("
		     .. (wl.start[index]-prev_start) .. " us after previous flow)")
	 prev_start = wl.start[index]
      end
   end
   return wl
end

function setupTimingWheel(tw_num_slots, default)
   local tw = {}
   tw["num_slots"] = tw_num_slots
   tw["slots"] = {}
   tw["num_free"] = 0
   tw["current_slot"] = 0
   tw["max_rate"] = default["max_rate"] or 10000
   tw["safe_max_rate"] = default["safe_max_rate"] or 8000
   for i = 1, tw.num_slots do
      tw.slots[i] = 0
      tw.num_free = tw.num_free + 1
   end
   return tw
end


function tick(tw)
      tw.current_slot = tw.current_slot + 1
      if (tw.current_slot >= tw.num_slots) then
	 --assert(tw.current_slot == tw.num_slots)
	 tw.current_slot = 0
      end
end

function get_next_free(tw, n)
   --assert(n < tw.num_slots)
   local next_index = tw.current_slot + n
   if (next_index >= tw.num_slots) then
      next_index = next_index - tw.num_slots
   end
   --assert(next_index < tw.num_slots)
   --assert(next_index >= 0)   
   while (tw.slots[next_index+1] > 0) do
      next_index = next_index + 1
      if (next_index == tw.num_slots) then
	 next_index = next_index - tw.num_slots
      end
   end
   return next_index
end

function cpgResetTheirFlowState(tf)
   tf["num_flows"] = 0
   tf["next_flow"] = 1
   tf["next_hop"] = 0
   tf["last_update"] = 0
   return
end

-- TODO: setupTheirFlowState name, nhops also nflows,
-- and global vars like next_flow, next_hop, last_update
-- all cpg control related state
function setupTheirFlowState()
   local tf = {}
   tf["num_flows"] = 0
   tf["next_flow"] = 1
   tf["next_hop"] = 0
   tf["last_update"] = 0
   tf["flow_id"] = {}
   tf["nhops"] = {}
   return tf
end

-- TODO: add cpg state i.e., demand, stable, last_update
function setupMyFlowState(wl, tw, default)
   local mf = {}
   mf["gap"] = {}
   mf["rate"] = {}
   mf["nsent"] = {}
   mf["name"] = {}
   mf["size"] = {}
   mf["flow_index"] = {}
   
   -- cpg control state
   mf["demand"] = {}
   mf["stable"] = {}
   mf["last_update"] = {}
   mf["next_flow"] = 1
   mf["global_last_update"] = 0

   local tw_insert_at = 0
   assert(tw_insert_at == 0)
   for j= 1, wl.nflows do
      assert(tw.num_free > 0)
      mf["gap"][j] = default["gap"]
      mf["rate"][j] = default["rate"]
      mf["nsent"][j] = 0
      mf["name"][j] = wl.name[j]
      mf["size"][j] = wl.size[j]
      mf["flow_index"][wl.name[j]] = j
      mf["demand"][j] = default["demand"]
      mf["stable"][j] = default["stable"]
      mf["last_update"][j] = 0
      assert(tw.slots[tw_insert_at+1] == 0)
      tw.slots[tw_insert_at+1] = j
      tw.num_free = tw.num_free - 1
      tw_insert_at = tw_insert_at+1
      if (tw_insert_at == tw.num_slots) then
	 tw_insert_at = 0
      end
   end

   mf["max_flows"] = wl.nflows
   mf["num_active"] = 0
   return mf
end


-- TO TEST
function fillControlPacketsForRound1(bufs2, mf, cpg_round, run_id)
   local i = 1
   while i < bufs2.size
      and mf.next_flow <= mf.max_flows
   and mf.global_last_update < cpg_round do
      -- get next flow whose round 1 packet needs to be sent
      while(mf.next_flow <= mf.max_flows
	       and (
		  mf.name[mf.next_flow] == -1 or
		  mf.last_update[mf.next_flow] == cpg_round)) do
	 mf.next_flow = mf.next_flow+1
      end
      if mf.next_flow <= mf.max_flows then
	 -- assert(mf.next_flow >= 1)
	 -- assert(mf.name[mf.next_flow] ~= -1)
	 -- assert(mf.name[mf.next_flow]~= nil)
	 -- assert(mf.last_update[mf.next_flow] < cpg_round)

	 -- fill in demand, stable
	 local pkt = bufs2[i]:getIP4Packet()
	 pkt.eth:setSrc(MAC_FROM_SENDER)
	 pkt.ip4:setSrc(SRC_IP_BASE+mf.name[mf.next_flow])
	 pkt.ip4:setDst(SRC_IP_BASE+FROM_SENDER*256+ip.PROTO_CONTROL_1)
	 pkt.ip4:setProtocol(ip.PROTO_CONTROL_1) 
	 pkt.payload.uint64[0] = (mf.demand[mf.next_flow]*(2^32))
	 pkt.payload.uint8[8] = 0 -- hop
	 pkt.payload.uint8[9] = mf.stable[mf.next_flow] -- stable

	 -- Put Cpg round info
	 pkt.payload.uint32[5] = CPG_ROUND_FOLLOWS
	 pkt.payload.uint32[6] = cpg_round	 
	 pkt.payload.uint32[7] = run_id
	 
	 mf.last_update[mf.next_flow] = cpg_round
	 i = i + 1
      else
	 --	 for j = 1, mf.max_flows do
	    -- if (mf.name[j] ~= -1) then
	    --    assert(mf.last_update[j] == cpg_round)
	    -- end
	 -- end
	 mf.next_flow = 1
	 mf.global_last_update = cpg_round
      end      
   end
   return i-1
end

function fillControlPacketsForRound3(bufs2, mf, cpg_round, run_id)
   local i = 1
   while i < bufs2.size and mf.next_flow <= mf.max_flows and mf.global_last_update < cpg_round do
      -- get next flow whose round 1 packet needs to be sent
      while(mf.next_flow <= mf.max_flows
	       and (mf.last_update[mf.next_flow] == cpg_round or
		    mf.name[mf.next_flow] == -1)) do
	 mf.next_flow = mf.next_flow+1
      end
      
      if mf.next_flow <= mf.max_flows then
	 -- assert(mf.next_flow >= 1)
	 -- assert(mf.name[mf.next_flow] ~= -1)
	 -- assert(mf.name[mf.next_flow]~= nil)
	 -- assert(mf.last_update[mf.next_flow] < cpg_round)

	 -- fill in demand, stable
	 local pkt = bufs2[i]:getIP4Packet()
	 pkt.eth:setSrc(MAC_FROM_SENDER)
	 pkt.ip4:setSrc(SRC_IP_BASE+mf.name[mf.next_flow])
	 pkt.ip4:setDst(SRC_IP_BASE+FROM_SENDER*256+ip.PROTO_CONTROL_3)
	 pkt.ip4:setProtocol(ip.PROTO_CONTROL_3)
	 pkt.payload.uint64[0] = (mf.demand[mf.next_flow]*(2^32))
	 pkt.payload.uint8[8] = mf.stable[mf.next_flow]

	 -- Put Cpg round info
	 pkt.payload.uint32[5] = CPG_ROUND_FOLLOWS
	 pkt.payload.uint32[6] = cpg_round
	 pkt.payload.uint32[7] = run_id

	 mf.last_update[mf.next_flow] = cpg_round
	 i = i + 1
      else
	 -- for j = 1, mf.max_flows do
	 --    if (mf.name[j] ~= -1) then
	 --       assert(mf.last_update[j] == cpg_round)
	 --    end
	 -- end
	 mf.next_flow = 1
	 mf.global_last_update = cpg_round
      end      
   end
   return i-1
end

-- TO TEST
function fillControlPacketsForRound2(bufs2, tf, cpg_round, run_id)
   local i = 1
   while i < bufs2.size and tf.last_update < cpg_round and tf.num_flows > 0 and tf.next_flow <= tf.num_flows do
      -- make a packet for tf.flow_id[tf.next_flow],
      -- and tf.next_hop and inc. next_hop
      local pkt = bufs2[i]:getCpgPacket()
      pkt.eth:setSrc(MAC_FROM_RECEIVER)
      pkt.ip4:setSrc(SRC_IP_BASE+tf.flow_id[tf.next_flow])
      pkt.ip4:setDst(SRC_IP_BASE+FROM_RECEIVER*256+ip.PROTO_CONTROL_2_rvs)
      pkt.ip4:setProtocol(ip.PROTO_CONTROL_2_rvs)
      pkt.cpg:setLinkID(tf.next_hop)
      pkt.cpg:setSumSat(0)
      pkt.cpg:setNumSat(0)
      pkt.cpg:setNumFlows(0)

      -- Put Cpg round info
      local ip_pkt = bufs2[i]:getIP4Packet()
      ip_pkt.payload.uint32[5] = CPG_ROUND_FOLLOWS
      ip_pkt.payload.uint32[6] = cpg_round
      ip_pkt.payload.uint32[7] = run_id

      --log:info("Sending rev in round2")
      --bufs2[i]:dump()
      tf.next_hop = tf.next_hop + 1
      if tf.next_hop == tf.nhops[tf.next_flow] then
	 tf.next_flow = tf.next_flow+1
	 tf.next_hop = 0	 
	 if tf.next_flow > tf.num_flows then
	    tf.last_update = cpg_round
	    if cpg_round%5 == 2 then
	       tf.last_update = cpg_round+1
	    end
	    tf.next_flow = 1
	 end	 	 
      end	       
      i = i + 1
   end
   return i-1
end

-- TO TEST
function fillControlPacketsForRound4(bufs2, tf, cpg_round, run_id)
   local i = 1
   while i < bufs2.size and tf.last_update < cpg_round and tf.num_flows > 0 and tf.next_flow <= tf.num_flows do
      -- make a packet for tf.flow_id[tf.next_flow],
      local pkt = bufs2[i]:getIP4Packet()
      pkt.eth:setSrc(MAC_FROM_RECEIVER)
      pkt.ip4:setSrc(SRC_IP_BASE+tf.flow_id[tf.next_flow])
      pkt.ip4:setDst(SRC_IP_BASE+FROM_RECEIVER*256+ip.PROTO_CONTROL_4)
      pkt.ip4:setProtocol(ip.PROTO_CONTROL_4)
      pkt.payload.uint8[0] = 0

      -- Put Cpg round info
      pkt.payload.uint32[5] = CPG_ROUND_FOLLOWS
      pkt.payload.uint32[6] = cpg_round
      pkt.payload.uint32[7] = run_id

      tf.next_flow = tf.next_flow+1
      if tf.next_flow > tf.num_flows then
	 tf.last_update = cpg_round
	 tf.next_flow = 1
      end
      i = i + 1
   end
   return i-1
end

function txSlave(workload_file, start_time,
		 rxQueue1, rxQueue2, queue, queue1, queue2, dstMac, dstMacNo, myMacNo, myDevNo, run_id)
   log:info("txSlave myDevNo " .. myDevNo .. " run_id " .. run_id)
   local lm_time_start = lm.getTime()
   local mt_start = getMonotonicTime()
   local offset_start = lm_time_start-mt_start
   log:info("txSlave " .. myDevNo .. " current clock time " .. mt_start .. " seconds")
   log:info("txSlave " .. myDevNo .. " current+1 clock time " .. lm_time_start .. " seconds")
   log:info("txSlave " .. myDevNo .. " offset " .. offset_start .. " seconds")

   
   -- rxQueue1 to receive control packets for my flows except round 2 (rxQueue2)
   -- queue to send data packets for my flows, queue1 to send extra control packets, queue2 to send round 2 control packets
   -- rxQueue, rxSpQueue, txSpQueue, txQueue, txQueue2, dstMac, dstMacNo, myMacNo)
   
   -- Set up 2 memory pool and 4 buffer arrays
   
   -- memory pool with default values for all packets, this is our archetype
   local mempool = memory.createMemPool(function(buf)
	 buf:getIP4Packet():fill{
	    -- fields not explicitly set here are initialized to reasonable defaults
	    ethSrc = MAC_FROM_SENDER, -- MAC of the tx device
	    ethDst = dstMac,
	    ip4Src = SRC_IP,
	    ip4Dst = DST_IP,
	    pktLength = PKT_LEN
				}
   end)

   local mempool2 = memory.createMemPool(function(buf)
	 buf:getIP4Packet():fill{
	    -- fields not explicitly set here are initialized to reasonable defaults
	    ethSrc = MAC_FROM_SENDER, -- MAC of the tx device
	    ethDst = dstMac,
	    ip4Src = SRC_IP,
	    ip4Dst = DST_IP,
	    pktLength = PKT_LEN2
				}
   end)

   -- a bufArray is just a list of buffers from a mempool that is processed as a single batch
   local bufs = mempool:bufArray(BATCH_SIZE)
   local bufs2 = mempool2:bufArray(BATCH_SIZE)
   local rxBufs = memory.bufArray()
   local fwdBufs = memory.bufArray()

   -- Workload info, by flow index (name, size in packet,
   --  start time in us). Load from workload file.
   local wl = loadWorkload(workload_file)
   if wl.nflows > 0 then
      log:info("TxSlave on dev " .. queue.id .. ", queue " .. queue.qid .. " loaded workload with " .. wl.nflows .. " flows")
   end
   -- Set up timing wheel one slot = one packet
   local tw = setupTimingWheel(1000, {})
   -- Per flow state for active flows that start here
   local mf = setupMyFlowState(wl, tw,
				     {["gap"]= INIT_GAP,
					["rate"] = INIT_RATE,
					["nsent"] = 0,
					["name"] = -1,
					["size"] = 0,
					["demand"] = INIT_DEMAND,
					["stable"] = 0,
					["last_update"] = 0})
   
   -- TODO: Need to sync this across machines so wall clock time
   -- is same, also well as correct for drifts
   local cpg_round = 0
   local cpg_type = -1
   local cpg_left = ROUND
   local prev_cpg_round = -100
   local started = false

   -- for logging configured rates per Cpg round
   local prep_start = nil
   local cpg_round_check = nil
   local num_sends = 0
   local flow_start = {}
   local flow_finish = {}
   local num_control_packets = 0

   local num_my_packets = {}
   local num_their_packets = {}
   local num_unknown_packets = {}
   local num_over = 0

   local incorrect_round = {}
   local correct_round = {}
   local max_async = {}
   
   while lm.running and num_over < wl.nflows
   do
      --while lm.running() do -- check if Ctrl+c was pressed
      local lm_time = getTimeFast() 
      if (started == false) then
	 while (lm_time < start_time) do
	    lm_time = getTimeFast()	    
	 end
	 while (cpg_type ~= 1) do
	    cpg_round = ceil((lm_time - start_time)/ROUND)
	    cpg_type = cpg_round%5
	    if (cpg_type ~= 1) then
	       lm_time = getTimeFast()
	    end
	 end
	 started = true
      end
      cpg_round = ceil((lm_time - start_time)/ROUND)
      cpg_type = cpg_round%5
      -- SINCE ROUND 2 is 2X longer
      if (cpg_type >= 3) then
	 cpg_type = cpg_type - 1
      end
      cpg_left = start_time + ((cpg_round+1) * ROUND) - lm_time
      if cpg_round%5 == 2 then
	 cpg_left = cpg_left + ROUND
      end
      -- check if cpg round changed
      if cpg_round > prev_cpg_round then
	 log:info("txSlave " .. myDevNo .. ": Cpg round "
		     .. cpg_round .. " type " .. cpg_type ..  " at time "
		     .. lm_time .. " (" .. (lm_time-start_time)*1e6 .. " us from start)"
		     .. " num packets of flow 1 sent " .. mf.nsent[1]
		     .. " number of flows " .. mf.max_flows)
	 if (cpg_round > prev_cpg_round+1) then
	    log:error("TxSlave " .. myDevNo .. ": Cpg round jumped from "
			 .. prev_cpg_round .. " to "
			 .. cpg_round .. " in one iteration.")
	 end
	 prev_cpg_round = cpg_round
      end

      
      -- Sending data packets
      bufs2:alloc(PKT_LEN)
      local num_bufs2 = 0
      local num_bufs1 = 0
      bufs:alloc(PKT_LEN)
      while num_bufs2 < bufs2.size and num_bufs1 < bufs.size do
	 local next_flow = tw.slots[tw.current_slot+1]
	 if (next_flow == 0) then
	    local pkt = bufs[num_bufs1+1]:getIP4Packet()
	    -- Drop void packets
	    pkt.eth:setSrc(dstMacNo)
	    pkt.eth:setDst(dstMacNo)
	    pkt.ip4:setSrc(SRC_IP_BASE)
	    pkt.ip4:setProtocol(555)
	    num_bufs1 = num_bufs1 + 1
	 else
	    local pkt = nil
	    local send_control = ((cpg_type == 1 or (cpg_type==3 and mf.stable[next_flow]==0))
		  and mf.last_update[next_flow] < cpg_round and cpg_left > ONE_WAY_DELAY)
	    -- Get the if we need to send control packet
	    if send_control == false then
	       pkt = bufs[num_bufs1+1]:getIP4Packet()
	       pkt.ip4:setDst(SRC_IP_BASE+FROM_SENDER*256+ip.PROTO_DATA)
	       pkt.ip4:setProtocol(ip.PROTO_DATA)
	       -- Clear Cpg round info
	       pkt.payload.uint32[5] = 0
	       pkt.payload.uint32[6] = 0
	       pkt.payload.uint32[7] = 0
	       num_bufs1 = num_bufs1 + 1
	    else	       
		  pkt = bufs2[num_bufs2+1]:getIP4Packet()
		  num_bufs2 = num_bufs2 + 1
	       	  pkt.payload.uint64[0] = (mf.demand[next_flow]*(2^32))
		  -- Put Cpg round info
		  pkt.payload.uint32[5] = CPG_ROUND_FOLLOWS
		  pkt.payload.uint32[6] = cpg_round
		  pkt.payload.uint32[7] = run_id
		  mf.last_update[next_flow] = cpg_round
		  -- Put round 1 or 3 specific info
		  if (cpg_type == 1) then
		     pkt.payload.uint8[8] = 0 -- hop
		     pkt.payload.uint8[9] = mf.stable[next_flow]
		     log:info("txSlave " .. myDevNo .. " adding control info "
				 .. " of Type 1 to data packets in round " .. cpg_round
				 .. " demand is (64b) " .. tonumber(pkt.payload.uint64[0])
				 .. " hop is (8b) " .. tonumber(pkt.payload.uint8[8])
				 .. " stable is (8b) " .. tonumber(pkt.payload.uint8[9]))
		     pkt.ip4:setDst(SRC_IP_BASE+FROM_SENDER*256+ip.PROTO_CONTROL_1)
		     pkt.ip4:setProtocol(ip.PROTO_CONTROL_1)
		  elseif (cpg_type == 3) then
		     pkt.payload.uint8[8] = mf.stable[next_flow]
		     log:info("txSlave " .. myDevNo .. " adding control info "
				 .. " of Type 3 to data packets in round " .. cpg_round
				 .. " demand is (64b) " .. tonumber(pkt.payload.uint64[0])
				 .. " stable is (8b) " .. tonumber(pkt.payload.uint8[8]))
		     pkt.ip4:setDst(SRC_IP_BASE+FROM_SENDER*256+ip.PROTO_CONTROL_3)
		     pkt.ip4:setProtocol(ip.PROTO_CONTROL_3)
		  end
	    end -- piggyback control or no
	    
	    -- Fill in data packet
	    local name = mf.name[next_flow]
	    local num_sent = mf.nsent[next_flow] + 1
	    local size = mf.size[next_flow]
	    -- important to update source MAC
	       -- else will use value in buffer (garbage/ old)
	    -- and possibly drop packet, not clear if it's set to default values on alloc
	    pkt.eth:setSrc(MAC_FROM_SENDER)
	    pkt.eth:setDst(dstMacNo)
	    pkt.ip4:setSrc(SRC_IP_BASE + name)
	    mf.nsent[next_flow] = num_sent --mf.nsent[next_flow] + 1 --num_sent
	    --log:info("We've sent " .. mf.nsent[next_flow] .. " packets of flow " .. mf.name[next_flow])
	    if (num_sent == 1) then
	       flow_start[name] = lm.getTime()
	    end
	    
	    -- Update timing wheel O(1)
	    tw.slots[tw.current_slot+1] = 0
	    if (mf.nsent[next_flow] < mf.size[next_flow]) then	       
	       local next_index = get_next_free(tw, mf.gap[next_flow])		  
	       tw.slots[next_index+1] = next_flow
	    else -- if flow ended
	       num_over = num_over + 1
	       flow_finish[name] = lm.getTime()
	       tw.num_free = tw.num_free+1
	       log:info("Finished sending flow " .. mf.name[next_flow] .. " in round " .. cpg_round)
	    end 	       
	 end -- send data or void
	 -- update current tick per packet
	 tick(tw)
      end -- for i, buf
      -- send out all packets and frees old bufs that have been sent
      if num_bufs1 > 0 then
	 queue:sendN(bufs, num_bufs1)
	 for i=num_bufs1, bufs.size do
	    bufs[i]:free()
	 end
      end
      if num_bufs2 > 0 then
	 queue1:sendN(bufs2, num_bufs2)
	 for i=num_bufs2, bufs.size do
	    bufs2[i]:free()
	 end
      end

      -- recv control packets
      if (cpg_type == 0) then
	 local count = rxQueue1:tryRecv(rxBufs, 1)	    
	 local data = 0
	 if count >= 1 then
	    log:info("txSlave " .. myDevNo
			.. " received " .. (count or 0) .. " control packets of type 0 in round " .. cpg_round)
	       for i = 1, count do
		  local buf = rxBufs[i]
		  local pkt = buf:getIP4Packet()
		  local proto = pkt.ip4:getProtocol()
		  local src_ip = pkt.ip4:getSrc()
		  local flow_id = src_ip - SRC_IP_BASE
		  local src_mac = pkt.eth:getSrc()
		  
	       -- update stats about out of sync packets
		  if (pkt.payload.uint32[5] == CPG_ROUND_FOLLOWS and pkt.payload.uint32[7] == run_id) then
		     local sent_cpg_round = tonumber(pkt.payload.uint32[6])
		     if (sent_cpg_round == cpg_round) then
			correct_round[proto] = (correct_round[proto] or 0) + 1
		     else
			incorrect_round[proto] = (incorrect_round[proto] or 0) + 1
			if false and cpg_round % 1000 == 0 then
			   sample_out_of_sync = {next=sample_out_of_sync, sent_cpg_round=sent_cpg_round, now=cpg_round, proto=proto}
			end
			local tmp = max_async[proto] or 0
			local diff = cpg_round - sent_cpg_round
			max_async[proto] = max(max(tmp, diff), -diff)
			if (diff > 100 or -diff > 100) then
			   log:error((kind[proto] or proto) .. " packet from " .. tonumber(sent_cpg_round)
				 .. " recvd in " .. tonumber(cpg_round))
			end
		     end
		  end
		  
		  -- if src_mac == MAC_FROM_SENDER then
		  -- 	  num_their_packets[proto] = (num_their_packets[proto] or 0) +1
		  -- elseif src_mac == MAC_FROM_RECEIVER then
		  -- 	     num_my_packets[proto] = (num_my_packets[proto] or 0) + 1
	       -- else
		  -- 	    num_unknown_packets[proto] = (num_unknown_packets[proto] or 0) + 1
		  -- end
		  
		  if (proto == ip.PROTO_CONTROL_4 and src_mac == MAC_FROM_RECEIVER and pkt.payload.uint32[7] == run_id) then		
		     local stable = pkt.payload.uint8[0]
		     log:info("txSlave " .. myDevNo .. " Received control packet of Type 4 in round " .. cpg_round .. " with stable (8b) " .. stable)
		     if stable > 0 then
			log:info("Flow " .. flow_id .. " just got stable")
		     end
		     mf.stable[mf.flow_index[flow_id]] = stable
		  end
		  buf:free()
	       end
	    end
	 end

	 -- TODO: In debugging mode, assert that all
	 --  the right flows have been updated in each round	 
	 -- round 2 echo, TO TEST
	 if cpg_type == 2 then	    
	    local count = rxQueue2:tryRecv(fwdBufs, 10)
	    if count >= 1 then
	       log:info("txSlave " .. myDevNo .. " received " .. (count or 0) .. " Type 2 control packets in round " .. cpg_round)
	       --log:info("tx slave " .. myDevNo .. "  got " .. count .. " packets on rx queue 2")
	       for i = 1, count do
	  	  local buf = fwdBufs[i]
	   	  local pkt = buf:getCpgPacket()
		  local ip_pkt = buf:getIP4Packet()
		  local pkt_run_id = ip_pkt.payload.uint32[7]
		  if (pkt_run_id ~= run_id) then
		     -- drop packet
		     -- TO DO: Need to know who to send to ..
		     log:error("Dropping packet at txSlave " .. myDevNo .. " with run id " .. pkt_run_id)
		     pkt.eth:setSrc(dstMacNo)
		     pkt.eth:setDst(dstMacNo)
		  else
		     --buf:dump()
		     pkt.eth:setDst(dstMacNo)
		
		     local flowId = pkt.ip4:getSrc() - SRC_IP_BASE
		     local proto = pkt.ip4:getProtocol()		     
		     pkt.eth:setSrc(MAC_FROM_SENDER)
		     pkt.ip4:setDst(SRC_IP_BASE+FROM_SENDER*256+ip.PROTO_CONTROL_2_fwd)
		     pkt.ip4:setProtocol(ip.PROTO_CONTROL_2_fwd)
		     
		     local rate = LINK_CAPACITY
		     local numFlows = pkt.cpg:getNumFlows()
		     --log:info("numFlows " .. numFlows)
		     local numSat = pkt.cpg:getNumSat()
		     --log:info("numSat " .. numSat)
		     local sumSat =  pkt.cpg:getSumSat()
		     local numUnsat = numFlows - numSat
		     --log:info("sumSat " .. sumSat)
		     if numUnsat > 0 then
			rate = (LINK_CAPACITY - sumSat)/numUnsat
		     end

		     pkt.cpg:setSumSat(rate)
		     local flow_index = mf.flow_index[flowId]
		     -- update state
		     if (rate < mf.demand[flow_index]) then
			mf.demand[flow_index] = rate
			log:info("Demand of flow " .. flowId .. " updated to rate " .. rate .. " Mb/s")
		     end

		     log:info("calculated rate " .. rate
				 .. " from (" .. LINK_CAPACITY .. " - "
				 .. sumSat .. ") / " .. numUnsat ..
			      " in cpg round " .. cpg_round .. " for link "
				 .. pkt.cpg:getLinkID())
		     -- TODO: float to int		     , done in cpg header

		     -- if packet belongs to this run it already has run_id and cpg_round
		     -- Put Cpg round info .. since this packet is Ethernet/ Ip/ Cpg cpg round info at 0,1,2
		     --pkt.payload.uint32[0] = CPG_ROUND_FOLLOWS
		     --pkt.payload.uint32[1] = cpg_round	 
		     --pkt.payload.uint32[2] = run_id
		     
		     -- update stats about out of sync packets
		     if (pkt.payload.uint32[0] == CPG_ROUND_FOLLOWS) then
		     	local sent_cpg_round = tonumber(pkt.payload.uint32[1])
		     	if (sent_cpg_round == cpg_round) then
		     	   correct_round[proto] = (correct_round[proto] or 0) + 1
		     	else
		     	   incorrect_round[proto] = (incorrect_round[proto] or 0) + 1
		     	   if false and cpg_round % 1000 == 0 then
		     	      sample_out_of_sync = {next=sample_out_of_sync, sent_cpg_round=sent_cpg_round, now=cpg_round, proto=proto}
		     	   end
		     	   local tmp = max_async[proto] or 0
		     	   local diff = cpg_round - sent_cpg_round
		     	   max_async[proto] = max(max(tmp, diff), -diff)
		     	   if (diff > 100 or -diff > 100) then
		     	      log:error((kind[proto] or proto) .. " packet from " .. tonumber(sent_cpg_round)
		     		    .. " recvd in " .. tonumber(cpg_round))
		     	   end
		     	end
		     end
		     
		     if pkt.eth:getSrc() == MAC_FROM_SENDER then
		        num_their_packets[proto] = (num_their_packets[proto] or 0) +1
		     elseif pkt.eth:getSrc() == MAC_FROM_RECEIVER then
		        num_my_packets[proto] = (num_my_packets[proto] or 0) + 1
		     else
		        num_unknown_packets[proto] = (num_unknown_packets[proto] or 0) + 1
		     end


		  end -- if run_id
	       end -- for i=1, count
	       queue2:sendN(fwdBufs, count)
	    end -- check if count > 0	     
	 end  -- checking if cpg_type is 2

	 num_sends = num_sends + 1
   end


   log:info("txSlave " .. myDevNo .. " finished in round " .. cpg_round)
      log:info("txSlave " .. myDevNo .. ": Flow stats")
   for i, name in ipairs(wl.name) do
      size = wl.size[i] or 0
      nsent = wl.nsent[name] or 0
      start = flow_start[name] or 0
      finish = flow_finish[name] or 0
      fct = ((8*nsent*PKT_LEN)/(finish-start))/1e6
      log:info("txSlave " .. myDevNo .. ": Flow # " .. i.. ": " .. name .. " 'sent' " .. nsent .. " packet with fct " .. fct .. " Mb/s "
		  .. " from " .. start*1e6 .. " us to " .. finish*1e6 .. " us")
   end
   log:info("txSlave " .. myDevNo .. ": Sync stats")
   for k, num in pairs(incorrect_round) do
      local tmp = max_async[k] or "n/a"
      log:info("txSlave at dev " .. myDevNo ..
		  " got " .. (correct_round[k] or 0) .. " " .. (kind[k] or k) .. " packets in the correct round"
		  .. " and " .. num ..  " packets in the incorrect round with max_async " .. tmp)
   end
   
   for k, num in pairs(correct_round) do
      if incorrect_round[k] == nil then
	 local tmp = max_async[k] or "n/a"
	 log:info("txSlave at dev " .. myDevNo ..
		     " got " .. (correct_round[k] or 0) .. " " .. (kind[k] or k) .. " packets in the correct round")
      end
   end
   log:info("txSlave " .. myDevNo .. ": Sample out of sync stats")
   local tmp = sample_out_of_sync
   while (tmp) do
      log:info("txSlave " .. myDevNo .. ": Round " .. (kind[tmp.proto] or tmp.proto)
		  .. " out of sync packet was sent in " ..  tonumber(tmp.sent_cpg_round)
		  .. ", recvd in " .. tonumber(tmp.now) .. " off by " .. tonumber(tmp.now-tmp.sent_cpg_round))
      tmp = tmp.next
   end

   log:info("txSlave " .. myDevNo .. ": Packet counts by proto")
   for k, num in pairs(num_my_packets) do
      local kind_desc = kind[k] or k
      log:info("txSlave at dev " .. myDevNo .. ": " .. num .. " packets of my flows of type " .. kind_desc)
   end
   for k, num in pairs(num_their_packets) do
      local kind_desc = kind[k] or k
      log:info("txSlave at dev " .. myDevNo .. ": " .. num .. " packets of their flows of type " .. kind_desc)
   end
   for k, num in pairs(num_unknown_packets) do
      local kind_desc = kind[k] or k
      log:info("txSlave at dev " .. myDevNo .. ": " .. num .. " packets of unknown flows of type " .. kind_desc)
   end

   --log:info("number of valid packets received " .. valid)

   local lm_time_end = lm.getTime()
   local mt_end = getMonotonicTime()
   local offset_end = lm_time_end-mt_end
   log:info("txSlave " .. myDevNo .. " current clock time " .. mt_end .. " seconds")
   log:info("txSlave " .. myDevNo .. " TSC clock time " .. lm_time_end .. " seconds")
   log:info("txSlave " .. myDevNo .. " offset " .. offset_end .. " seconds")
   log:info("txSlave " .. myDevNo .. " offset changed by " .. (offset_end - offset_start)*1e9
	       .. " ns over " .. (mt_end-mt_start) .. " s of system clock.")

  
   if wl.nflows > 0 then
      lm.sleepMillisIdle(10)
      lm.stop()
   end
   
   -- local tmp = prep_start
   -- local prev = 0
   -- while tmp do
   --    local avg_diff = (((tmp.value - prev) * 1e6)/CHECK_TIME_EVERY)
   --    local mbps = ((BATCH_SIZE * PKT_LEN * 8))/avg_diff
   --    --+ (BATCH_SIZE2 * PKT_LEN2 * 8))/avg_diff
   --    log:info(tmp.value * 1e6 .. " us start prep, "
   -- 		  .. avg_diff
   -- 		  .. " us per batch of " .. BATCH_SIZE .. " "
   -- 		  .. PKT_LEN .. " B packets"
   -- 	       .. " for a rate of " .. mbps .. " Mb/s")
   --    -- .. " plus ".. BATCH_SIZE2 .. " " .. PKT_LEN2 .. " B packets"
   --    prev = tmp.value
   --    tmp = tmp.next
   -- end

   -- local tmp = cpg_round_check
   -- local rev = nil
   -- while tmp do
   --    rev = {next=rev, value=tmp.value, nflows=tmp.nflows, rate=tmp.rate}
   --    tmp = tmp.next
   -- end

   -- tmp = rev
   -- while tmp do
   --    --log:info("Cpg round check at " .. tmp.value*1e6 .. " us (" .. (diff*1e6) .. " us after last round check), num_active_flows in previous round " .. tmp.nflows .. " .")
   --    log:info((tmp.value-start_time)*1e6 .. " us, num_active_flows " .. tmp.nflows .. ", each with rate "
   -- 	 .. tmp.rate .. " Mb/s for total " .. (tmp.nflows*tmp.rate) .. " Mb/s .")
   --    tmp = tmp.next
   -- end

end

function rxSlave1(workload_file, start_time,
		  rxQueue, queue, dstMac, dstMacNo, myMacNo, myDevNo, run_id)

   
   local lm_time_start = lm.getTime()
   local mt_start = getMonotonicTime()
   local offset_start = lm_time_start-mt_start
   log:info("rxSlave myDevNo " .. myDevNo .. " run_id " .. run_id)
   log:info("rxSlave " .. myDevNo .. " current clock time " .. mt_start .. " seconds")
   log:info("rxSlave " .. myDevNo .. " TSC clock time " .. lm_time_start .. " seconds")
   log:info("rxSlave " .. myDevNo .. " offset " .. offset_start .. " seconds")

   -- rxQueue1 to receive control packets for my flows except round 2 (rxQueue2)
   -- queue to send data packets for my flows, queue1 to send extra control packets, queue2 to send round 2 control packets
   -- rxQueue, rxSpQueue, txSpQueue, txQueue, txQueue2, dstMac, dstMacNo, myMacNo)
   
   -- Set up 2 memory pool and 4 buffer arrays
   
   -- memory pool with default values for all packets, this is our archetype
   -- to send control packets for round 2 and 4
   local mempool = memory.createMemPool(function(buf)
	 buf:getIP4Packet():fill{
	    -- fields not explicitly set here are initialized to reasonable defaults
	    ethSrc = MAC_FROM_RECEIVER, -- MAC of the tx device
	    ethDst = dstMac,
	    ip4Src = SRC_IP,
	    ip4Dst = DST_IP,
	    pktLength = PKT_LEN2
				}
   end)
   -- a bufArray is just a list of buffers from a mempool that is processed as a single batch
   local bufs = mempool:bufArray(BATCH_SIZE2)
   -- to receive data and control packets
   local rxBufs = memory.bufArray()

   local twl = loadWorkload(workload_file)
   if twl.nflows > 0 then
      log:info("rxSlave on dev " .. rxQueue.id .. ", queue " .. rxQueue.qid .. " loaded workload with " .. twl.nflows .. " flows")
   end
   local tf = setupTheirFlowState()
   
   -- Cpg rounds start at start_time, tentatively 3ms from now
   -- TODO: Need to sync this across machines so wall clock time
   -- is same, also well as correct for drifts
   local cpg_round = 0
   local cpg_type = -1
   local cpg_left = ROUND
   local prev_cpg_round = -100
   local started = false

   local num_sends = 0
   local flow_start = {}
   local flow_finish = {}
   local flow_count = {}
   
   local num_my_packets = {}
   local num_their_packets = {}
   local num_unknown_packets = {}


   local sample_out_of_sync = nil

   local incorrect_round = {}
   local correct_round = {}
   local max_async = {}
   
   (require "jit.p").start("al")   
   while lm.running() do -- check if Ctrl+c was pressed

      local lm_time = getTimeFast() 
      if (started == false) then
	 while (lm_time < start_time) do
	    lm_time = getTimeFast()	    
	 end
	 while (cpg_type ~= 1) do
	    lm_time = getTimeFast()
	    cpg_round = ceil((lm_time - start_time)/ROUND)
	    cpg_type = cpg_round%5	    
	 end
	 started = true
      end
      cpg_round = ceil((lm_time - start_time)/ROUND)      
      cpg_type = cpg_round%5
      -- SINCE ROUND 2 is 2X longer
      if (cpg_type >= 3) then
	 cpg_type = cpg_type - 1
      end
      cpg_left = start_time + ((cpg_round+1) * ROUND) - lm_time
      if cpg_round%5 == 2 then
	 cpg_left = cpg_left + ROUND
      end

      if cpg_round > prev_cpg_round then
	 log:info("rxSlave " .. myDevNo .. ": Cpg round "
		     .. cpg_round .. " at time " .. lm_time
		     .. " (" .. (lm_time-start_time)*1e6 .. " us from start)")
	 if (cpg_round > prev_cpg_round+1) then
	    log:error("rxSlave1 " .. myDevNo .. ": Cpg round jumped from "
			 .. prev_cpg_round .. " to "
			 .. cpg_round .. " in one iteration.")
	 end	   
	 -- only update on receiver side, reset flow state at the beginning or round 1
	 prev_cpg_round = cpg_round
	 if (cpg_type == 1) then
	    cpgResetTheirFlowState(tf)
	 end
      end
      
      -- receive data and control packets
      local count = 0

      if queue == nil then
	 count = rxQueue:recv(rxBufs)
      else
	 count = rxQueue:tryRecv(rxBufs, 10)
	 if count > 0 then
	    log:info("rxSlave " .. myDevNo .. " received " .. (count) .. " packets round " .. cpg_round)
	 end
      end
      if count > 0 then
	 for i = 1, count do
	    local buf = rxBufs[i]
	    local pkt = buf:getIP4Packet()
	    local src_ip = pkt.ip4:getSrc()
	    local flow_id = src_ip - SRC_IP_BASE
	    local proto = pkt.ip4:getProtocol()

	    -- update packet counts
	    if pkt.eth:getSrc() == MAC_FROM_SENDER then
	       num_their_packets[proto] = (num_their_packets[proto] or 0) +1
	    elseif pkt.eth:getSrc() == MAC_FROM_RECEIVER then
	       num_my_packets[proto] = (num_my_packets[proto] or 0) + 1
	    else num_unknown_packets[proto] = (num_unknown_packets[proto] or 0) + 1
	    end

	    -- update stats about out of sync packets
	    if (pkt.payload.uint32[5] == CPG_ROUND_FOLLOWS and pkt.payload.uint32[7] == run_id) then
	       local sent_cpg_round = tonumber(pkt.payload.uint32[6])
	       if (sent_cpg_round == cpg_round) then
		  correct_round[proto] = (correct_round[proto] or 0) + 1
	       else
		  incorrect_round[proto] = (incorrect_round[proto] or 0) + 1
		   
		     
		  if cpg_round % 1000 == 0 then
		     sample_out_of_sync = {next=sample_out_of_sync, sent_cpg_round=sent_cpg_round, now=cpg_round, proto=proto}
		  end
		  local tmp = max_async[proto] or 0
		  local diff = (cpg_round - sent_cpg_round)
		  max_async[proto] = max(max(tmp, diff), -diff)
		  --if (diff > 100 or -diff > 100 or proto == ip.PROTO_CONTROL_fwd_2) then
		  if (diff ~= 0) then
		     log:error((kind[proto] or proto) .. " packet from " .. tonumber(sent_cpg_round)
			   .. " recvd in " .. tonumber(cpg_round))
		  end
	       end	       	       
	    else
	       if proto ~= ip.PROTO_DATA then
		  rxBufs[i]:dump()
		  log:info("rxSlave " .. myDevNo .. " received round " .. (kind[proto] or proto)
			      .. " control packets with run_id " .. pkt.payload.uint32[7] .. " and CPG_ROUND_FOLLOWS " .. pkt.payload.uint32[5]
			      .. " and sent_cpg_round " .. tonumber(pkt.payload.uint32[6]) .. " in round " .. cpg_round)
	       end
	    end
	    	    
	    -- updating state and flow counts
	    if (queue ~= nil and proto == ip.PROTO_CONTROL_1 and cpg_type == 1 and pkt.payload.uint32[7] == run_id) then
		  local hop = pkt.payload.uint8[8]
		  local stable = pkt.payload.uint8[9]
		  --log:info("Flow " .. flow_id .. " went through " .. hop .. " hops.\n")
		  log:info("rxSlave " .. myDevNo .. " received a type 1 control packet in round " .. cpg_round
			   .. " setting up " .. hop .. " hops for flow " .. flow_id)

		  tf.num_flows = tf.num_flows + 1
		  tf.flow_id[tf.num_flows] = flow_id
		  tf.nhops[tf.num_flows] = hop
	    end	-- ends if proto == .. CONTROL_1

	    -- update flow stats
	    if buf:getSize() > 1000 then
	       if flow_start[flow_id] == nil then
		  flow_start[flow_id] = getMonotonicTime()
		  flow_count[flow_id] = 1
	       else
		  flow_count[flow_id] = flow_count[flow_id] + 1
		  flow_finish[flow_id] = getMonotonicTime()
	       end
	    end
	    buf:free()
	 end -- for i = 1, count
      end -- if count > 0
      
      --send control packets (how many?) TO TEST
      if queue ~= nil then
	 if (cpg_left > ONE_WAY_DELAY and cpg_type == 0)
	    or (cpg_left > ROUND + ONE_WAY_DELAY and cpg_type == 2)
	    and tf.last_update < cpg_round
	 then
	    bufs:alloc(PKT_LEN2)
	    local count = 0
	    if (cpg_type == 2) then
	       count = fillControlPacketsForRound2(bufs, tf, cpg_round, run_id)
	    elseif (cpg_type == 0) then
	       count = fillControlPacketsForRound4(bufs, tf, cpg_round, run_id)
	    end
	    
	    if count > 0 then
	       log:info("rxSlave " .. myDevNo .. " sending " .. count .. " control packets of type " .. cpg_type
			   .. " in round " .. cpg_round)
	       queue:sendN(bufs, count)
	       for i=count+1, bufs.size do
		  bufs[i]:free()
	       end
	    end	 -- ends if count > 0
	    
	 end -- ends if cpg_left > ..
      end -- ends if queue ~= nil
   end -- ends while lm.running()
   (require "jit.p").stop()

   log:info("rxSlave " .. myDevNo .. " finished in round " .. cpg_round)

   log:info("rxSlave " .. myDevNo .. ": Sync stats")
   for k, num in pairs(incorrect_round) do
      local tmp = max_async[k] or "n/a"
      log:info("rxSlave at dev " .. myDevNo ..
		  " got " .. (correct_round[k] or 0) .. " " .. (kind[k] or k) .. " packets in the correct round"
		  .. " and " .. num ..  " packets in the incorrect round with max_async " .. tmp)
   end
   for
   k, num in pairs(correct_round) do
      if incorrect_round[k] == nil then
	 local tmp = max_async[k] or "n/a"
	 log:info("rxSlave at dev " .. myDevNo ..
		     " got " .. (correct_round[k] or 0) .. " " .. (kind[k] or k) .. " packets in the correct round")
      end
   end

   log:info("rxSlave " .. myDevNo .. ": Packet counts by proto")
   for k, num in pairs(num_my_packets) do
      local kind_desc = kind[k] or k
      log:info("rxSlave at dev " .. myDevNo .. ": " .. num .. " packets of my flows of type " .. kind_desc)
   end
   for k, num in pairs(num_their_packets) do
      local kind_desc = kind[k] or k
      log:info("rxSlave at dev " .. myDevNo .. ": " .. num .. " packets of their flows of type " .. kind_desc)
   end
   for k, num in pairs(num_unknown_packets) do
      local kind_desc = kind[k] or k
      log:info("rxSlave at dev " .. myDevNo .. ": " .. num .. " packets of unknown flows of type " .. kind_desc)
   end

   log:info("rxSlave " .. myDevNo .. ": Samples out of sync")
   local tmp = sample_out_of_sync
   while (tmp) do
      log:info("rxSlave1 " .. myDevNo .. ": Round " .. (kind[tmp.proto] or tmp.proto)
		  .. " out of sync packet was sent in " ..  tonumber(tmp.sent_cpg_round)
		  .. ", recvd in " .. tonumber(tmp.now) .. " off by " .. tonumber(tmp.now-tmp.sent_cpg_round))
      tmp = tmp.next
   end


   log:info("rxSlave " .. myDevNo .. ": Flow stats")
   for name, start in pairs(flow_start) do
      nsent = flow_count[name] or 0
      finish = flow_finish[name] or 0
      fct = ((8*nsent*PKT_LEN)/(finish-start))/1e6
      log:info("rxSlave " .. myDevNo ..": Flow " .. name .. ", received " .. nsent .. " packets with fct " .. fct .. " Mb/s"
		  .. " from " ..  (start*1e6) .. " us to " .. (finish*1e6))
   end

   local lm_time_end = lm.getTime()
   local mt_end = getMonotonicTime()
   local offset_end = lm_time_end-mt_end
   log:info("rxSlave " .. myDevNo .. " current clock time " .. mt_end .. " seconds")
   log:info("rxSlave " .. myDevNo .. " TSC clock time " .. lm_time_end .. " seconds")
   log:info("rxSlave " .. myDevNo .. " offset " .. offset_end .. " seconds")
   log:info("rxSlave " .. myDevNo .. " offset changed by " .. (offset_end - offset_start)*1e9
	       .. " ns over " .. (mt_end-mt_start) .. " s of system clock.")

   lm.stop()
end


