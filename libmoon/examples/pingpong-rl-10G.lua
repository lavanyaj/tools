--- A simple UDP packet generator
local lm     = require "libmoon"
local device = require "device"
local stats  = require "stats"
local log    = require "log"
local memory = require "memory"
local arp    = require "proto.arp"
local filter    = require "filter"
local ceil = math.ceil

--ifaddr = {"nf0": {"mac": "02:53:55:4d:45:00", "ip": "1.2.3.4"}
-- eth2      Link encap:Ethernet  HWaddr 0c:c4:7a:b7:60:3c   dev 0
-- eth3      Link encap:Ethernet  HWaddr 0c:c4:7a:b7:60:3d   dev 1
-- set addresses here
local DST_MAC       = "0c:c4:7a:b7:60:3d"
local PKT_LEN       = 1500
local SRC_IP        = "10.0.0.0" -- "10.0.0.10"
local SRC_IP_BASE   = 167772160 -- "10.0.0" -- "10.0.0.10"
local DST_IP        = "1.2.3.4" --"10.1.0.10"
local SRC_PORT_BASE = 1000 
local DST_PORT      = 1234
local NUM_FLOWS     = 1
local ETH_DROP = 0x1234

-- the configure function is called on startup with a pre-initialized command line parser
function configure(parser)
	parser:description("Edit the source to modify constants like IPs and ports.")
	parser:argument("dev", "Devices to use."):args("+"):convert(tonumber)
	parser:option("-t --threads", "Number of threads per device."):args(1):convert(tonumber):default(1)
	return parser:parse()
end

function master(args,...)
   -- configure devices and queues
   -- dev 0 generates packets
   -- dev 1 receives packets and echoes them
   local arpQueues = {}
   for i, dev in ipairs(args.dev) do
	   -- arp needs extra queues
	   local dev = device.config{
	      port = dev,
	      txQueues = args.threads,
	      rxQueues = args.threads+1,
	      rssQueues = args.threads
	   }
	   args.dev[i] = dev
   end
   device.waitForLinks()

   -- print statistics
   stats.startStatsTask{devices = args.dev}

   -- configure tx rates and start transmit slaves
   dev = args.dev[1]
   for i = 1, args.threads do
      local queue = dev:getTxQueue(i - 1)
      lm.startTask("txSlave", queue, DST_MAC)
   end

   dev = args.dev[2]
   --dev:l2Filter(ETH_DROP, args.threads) -- last rx queue is discard queue
   for i = 1, args.threads do
      lm.startTask("forward", dev:getRxQueue(i - 1), dev:getTxQueue(i - 1))
   end
   lm.waitForTasks()
end

function txSlave(queue, dstMac)

   
   -- memory pool with default values for all packets, this is our archetype
   local mempool = memory.createMemPool(function(buf)
	 buf:getUdpPacket():fill{
	    -- fields not explicitly set here are initialized to reasonable defaults
	    ethSrc = dstMac,--queue, -- MAC of the tx device
	    ethDst = dstMac,
	    ip4Src = SRC_IP,
	    ip4Dst = DST_IP,
	    udpSrc = SRC_PORT,
	    udpDst = DST_PORT,
	    pktLength = PKT_LEN
				}
   end)

   local r = 0
   -- timing wheel-ish say each pkt is like checking at time current, then current_time += 1.2us
   -- rate 8 Gb/s --> one packet every 1.5us --> 1 every 1.25 --> 4 every 5??
   -- rates say 1, 4, 5 Gb/s --> one packet every 12us, 3us, 2.4us -->
   -- insert flow 1 12us into future at say t = 12us..
   -- when iterating through timeslots 0us nothing ... 12us ah fill in and put 12us into future
   -- max we schedule 1000 * 1.2 us into future

   num_slots = 1000
   slots = {}
   for i = 1, num_slots do
      slots[i] = 0
   end

   local num_turns = 1
   -- when indexing into array, always +1
   local current_slot = 0

   -- each time step increment current_slot by 1 % 1000
   -- inserting x timeslots into future (current_slot + x)%1000
   -- per flow
   local gap_flow = 0
   local rate_flow = 0
   local sent_flow = 0
   
   local rates = {1000, 2000, 5000}
   local max_rate = 10000
   local next_rate = rates[r+1]   
   -- a bufArray is just a list of buffers from a mempool that is processed as a single batch
   local bufs = mempool:bufArray()
   local num_sends = 0

   -- insert first packet in current slot, hmm do this quick?
   slots[1] = 1
   while num_sends < 10000 and lm.running() do -- check if Ctrl+c was pressed
      if (num_sends%100 == 0) then
	 rate_flow = next_rate
	 gap_flow = ceil(max_rate/rate_flow)
	 --index = (current_slot + gap_flow)%num_slots
	 --slots[index+1] = 1
	 r = (r + 1)%3
	 next_rate = rates[r+1]
       end
      bufs:alloc(PKT_LEN)

      for i, buf in ipairs(bufs) do
	 local pkt = buf:getUdpPacket()

	 local next_flow = slots[current_slot+1]
	 -- --print("current_slot %d, slots[.+1] %d", current_slot, next_flow)
	 if (next_flow == 0) then
	     pkt.eth:setType(ETH_DROP)
	 else
	    --    pkt.eth:setType(0x1)
	  
	    pkt.ip4:setSrc(SRC_IP_BASE + next_flow%255)
	 --    -- reset slot and re-insert packet if more
	    slots[current_slot+1] = 0
	    index = (current_slot + gap_flow)%num_slots
	 --    -- actually could have multiple packets in slot	    
	    slots[index+1] = 1
	 --    --print("index %d, slots[.+1] %d", index, slots[index+1])
	    sent_flow = sent_flow + 1
	 end
	 -- -- actually increment only if no more packet in slot
	 current_slot = (current_slot + 1)%num_slots
	 -- if (current_slot == 0) then
	 --    num_turns = num_turns + 1
	 -- end
      end
      
      -- send out all packets and frees old bufs that have been sent
      queue:send(bufs)
      num_sends = num_sends + 1
   end
   log:info("sent %d packets out of %d", sent_flow, ((num_slots * (num_turns-1)) + current_slot))
end


function forward(rxQueue, txQueue)
	-- a bufArray is just a list of buffers that we will use for batched forwarding
	local bufs = memory.bufArray()
	while lm.running() do -- check if Ctrl+c was pressed
		-- receive one or more packets from the queue
		local count = rxQueue:recv(bufs)
		-- send out all received bufs on the other queue
		-- the bufs are free'd implicitly by this function
		txQueue:sendN(bufs, count)
	end
end

