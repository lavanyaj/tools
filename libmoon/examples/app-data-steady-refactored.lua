--- A simple UDP packet generator. Generates packets for given flow schedule.
--- Notion of Cpg round = (current time - given start time)/ xx us
--- Flows are added to active list every 4th Cpg round
--- Use circular array of tw_num_slots slots to schedule packets of flows
--- Send BATCH_SIZE packets every loop with (hopefully) few operations/ pkt
--- Current slot in tw_num_slots ticks with each packet in batch.. so if
--- packets are being sent at line rate, a slot = Line rate tx time of pkt.
--- A flow that hasn't finished in re-inserted n slots in the future where
--- n is inter-packet time (actually rounded up to nearest multiple of 1.2us)
--- for flow.
--- Jan 9th: load flow schedule from file. count flow stats at receiver.
---  update all active flow's rates to max_rate/N every 4th Cpg round.
---  log flow stats at Tx and Rx. simple rdtsc check time every iteration.

local lm     = require "libmoon"
local device = require "device"
local stats  = require "stats"
local log    = require "log"
local memory = require "memory"
local arp    = require "proto.arp"
local ip    = require "proto.ip4"
local ethernet    = require "proto.ethernet"
local filter    = require "filter"
local open = io.open
local ceil = math.ceil

--ifaddr = {"nf0": {"mac": "02:53:55:4d:45:00", "ip": "1.2.3.4"}
-- eth2      Link encap:Ethernet  HWaddr 0c:c4:7a:b7:60:3c   dev 0
-- eth3      Link encap:Ethernet  HWaddr 0c:c4:7a:b7:60:3d   dev 1
-- set addresses here

local DST_MAC_NO = {}
local DST_MAC = {}
DST_MAC_NO[2] = 8942694572552
DST_MAC[2] = "08:22:22:22:22:08" --"0c:c4:7a:b7:60:3d" --"02:53:55:4d:45:00" -- resolved via ARP on GW_IP or DST_IP, can be overriden with a string here
DST_MAC_NO[1] = 8869393797384
DST_MAC[1] = "08:11:11:11:11:08" --"0c:c4:7a:b7:60:3d" --"02:53:55:4d:45:00" -- resolved via ARP on GW_IP or DST_IP, can be overriden with a string here
--local DST_MAC       = "08:22:22:22:22:08" -- "08:11:11:11:11:08" -- --"02:53:55:4d:45:00" --"0c:c4:7a:b7:60:3d"
--local DST_MAC_NO = 8942694572552 -- 8869393797384 -- 0x81111111108 -- 8942694572552 --0x82222222208
local SRC_IP        = "10.0.0.0" -- "10.0.0.10"
local SRC_IP_BASE   = 167772160 -- "10.0.0" -- "10.0.0.10"
local DST_IP        = "1.2.3.4" --"10.1.0.10"
local SRC_PORT_BASE = 1000 
local DST_PORT      = 1234
local NUM_FLOWS     = 1
local ETH_DROP = 0x1234
local ROUND = 50 * 1e-6
-- checks every 15-20 us .. sends 15 packets every 15-20us enough to send all flows in this time.. and bw??

local PKT_LEN       = 1500
local BATCH_SIZE = 15

local PKT_LEN2       = 128
local BATCH_SIZE2 = 15

local CHECK_TIME_EVERY = 100

local INIT_GAP = 10
local INIT_RATE = 1000 --1000
local INIT_DEMAND = 20000

-- the configure function is called on startup with a pre-initialized command line parser
function configure(parser)
	parser:description("Edit the source to modify constants like IPs and ports.")
	parser:argument("dev", "Devices to use."):args("+"):convert(tonumber)
	parser:option("-t --threads", "Number of threads per device."):args(1):convert(tonumber):default(1)
	return parser:parse()
end

function master(args,...)   
   for i, dev in ipairs(args.dev) do
      -- 3 tx queue
      --  to send data
      --  to send control packets
      --  to echo Rnd 2 control packets at source of flow
      -- 2 rx queues
      --  to recv data and control packets
      --  to recv Rnd 2 control packets at source of flow
      local dev = device.config{
	 port = dev,
	      txQueues = args.threads*3,
	      rxQueues = args.threads*2+1,
	      rssQueues = args.threads
      }
      args.dev[i] = dev
      
   end
   device.waitForLinks()

   -- print statistics
   stats.startStatsTask{devices = args.dev}

   local i = 1
   local dev = args.dev[i]
   for j = 1, args.threads do
      local queue = dev:getTxQueue(j - 1)
      local queue2 = dev:getTxQueue(j+1 - 1)
      local txSpQueue = dev:getTxQueue(j+2 - 1)
      local rxQueue = dev:getRxQueue(j - 1)
      local rxSpQueue = dev:getRxQueue(j+1 - 1)
      dev:fiveTupleFilter({			 
	    proto = ip.PROTO_ICMP
			  }, rxSpQueue)
      lm.startTask("txSlave", rxQueue, rxSpQueue, txSpQueue, queue, queue2, DST_MAC[3-i], DST_MAC_NO[3-i])
   end

   i = 2
   dev = args.dev[i]
   for j = 1, args.threads do
      lm.startTask("rxSlave", dev:getRxQueue(j - 1))
   end
   lm.waitForTasks()
end

function loadWorkload(filename)
   local wl = {}
   wl["name"] = {}
   wl["size"] = {}
   wl["start"] = {}
   wl["nsent"] = {}
   wl["nflows"] = 0
   wl["next_flow"] = 1
   
   local file = open(filename)
   local shift_start_us = 1000
   local prev_start = 0
   if file then
      for line in file:lines() do
	 local words = {}
	 word_no = 1
	 for w in  string.gmatch(line, "%S+") do
	    words[word_no] = w
	    word_no = word_no + 1
	    end
	 local size_mtus = ceil(words[1]/1500.0)
	 local index = wl.nflows + 1
	 wl.name[index] = index
	 wl.size[index] = size_mtus
	 wl.start[index] = shift_start_us+words[2]
	 wl.nsent[index] = 0
	 wl.nflows = wl.nflows + 1
	 log:info("flow " .. index .. " has size " .. size_mtus .. " starts at " .. wl.start[index] .. " ("
		     .. (wl.start[index]-prev_start) .. " us after previous flow)")
	 prev_start = wl.start[index]
      end
   end
   return wl
end

function setupTimingWheel(tw_num_slots, default)
   local tw = {}
   tw["num_slots"] = tw_num_slots
   tw["slots"] = {}
   tw["num_free"] = 0
   tw["current_slot"] = 0
   tw["max_rate"] = default["max_rate"] or 10000
   tw["safe_max_rate"] = default["safe_max_rate"] or 8000
   for i = 1, tw.num_slots do
      tw.slots[i] = 0
      tw.num_free = tw.num_free + 1
   end
   return tw
end

function setupDoublyLinkedList(num_items)
   local start_free = 1
   local prev_free = {}
   local next_free = {}
   for i = 1, num_items do
      prev_free[i] = i-1
      next_free[i] = i+1
      if i == 1 then
	 prev_free[i] = num_items
      elseif i == max_flows then
	 next_free[i] = 1
      end
   end
   return prev_free, next_free, start_free
end


function tick(tw)
      tw.current_slot = tw.current_slot + 1
      if (tw.current_slot >= tw.num_slots) then
	 --assert(tw.current_slot == tw.num_slots)
	 tw.current_slot = 0
      end
end

function get_next_free(tw, n)
	       -- reinsert in next empty slot after gap slots
	       -- could take 1000 steps if timing wheel is full
	       -- average case I guess < 10 but check
	       -- TODO(lav): if hotspot, then use linked list
	       -- of free slots (easy to maintain order since
	       -- they are freed in order)
	       -- and binary search to find
	       -- next free slot after current + gap
   --assert(n < tw.num_slots)
   local next_index = tw.current_slot + n
   if (next_index >= tw.num_slots) then
      next_index = next_index - tw.num_slots
   end
   --assert(next_index < tw.num_slots)
   --assert(next_index >= 0)
   
   while (tw.slots[next_index+1] > 0) do
      next_index = next_index + 1
      if (next_index == tw.num_slots) then
	 next_index = next_index - tw.num_slots
      end
   end
   return next_index
end
function cpgResetTheirFlowState(tf)
   tf["num_flows"] = 0
   tf["next_flow"] = 0
   tf["next_hop"] = 0
   tf["last_update"] = 0
   return
end

-- TODO: setupTheirFlowState name, nhops also nflows,
-- and global vars like next_flow, next_hop, last_update
-- all cpg control related state
function setupTheirFlowState()
   local tf = {}
   tf["num_flows"] = 0
   tf["next_flow"] = 0
   tf["next_hop"] = 0
   tf["last_update"] = 0
   tf["nhops"] = {}
   return tf
end

-- TODO: add cpg state i.e., demand, stable, last_update
function setupMyFlowState(max_flows,default)
   local mf = {}
   mf["gap"] = {}
   mf["rate"] = {}
   mf["nsent"] = {}
   mf["name"] = {}
   mf["size"] = {}
   mf["max_flows"] = max_flows
   mf["num_active"] = 0
   -- cpg control state
   mf["demand"] = {}
   mf["stable"] = {}
   for i, field in ipairs(mf) do
      assert(default[field] ~= nil)
      for j = 1, max_flows do
	 mf[field][j] = default[field]
      end
   end
   mf["next_free"], mf["prev_free"], mf["start_free"]
      = setupDoublyLinkedList(max_flows)
   mf["num_free"] = max_flows
   return mf
end

function removeExpiredFlows(expired_flows, mf, wl)
   -- could avoid per-flow ops here
   -- remove all expired flows
   local curr = mf.start_free
   local tmp = mf.next_free[mf.start_free]
   local ef  = expired_flows 
   while (ef) do
      mf.num_active = mf.num_active - 1
      --log:info("Removing flow ".. ef.flow_index.. ": ".. mf.name[ef.flow_index])
      wl.nsent[mf.name[ef.flow_index]] = ef.nsent
      mf.name[ef.flow_index] = -1
      mf.next_free[curr] = ef.flow_index
      curr = ef.flow_index
      mf.num_free = mf.num_free + 1
      ef = ef.next
   end
   mf.next_free[curr] = tmp
   mf.prev_free[tmp] = curr		   
end

function addNewFlows(mf, wl, tw, cpg_round)
   -- add new flows, until no more free timeslots or flow state
   local curr = mf.start_free
   local tmp = mf.prev_free[mf.start_free]
   local tw_insert_at = tw.current_slot
		   
   while wl.next_flow < wl.nflows and
      wl.start[wl.next_flow] <= (cpg_round * ROUND * 1e6)
   and mf.num_free > 0 and tw.num_free > 0 do
      mf.num_active = mf.num_active + 1
      mf.name[curr] = wl.name[wl.next_flow]
      mf.size[curr] = wl.size[wl.next_flow]
      mf.nsent[curr] = 0
      mf.gap[curr] = INIT_GAP_FLOW
      mf.rate[curr] = INIT_RATE_FLOW
      while tw.slots[tw_insert_at+1] > 0 and tw.num_free > 0 do
	 tw_insert_at = tw_insert_at+1
	 if (tw_insert_at == tw.num_slots) then
	    tw_insert_at = 0
	 end
      end
      -- insert first data packet/ SYN
      -- (also need to update rates of other flows)
      -- always leave room for first data packets.
      --assert(tw.slots[tw_insert_at+1] == 0)
      tw.slots[tw_insert_at+1] = curr
      tw.num_free = tw.num_free - 1
      --log:info("Adding new flow ".. curr.. ": ".. name_flow[curr].. " at "
      --		  .. tw_insert_at
      --		  .. " gap " .. gap_flow[curr] .. ", rate " .. rate_flow[curr]
      --	  .. ", scheduled to start " .. wl_start[next_new_flow] .. " us after T=0")
		      
      wl.next_flow = wl.next_flow + 1
		      
      curr = mf.next_free[curr]
      mf.num_free = mf.num_free - 1
   end
   mf.start_free = curr
   mf.prev_free[curr] = tmp
   mf.next_free[tmp] = curr
end

function txSlave(rxQueue, rxSpQueue, txSpQueue, txQueue, txQueue2, dstMac, dstMacNo)
   
   -- Set up 2 memory pool and 4 buffer arrays
   
   -- memory pool with default values for all packets, this is our archetype
   local mempool = memory.createMemPool(function(buf)
	 buf:getUdpPacket():fill{
	    -- fields not explicitly set here are initialized to reasonable defaults
	    ethSrc = txQueue, -- MAC of the tx device
	    ethDst = dstMac,
	    ip4Src = SRC_IP,
	    ip4Dst = DST_IP,
	    udpSrc = SRC_PORT,
	    udpDst = DST_PORT,
	    pktLength = PKT_LEN
				}
   end)

   local mempool2 = memory.createMemPool(function(buf)
	 buf:getUdpPacket():fill{
	    -- fields not explicitly set here are initialized to reasonable defaults
	    ethSrc = tx2Queue, -- MAC of the tx device
	    ethDst = dstMac,
	    ip4Src = SRC_IP,
	    ip4Dst = DST_IP,
	    udpSrc = SRC_PORT,
	    udpDst = DST_PORT,
	    pktLength = PKT_LEN2
				}
   end)

   -- a bufArray is just a list of buffers from a mempool that is processed as a single batch
   local bufs = mempool:bufArray(BATCH_SIZE)
   local bufs2 = mempool:bufArray(BATCH_SIZE2)
   local rxBufs = memory.bufArray()
   local fwdBufs = memory.bufArray()

   -- Workload info, by flow index (name, size in packet,
   --  start time in us). Load from workload file.
   local wl = loadWorkload("examples/flows-steady.txt")
   log:info("Loaded workload with " .. wl.nflows .. " flows")
   
   -- Set up timing wheel one slot = one packet
   local tw = setupTimingWheel(1000, {})
   -- Per flow state for active flows that start here
   local my_flows = setupMyFlowState(100,
				     {["gap"]= INIT_GAP,
					["rate"] = INIT_RATE,
					["nsent"] = 0,
					["name"] = -1,
					["size"] = 0,
					["demand"] = INIT_DEMAND,
					["stable"] = false})
   local their_flows = setupTheirFlowState()
   
   local expired_flows = nil

   -- Per flow state for active flows that start there
   
   -- Cpg rounds start at start_time, tentatively 3ms from now
   -- TODO: Need to sync this across machines so wall clock time
   -- is same, also well as correct for drifts
   local start_time = lm.getTime() + 1e-3
   log:info("Starting simulation at ", start_time)
   local cpg_round = 0
   local prev_cpg_round = -100


   -- for logging configured rates per Cpg round
   local common_gap = 0
   local common_rate = 0
   local prev_round_change_time = 0   
   local prep_start = nil
   local cpg_round_check = nil
   local num_sends = 0
   local flow_start = {}
   local flow_finish = {}

   
   while num_sends < 150000 and wl.next_flow < 1000 and lm.running() do
   --while lm.running() do -- check if Ctrl+c was pressed
      local lm_time = lm.getTime()
      cpg_round = ceil((lm_time - start_time)/ROUND)      
      -- code to benchmark time taken per batch
      if (num_sends > 0
	  and num_sends % CHECK_TIME_EVERY == 0) then
	 prep_start = {next=prep_start, value=lm_time}
      end

      -- check if cpg round changed
      -- TODO: when round is a multiple of 10 refresh all calculations?
      if cpg_round > 0 and cpg_round > prev_cpg_round then
	 --log:info("Cpg round " .. cpg_round .. " at time " .. lm_time .. " (" .. (lm_time-start_time)*1e6 .. " us from start)")
	 prev_cpg_round = cpg_round
	 cpg_round_check = {next=cpg_round_check, value=prev_round_change_time, nflows=my_flows.num_active, rate=common_rate}
	 prev_round_change_time = lm_time
	 if (cpg_round ~= 0 and cpg_round > prev_cpg_round+1) then
	    log:error("Cpg round jumped from "
			 .. prev_cpg_round .. " to "
			 .. cpg_round .. " in one iteration.")
	 end	   
	    
	 if (cpg_round%4 == 1) then
	    -- update tf.num_flows, tf.next_flow, tf.next_hop to 0 every round 1
	    -- no change to mf.xx
	    
	    -- on reset, we update set of flows and set demand to inf., stable to false, last_update to 0
	    
	    -- now we'll make this part of resetting Cpg calculatiosn
	    local old_num_active = my_flows.num_active
	    removeExpiredFlows(expired_flows, my_flows, wl)
	    expired_flows = nil
	    -- assert(my_flows.num_active >= 0)
	    -- assert(my_flows.num_active <= old_num_active)	    
	    -- assert(expiredFlows == nil)
	    old_num_active = my_flows.num_active
	    addNewFlows(my_flows, wl, tw, cpg_round)
	    -- assert(my_flows.num_active >= 0)
	    -- assert(my_flows.num_active >= old_num_active)
	    --log:info("After add new flows, next flow to start from workload is " .. wl.next_flow)
	    -- update rates of active flows
	    local new_rate = tw.safe_max_rate/my_flows.num_active
	    local new_gap = ceil(tw.max_rate/new_rate) -- max_rate/new_rate, a little slower for headroom
	    common_gap = new_gap
	    common_rate = tw.max_rate/new_gap
	    -- TODO(lav): linked list of active flows's state?
	    -- now we'll make this part of Cpg rate processing
	    for i=1, my_flows.max_flows do
	       if (my_flows.name[i] ~= -1) then
		  my_flows.gap[i] = new_gap
		  my_flows.rate[i] = new_rate
	       end
	    end
	 end
      end
      

      if (cpg_round > 0) then
	 -- Receiving data and control packets

	 lm_time = lm.getTime()
	 -- Sending data packets
	 bufs:alloc(PKT_LEN)
	 for i, buf in ipairs(bufs) do
	    local pkt = buf:getUdpPacket()
	    local next_flow = tw.slots[tw.current_slot+1]
	    if (next_flow > 0) then

	       -- Fill in data packet
	       local name = my_flows.name[next_flow]
	       local num_sent = my_flows.nsent[next_flow] + 1
	       local size = my_flows.size[next_flow]
	       -- important to update source MAC
	       -- else will use value in buffer (garbage/ old)
	       -- and possibly drop packet
	       pkt.eth:setSrc(0x253554d4500)
	       pkt.ip4:setSrc(SRC_IP_BASE + name)
	       my_flows.nsent[next_flow] = num_sent
	       if (num_sent == 1) then
		  flow_start[name] = lm_time
	       end

	       -- Add control info
	       -- if round 1 and last_update[next_flow] < 1 add control and set last_update
	       -- else if round 3 and last_update[next_flow] < 1 add control and set last_update
	       
	       -- Update timing wheel O(1)
	       tw.slots[tw.current_slot+1] = 0
	       if (num_sent < size) then	       
		  local next_index = get_next_free(tw, my_flows.gap[next_flow])
		  tw.slots[next_index+1] = next_flow
	       else -- if flow ended 
		  expired_flows =
		     {next = expired_flows, flow_index=next_flow, nsent=size}
		  flow_finish[name] = lm_time
		  --log:info("Finished sending flow " .. name_flow[next_flow] .. " in round " .. cpg_round)
	       end
	       
	    else
	       -- Drop void packets
	       pkt.eth:setSrc(dstMacNo)
	       -- so packet is dropped at switch
	    end
	    
	    -- update current tick per packet
	    --tick(tw)
	    tw.current_slot = tw.current_slot + 1
	    if (tw.current_slot == tw.num_slots) then
	       --assert(tw.current_slot == tw.num_slots)
	       tw.current_slot = 0
	    end
	 end
	 -- send out all packets and frees old bufs that have been sent
	 txQueue:send(bufs)

	 -- send control packets (how many?)
	 -- if round 1 and last_update[next_flow] < 1 add control and set last_update
	 -- else if round 3 and last_update[next_flow] < 1 add control and set last_update
	 -- else if round 4 then similar to round 2
	 -- else if round 2 fill in if last_update < cpg_round and
	 --   next_flow < tf.nflows and next_hop < tf.nhops[next_flow]
	 --   then fill in tf.name[next_flow], tf.next_hop++. If tf.next_hop = tf.nhops[next_flow]
	 --   then tf.next_flow++, tf.next_hop = 0. If tf.next_flow == tf.nflows then
	 --   last_update = cpg_round and tf.next_flow = 0 and tf.next_hop = 0
	 
	 -- recv control and data packets
	 -- local count = rxQueue:tryRecv(rxBufs)
	 -- for i = 1, count do
	 --    local buf = rxBufs[i]
	 --    local pkt = buf:getUdpPacket()
	 --    -- if round 1 control packet of their flow
	 --    -- if stable tf[flowId] = nhops !!!! and tf.nflows++ 
	 --    -- if round 4 control packet of my flow
	 --    --  update stable
	 --    --local src_ip = pkt.ip4:getSrc()
	 --    --local flow_id = src_ip - SRC_IP_BASE
	 --    buf:free()
	 -- end

	 -- round 2 echo
	 -- if cpg_round%2 == 0 then
	 --    local count = rxSpQueue:tryRecv(fwdBufs)
	 --    if count >= 1 then
	 --       for i = 1, count do
	 -- 	  local buf = fwdBufs[i]
	 -- 	  local pkt = buf:getUdpPacket()
	 -- 	  -- calculate rate
	 -- 	  -- update demand if rate's smaller
	 -- 	  -- echo
	 --       end
	 --       txSpQueue:sendN(fwdBufs, count)
	 --    end
	 -- end
	 
	 num_sends = num_sends + 1
      end
   end

   for i, name in ipairs(wl.name) do
      size = wl.size[i] or 0
      nsent = wl.nsent[name] or 0
      start = flow_start[name] or 0
      finish = flow_finish[name] or 0
      fct = ((8*nsent*PKT_LEN)/(finish-start))/1e6
      log:info("Flow # " .. i.. ": " .. name .. " 'sent' " .. nsent .. " packet with fct " .. fct .. " Mb/s "
		  .. " from " .. start*1e6 .. " us to " .. finish*1e6 .. " us")
   end

   -- local tmp = prep_start
   -- local prev = 0
   -- while tmp do
   --    local avg_diff = (((tmp.value - prev) * 1e6)/CHECK_TIME_EVERY)
   --    local mbps = ((BATCH_SIZE * PKT_LEN * 8))/avg_diff
   --    --+ (BATCH_SIZE2 * PKT_LEN2 * 8))/avg_diff
   --    log:info(tmp.value * 1e6 .. " us start prep, "
   -- 		  .. avg_diff
   -- 		  .. " us per batch of " .. BATCH_SIZE .. " "
   -- 		  .. PKT_LEN .. " B packets"
   -- 	       .. " for a rate of " .. mbps .. " Mb/s")
   --    -- .. " plus ".. BATCH_SIZE2 .. " " .. PKT_LEN2 .. " B packets"
   --    prev = tmp.value
   --    tmp = tmp.next
   -- end

   local tmp = cpg_round_check
   local rev = nil
   while tmp do
      rev = {next=rev, value=tmp.value, nflows=tmp.nflows, rate=tmp.rate}
      tmp = tmp.next
   end

   tmp = rev
   while tmp do
      --log:info("Cpg round check at " .. tmp.value*1e6 .. " us (" .. (diff*1e6) .. " us after last round check), num_active_flows in previous round " .. tmp.nflows .. " .")
      log:info((tmp.value-start_time)*1e6 .. " us, num_active_flows " .. tmp.nflows .. ", each with rate "
   	 .. tmp.rate .. " Mb/s for total " .. (tmp.nflows*tmp.rate) .. " Mb/s .")
      tmp = tmp.next
   end
   lm.stop()
end


function rxSlave(rxQueue)
	-- a bufArray is just a list of buffers that we will use for batched forwarding
   local flow_start = {}
   flow_start["test"] = 0
   local flow_finish = {}
   local flow_count = {}
   local bufs = memory.bufArray()
   while lm.running() do -- check if Ctrl+c was pressed
      -- receive one or more packets from the queue
      local lm_time = lm.getTime()
      local count = rxQueue:recv(bufs)
      for i = 1, count do
	 local buf = bufs[i]
	 local pkt = buf:getUdpPacket()
	 local src_ip = pkt.ip4:getSrc()
	 local flow_id = src_ip - SRC_IP_BASE
	 if flow_start[flow_id] == nil then
	    flow_start[flow_id] = lm_time
	    flow_count[flow_id] = 1
	 else
	    flow_finish[flow_id] = lm_time
	    flow_count[flow_id] = flow_count[flow_id] + 1
	 end
	 buf:free()
      end
      -- send out all received bufs on the other queue
      -- the bufs are free'd implicitly by this function
      --txQueue:sendN(bufs, count)
   end
   log:info("Packets received")
   for name, start in pairs(flow_start) do
      nsent = flow_count[name] or 0
      finish = flow_finish[name] or 0
      fct = ((8*nsent*PKT_LEN)/(finish-start))/1e6
      log:info("Flow " .. name .. ", received " .. nsent .. " packets with fct " .. fct .. " Mb/s"
		  .. " from " ..  (start*1e6) .. " us to " .. (finish*1e6))


   end
   
end


   -- say round 2 packets received on special queue
   
   -- their flow state: e.g., last_cpg
   -- for each packet we receive
   --  it could be a control packet for my flow [ignore my flow's control packets unless round 4]
   --   if it's a round 1 packet: not possible. (assert round is 1)
   --   if it's a round 2 packet: calculate rate, update min_rate, nlinks_for_min_rate and echo
   --   if it's a round 3 packet: not possible. (assert round is 1)
   --   if it's a round 4 packet: update stable and inc last_cpg_update.
   
   --  it could be a control packet for their flow [ignore their flow's control packet unless round 1]
   --    if it's a round 1 packet: if flow is unstable, set up state for flow [num_hops, last_round=1]
   --    if it's a round 2 packet: do nothing, update meant for switch
   --    if it's a round 3 packet: do nothing, update meant for switch
   --    if it's a round 4 packet: not possible.
   
   --  it could be a data packet for their flow
   --    update nrecvd, start, end

   
   -- SENDING CONTROL PACKETS FOR THEIR FLOWS (only have a bit of state built up in each round 1 and global state curr flow, last_update)
   -- if round is 2 and current flow is not nil and last_update is not 2
   -- loop to send their control packets
   --  for each pkt
   --  fill in link id x and inc x if current flow has > x hops else inc current flow and reset x
   --  set last_update to 2 when done   
   -- if round is 4 and current flow is not nil and last_update is not 4
   -- loop to send their control packets
   --  for each pkt
   --  send empty packet and inc current flow
   -- set last_update 4 when done and reset theirFlowsNum to 0.


   -- Not sure what Tx rate will be once we add Rx loop, echo loop, Tx control loop

   -- Resetting active flows every 100 rounds??
   
   -- RECEIVING PACKETS FOR ALL FLOWS
   -- loop to receive batch of 64 packets mix of control and data [control and mine and round 2 filtered out]
   --  if control and mine and round 4, update stable and inc last_cpg_update (assert round is 4)
   --  elseif theirs and round 1 and unstable, add flowId, nhops, lastRound to theirFlows[i++]
   --  if data update nrecvd, start, end
   --  no acks

   -- if round is 2
   -- special loop to receive MY control packets in round 2
   -- receive packets and for each packet, calculate rate, fill in
   -- return all packets

   -- SENDING PACKETS FOR ALL FLOWS
   -- Batch of 15 data packets
   --  if round is 1 and last_cpg_update is <1 add demand and stable and inc. last_cpg_update
   --  if round is 3 and last_cpg_update is <3 and flow is unstable add min_rate and inc. last_cpg_update

   -- if any thing left to send in this round for MINE or for THEIR flow
   -- Batch of 15 control packets
   -- if round is 1 and last_cpg_update is <1 add demand and stable and inc. last_cpg_update
   -- THEIR: if round is 2 and current flow is not nil and last_update is not 2 fill in link id x and inc x if current flow has > x hops else inc current flow and reset x
   --  and set last_update to 2 when done (tag q)   
   -- if round is 3 and last_cpg_update is <3 and flow is unstable add min_rate and inc. last_cpg_update
   -- THEIR:if round is 4 and current flow is not nil and last_update is not 4 send empty packet and inc current flow
   -- set last_update 4 when done and reset theirFlowsNum to 0.
   
   ----------------------------
   -- tentative- update last_cpg_update only when we're done with all
   -- our updates for the flow in that round
   
   -- my flow state: e.g., last_cpg_update = 0, demand, stable, min_rate, nlinks_for_min_rate, nlinks, nsent, flow_start, flow_end
   -- for each (data) packet we're going to send, then later for each flow see last_cpg_update and if needed send control
   --  if round is 1 and last_cpg_update is <1 add demand and stable and inc. last_cpg_update
   --  else if round is 2 and .. special handling in rx
   --  else if round is 3 and .. if flow is unstable add min_rate
   --  else if round is 4 and .. nothing to send

   -- separate loop to send control packets for their flows
   -- we just setup   state for their unstable flows during rx in round 1 [num_hops, last_round=1]
   -- could have a last_cpg_update_1 pointer and send in sequence
   -- if round is 1: we're in the process of learning about new unstable flows (we also reset?)
   -- if round is 2: send empty packet for each link and inc last_cpg_update
   -- if round is 3: nothing to send 
   -- if round is 4: send empty packet to pick up new status inc last_cpg_update
   -- (assert that we've sent all flows' packets in round 3 in round 4 etc.)
