local lm     = require "libmoon"
local dpdk        = require "dpdk"
local memory    = require "memory"
local device    = require "device"
local utils     = require "utils"
local log        = require "log"

local arp        = require "proto.arp"
local ip        = require "proto.ip4"
local icmp        = require "proto.icmp"

local OSNT_MAC          = "08:11:11:11:11:08"
local BCAST_MAC         = "ff:ff:ff:ff:ff:ff"
local PROTO_SYNC        = 210
local PROTO_SYNC_ACK    = 211
local PKT_SIZE          = 64
local PKT_CNT           = 2
local SRC_IP_BASE   = 167772160 -- "10.0.1"

local SRC_IP_1          = "10.0.1.1"
local SRC_IP_2          = "10.0.1.2"
local DST_IP            = "10.0.0.2"

-- checking if tsc of different cores and synchronized
-- dst ip based filter,
-- made following change to filter.lua fiveTupleFilter()
-- 		dst_ip = filter.dstIp,
--		dst_ip_mask = filter.dstIpMask or 0,
-- note filter ip in network order,
-- with dpdk's ixgbe / 82599, ip mask can only be full address (2^32-1) or ignore (0), no prefixes
function master()

   -- 4294967296
   local dev = device.config({port = 1, rxQueues = 5, txQueues = 5})

--   log:info("parseIPAddress(10.0.1.4) " .. parseIPAddress("10.0.1.4"))
--   result = ("10.0.1.4" and parseIPAddress("10.0.1.4"))
--   log:info(result)
   ok1 = dev:fiveTupleFilter({ dstIp = bswap(SRC_IP_BASE+10), dstIpMask=4294967295}, dev:getRxQueue(4))
   ok2 = dev:fiveTupleFilter({ dstIp = bswap(SRC_IP_BASE+2), dstIpMask=4294967295 }, dev:getRxQueue(2))
   ok3 = dev:fiveTupleFilter({ dstIp = bswap(SRC_IP_BASE+3), dstIpMask=4294967295 }, dev:getRxQueue(3))

   print(ok1)
   print(ok2)
   print(ok3)
   lm.startTask("startSlaveTask", dev:getRxQueue(4), dev:getTxQueue(4))
   lm.startTask("startSlaveTask", dev:getRxQueue(2), dev:getTxQueue(2))
   lm.startTask("startSlaveTask", dev:getRxQueue(3), dev:getTxQueue(3))


   local dev0 = device.config({port = 0, rxQueues = 1, txQueues = 1})
   lm.startTask("sendSync_wait", dev0:getTxQueue(0))

   lm.waitForTasks()
end

-- local DIGITS = { 1, 8 }
--
-- local states = {
--     "         ",
--     "    X    ",
--     "   XXX   ",
--     "  XXXXX  ",
--     " XXXXXXX ",
--     "XXXXXXXXX",
--     "XXXX XXXX",
--     "XXX   XXX",
--     "XX     XX",
--     "X       X",
-- }
--
-- for i, v in ipairs(states) do
--     states[i] = tonumber((v:gsub(" ", DIGITS[1]):gsub("X", DIGITS[2])))
-- end
--
-- local function getSymbol(step)
--     return states[step % #states + 1]
-- end


function sendSync_wait(txQueue)
   local mempool = memory.createMemPool(function(buf)
	 buf:getIP4Packet():fill{
	    ethSrc = txQueue,
	    ethDst = "08:22:22:22:22:08",
	    ip4Src = SRC_IP,
	    ip4Dst = DST_IP,
	    ip4Protocol = PROTO_SYNC,
	    pktLength = PKT_SIZE
				}
   end)
   local bufs = mempool:bufArray(3)
   bufs:alloc(PKT_SIZE)
   bufs:offloadIPChecksums()
   bufs[1]:getIP4Packet().ip4:setDst(SRC_IP_BASE+10)
   bufs[2]:getIP4Packet().ip4:setDst(SRC_IP_BASE+2)
   bufs[3]:getIP4Packet().ip4:setDst(SRC_IP_BASE+3)
   --bufs[1]:dump()
   txQueue:sendN(bufs, 3)
   for i, buf in ipairs(bufs) do
      if i > 3 then
	 buf:free()
      end
   end
end

-- Send 2 SYNC_ACK packet every second for 50 seconds
function startSlaveTask(rxQueue, txQueue)
   log:info("starting task to Send 2 SYNC_ACK packet every second for 50 seconds")
   
   local txMem = memory.createMemPool(function(buf)
	 buf:getIP4Packet():fill{
	    ethSrc = txQueue,
	    ethDst = OSNT_MAC,
	    ip4Src = SRC_IP_BASE,
	    ip4Dst = DST_IP,
	    ip4Protocol = PROTO_SYNC_ACK,
	    pktLength = PKT_SIZE
				}
   end)

   -- prepare packets 
   local txBufs = txMem:bufArray()
   --txBufs:offloadIPChecksums()	    
   txBufs:alloc(PKT_SIZE)
   log:info("prepared first batch of packets to send")
   
   local rxBufs = memory.bufArray()
   local sync_triggered = false

   local sent = 0


   local lm_times = nil
   local start_time = 0
   local max_sends = txBufs.size
   local prev_time = 0
   local lm_time = 0
   while lm.running() and sync_triggered==false do
      -- block until we get at least one packet
      rx = rxQueue:recv(rxBufs)
      lm_time = lm.getTime()
      prev_time = lm_time -- start counting 1s immediately
      start_time = lm_time
      if rx > 0 then
	 while lm.running() and sent+1 < max_sends do
	    while (lm_time < prev_time + 1) do
	       lm_time = lm.getTime()
	    end
	    prev_time = lm_time
	    --txQueue:sendSingle(txBufs[sent+1])
	    lm_times = {next=lm_times, value1=prev_time}
	 end
      end
   end


   log:info("Thread " .. txQueue.qid .. ": received " .. rx .. " packets at time " .. start_time*1e6 .. " us")
   for i=1,rx do
      rxBufs[i]:dump()
      rxBufs[i]:free()
   end

   local tmp = lm_times
   local last1 = start_time

   local i = 1
   local rev = nil
   while tmp do
      rev = {next=rev, value1=tmp.value1}
      tmp = tmp.next
   end
   
   tmp = rev
   while tmp do
      local diff1 = tmp.value1 - last1

      log:info("Thread " .. txQueue.qid .. ": Packets " .. i .. " sent at "
		  .. tmp.value1*1e6 .. " us"
		  .. "( " .. diff1 .. " s after previous packet)")
      last1 = tmp.value1
      tmp = tmp.next
      i = i + 1
   end
end
