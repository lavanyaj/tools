--- Tentative outline of final app
local lm     = require "libmoon"
local device = require "device"
local stats  = require "stats"
local log    = require "log"
local memory = require "memory"
local arp    = require "proto.arp"
local filter    = require "filter"
local ceil = math.ceil

--ifaddr = {"nf0": {"mac": "02:53:55:4d:45:00", "ip": "1.2.3.4"}
-- eth2      Link encap:Ethernet  HWaddr 0c:c4:7a:b7:60:3c   dev 0
-- eth3      Link encap:Ethernet  HWaddr 0c:c4:7a:b7:60:3d   dev 1
-- set addresses here

local DST_MAC       = "08:22:22:22:22:08" -- "08:11:11:11:11:08" -- --"02:53:55:4d:45:00" --"0c:c4:7a:b7:60:3d"
local DST_MAC_NO = 8942694572552 -- 8869393797384 -- 0x81111111108 -- 8942694572552 --0x82222222208
local PKT_LEN       = 1500
local SRC_IP        = "10.0.0.0" -- "10.0.0.10"
local SRC_IP_BASE   = 167772160 -- "10.0.0" -- "10.0.0.10"
local DST_IP        = "1.2.3.4" --"10.1.0.10"
local SRC_PORT_BASE = 1000 
local DST_PORT      = 1234
local NUM_FLOWS     = 1
local ETH_DROP = 0x1234

local CPG_INIT_DEMAND = 1e12

-- the configure function is called on startup with a pre-initialized command line parser
function configure(parser)
	parser:description("Edit the source to modify constants like IPs and ports.")
	parser:argument("dev", "Devices to use."):args("+"):convert(tonumber)
	parser:option("-t --threads", "Number of threads per device."):args(1):convert(tonumber):default(1)
	return parser:parse()
end

function master(args,...)
   -- configure devices and queues
   -- each device generates data packets
   -- and receives data/ ack packets

   -- workload in app function
   for i, dev in ipairs(args.dev) do
	   -- arp needs extra queues
	   local dev = device.config{
	      port = dev,
	      txQueues = args.threads,
	      rxQueues = args.threads+1,
	      rssQueues = args.threads
	   }
	   args.dev[i] = dev
	   
   end
   device.waitForLinks()

   -- print statistics
   stats.startStatsTask{devices = args.dev}

   -- configure tx rates and start transmit slaves
   local setup = {}
   if (setup[0] ==  nil) then
      log:info("setup[0]: nil")
   end
   for i, dev in ipairs(args.dev) do
      local dev = args.dev[i]
      for i = 1, args.threads do
	 local txQueue = dev:getTxQueue(i - 1)
	 local rxQueue = dev:getRxQueue(i - 1)
	 lm.startTask("app", txQueue, DST_MAC, DST_MAC_NO, rxQueue)
      end
   end
   
   lm.waitForTasks()
end

function app(txQueue, dstMac, dstMacNo, rxQueue)

   
   -- memory pool with default values for all packets, this is our archetype
   local mempool = memory.createMemPool(function(buf)
	 buf:getUdpPacket():fill{
	    -- fields not explicitly set here are initialized to reasonable defaults
	    ethSrc = txQueue, -- MAC of the tx device
	    ethDst = dstMac,
	    ip4Src = SRC_IP,
	    ip4Dst = DST_IP,
	    udpSrc = SRC_PORT,
	    udpDst = DST_PORT,
	    pktLength = PKT_LEN
				}
   end)

   local r = 0

   -- Workload info, by flow index (name, size in packet, start time in us)
   -- To be pre-calculated and filled in
   local wl_name = {}
   local wl_size = {}
   local wl_start = {}

   -- Set up timing wheel
   -- Here, we're assuming minimum speed we support is 10 Mb/s i.e.,
   -- some link could have >= 100 unsaturated flows (very unlikely)
   tw_num_slots = 1000
   tw_slots = {}
   for i = 1, tw_num_slots do
      tw_slots[i] = 0
   end
   -- when indexing into array, always +1
   local tw_current_slot = 0
   -- smallest slot is 1/tw_max_rate, 1.2 us for 10 Gb/s
   local tw_max_rate = 10000

   -- Per flow state for data plane, only for active flows
   local max_flows = 10
   local gap_flow = {}
   local rate_flow = {}
   local nsent_flow = {}
   local name_flow = {}
   local size_flow = {}
   for i = 1, max_flows do
      gap_flow[i] = ceil(1/max_flows)
      rate_flow[i] = tw_max_rate/max_flows
      nsent_flow[i] = 0
      size_flow[i] = 0
      name_flow[i] = -1
   end

   -- Per flow state for Cpg control
   local cpg_demand_flow = {}
   local cpg_stable_flow = {}
   local cpg_last_update_in_round_flow = {}

   local cpg_round = 0
   local next_new_flow = 1
   
   -- Insert first packet of flow 1 into current slot
   tw_slots[tw_current_slot+1] = 1
   
   -- local rates = {2000, 5000, 10000}
   -- local next_rate = rates[r+1]

   -- a bufArray is just a list of buffers from a mempool that is processed as a single batch
   -- we'll have one list used to receive data or ACK packets (and reply)
   -- and another list to send data packets
   -- tentatively planning to include control info in the data or ACK packets
   -- instead of generating more packets.
   local bufs = mempool:bufArray()
   local forward_bufs = memory.bufArray()

   -- Cpg rounds start from nearest even millisecdon in the future
   -- TODO: Need to sync this start time across machines so wall clock time
   -- is same, also well as correct for drifts
   local now = lm.getTime()*1e6
   local start_time = ceil(now/1000) * 1000
   
   local num_sends = 0
   --while num_sends < 1000 and lm.running do
   while lm.running() do -- check if Ctrl+c was pressed

      -- Check if we need to update Cpg round after
      -- each batch of receive and send
      if (num_sends%1 == 0) then
	 local curr_time = lm.getTime()*1e6
	 local tmp = ceil((curr_time-start_time)/100)
	 if tmp > cpg_round then
	    assert(tmp == cpg_round+1)
	    cpg_round = tmp
	 end

	 -- Update list of flows every 4 rounds
	 -- No big changes for other rounds
	 if cpg_round%4 == 0 then
	    -- remove old flows
	    for i = 1, max_flows do	       
	       if (size_flow[i] == nsent_flow[i]) then
		  name_flow[i] = -1
	       end
	       cpg_demand_flow[i] = CPG_INIT_DEMAND
	    end
	    -- add new flow
	    local add_at = 1
	    while next_new_flow < wl_nflows and start[next_new_flow] <= curr_time do
	       while add_at < max_flows and name_flow[add_at] > 0 do
		  add_at = add_at+1
	       end
	       name_flow[add_at] = wl_name[next_new_flow]
	       size_flow[add_at] = wl_size[next_new_flow]
	       nsent_flow[add_at] = 0
	       gap_flow[add_at] = ceil(1/max_flows)
	       rate_flow[add_at] = tw_max_rate/max_flows
	       next_new_flow = next_new_flow+1

	       cpg_last_update_in_round_flow[add_at] = 0
	    end
	 end
      end

      -- an application might be a source for some flows
      -- and destination for other flows
      -- so need to receive both DATA and ACK packets

      -- Will this still run at 10G with receive, forward and send??

      -- RECEIVE DATA AND ACK PACKETS FROM RXQUEUE AND REPLY ON TXQUEUE
      -- (DIFF FROM TXQUEUE TO SEND DATA PACKETS SO PIPELINE)
      -- Do we send ACKS for every packet??
      -- Do we use timeslots for ACKs??
      -- Maybe reserve 9G for data and 1G for ACKs?
      local count = rxQueue:recv(forward_bufs)
      for i, buf in ipairs(forward_bufs) do
	 -- Swap source and destination
	 -- if (ACK) then mark buf as void, so it will be dropped by switch
	 
	 -- Cpg updates
	 if cpg_stable_flow[flow_index] == 0 and cpg_round > cpg_last_update_in_round_flow then
	    if (cpg_round%4 == 1) then
	       -- when pkt w sumsat etc. received,
	       -- calculate rate and set demand = rate if rate smaller
	       -- can update gap once we hear from all hops
	       -- TODO: we can find number of hops from any of the control pkts

	       -- Update flow's inter-packet arrival time for next
	       -- time it's re-scheduled
	       
	       --local flow_index = 1
	       --rate_flow[flow_index] = next_rate
	       --gap_flow[flow_index] = ceil(tw_max_rate/rate_flow[flow_index])	 
	       --r = (r + 1)%3
	       --next_rate = rates[r+1]
	    elseif (cpg_round%4 == 3) then
	       -- handle in rx and set flow to stable if any link is stable
	    end
	 end
      end
      txQueue:sendN(forward_bufs, count)


      -- SEND DATA PACKETS ON TXQUEUE
      bufs:alloc(PKT_LEN)
      for i, buf in ipairs(bufs) do
	 local pkt = buf:getUdpPacket()

	 -- Check whether we send a void packet or a flow packet
	 
	 -- Note: If each slot is 0.12us instead of actual packet time
	 -- then every 10 empty slots gains you
	 -- a chance to send a void packet (check..)
	 if (tw_slots[tw_current_slot+1] > 0) then
	    -- Time to send a flow packet
	    local flow_index = tw_slots[tw_current_slot+1]
	    -- important to update source else will use value in buffer (garbage/ old)
	    pkt.eth:setSrc(0x253554d4500)
	    pkt.ip4:setSrc(SRC_IP_BASE + (name_flow[flow_index]%255))

	    -- Update Cpg
	    if cpg_stable_flow[flow_index] == 0 and cpg_round > cpg_last_update_in_round_flow then
	       if (cpg_round%4 == 0) then
		  -- fill in demand and unstable
	       elseif (cpg_round%4 == 2) then
		  -- fill in demand only
	       end
	    end
	    
	    -- Update timing wheel O(1)
	    -- and reschedule flow
	    tw_slots[tw_current_slot+1] = 0
	    local next_index = tw_current_slot + gap_flow[flow_index]
	    if (next_index >= tw_num_slots) then
	       next_index = next_index - tw_num_slots
	    end

	    --assert(tw_slots[tw_current_slot+1] == 0)
	    --assert(tw_slots[next_index+1] == 0)
	    --assert(next_index < tw_num_slots)

	    -- Find next empty slot
	    --while (tw_slots[next_index+1] > 0) do
	    -- next_index = next_index+1
	    -- if (next_index >= tw_num_slots) then
	    -- next_index = 0
	    -- if next_index == current_slot then
	    -- alert(flow has no more room)
	    -- break
	    -- end
	    -- end
	    tw_slots[next_index+1] = 1
	    --end
	    nsent_flow[flow_index] = nsent_flow[flow_index] + 1
	 else
	    -- Time to send a void packet
	    pkt.eth:setSrc(dstMacNo) -- so packet is dropped at switch
	 end
	 
	 tw_current_slot = tw_current_slot + 1
	 if (tw_current_slot >= tw_num_slots) then
	    --assert(tw_current_slot == tw_num_slots)
	    tw_current_slot = 0
	 end
      end
      -- send out all packets and frees old bufs that have been sent
      queue:send(bufs)
      num_sends = num_sends + 1
   end
   log:info("sent %d packets out of ~%d", nsent_flow, (num_sends * 63))
end



