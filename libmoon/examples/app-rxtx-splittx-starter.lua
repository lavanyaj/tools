--- A simple UDP packet generator. Generates packets for given flow schedule.
--- Notion of Cpg round = (current time - given start time)/ xx us
--- Flows are added to active list every 4th Cpg round
--- Use circular array of tw_num_slots slots to schedule packets of flows
--- Send BATCH_SIZE packets every loop with (hopefully) few operations/ pkt
--- Current slot in tw_num_slots ticks with each packet in batch.. so if
--- packets are being sent at line rate, a slot = Line rate tx time of pkt.
--- A flow that hasn't finished in re-inserted n slots in the future where
--- n is inter-packet time (actually rounded up to nearest multiple of 1.2us)
--- for flow.
--- Jan 9th: load flow schedule from file. count flow stats at receiver.
---  update all active flow's rates to max_rate/N every 4th Cpg round.
---  log flow stats at Tx and Rx. simple rdtsc check time every iteration.
--- Jan 10th: updated workload to try steady flows, configure rates as C/N every 4 rnds. Logging.
--- Jan 11th: refactored tw and flow state updates, added my_flow and their_flow state for cpg
---   calculations. Added a bunch of code (in comments) for Cpg updates to test incrementally

local lm     = require "libmoon"
local device = require "device"
local barrier = require "barrier"
local  array32       = require "swsr"
local stats  = require "stats"
local array64 = require " array64"
local log    = require "log"
local memory = require "memory"
local arp    = require "proto.arp"
local ip    = require "proto.ip4"
local cpg = require "proto.cpg"
local ethernet    = require "proto.ethernet"
local filter    = require "filter"
local open = io.open
local ceil = math.ceil
local ffi = require "ffi"
local max = math.max



--ifaddr = {"nf0": {"mac": "02:53:55:4d:45:00", "ip": "1.2.3.4"}
-- eth2      Link encap:Ethernet  HWaddr 0c:c4:7a:b7:60:3c   dev 0
-- eth3      Link encap:Ethernet  HWaddr 0c:c4:7a:b7:60:3d   dev 1
-- set addresses here

-- for every packet must set SRC_MAC SENDER/RECEIVER, SRC_IP to flow_id, DST_MAC to dstMac, DST_IP to SENDER/RECEIVER*256+protocol, proto
local DST_MAC_NO = {}
local DST_MAC = {}
DST_MAC_NO[2] = 8942694572552
DST_MAC[2] = "08:22:22:22:22:08" --"0c:c4:7a:b7:60:3d" --"02:53:55:4d:45:00" -- resolved via ARP on GW_IP or DST_IP, can be overriden with a string here
DST_MAC_NO[1] = 8869393797384
DST_MAC[1] = "08:11:11:11:11:08" --"0c:c4:7a:b7:60:3d" --"02:53:55:4d:45:00" -- resolved via ARP on GW_IP or DST_IP, can be overriden with a string here
--local DST_MAC       = "08:22:22:22:22:08" -- "08:11:11:11:11:08" -- --"02:53:55:4d:45:00" --"0c:c4:7a:b7:60:3d"
--local DST_MAC_NO = 8942694572552 -- 8869393797384 -- 0x81111111108 -- 8942694572552 --0x82222222208
local SRC_IP        = "10.0.0.0" -- "10.0.0.10"
local SRC_IP_BASE   = 167772160 -- "10.0.0" -- "10.0.0.10"
local DST_IP        = "1.2.3.4" --"10.1.0.10"
local SRC_PORT_BASE = 1000 
local DST_PORT      = 1234
local NUM_FLOWS     = 1
local ETH_DROP = 0x1234
local ROUND = 600e-6

-- checks every 15-20 us .. sends 15 packets every 15-20us enough to send all flows in this time.. and bw??

local TIMESTAMP_FOLLOWS = 0xbadcab
local CPG_ROUND_FOLLOWS = 0xdabada
local UINT32_MAX = 4294967295
local PKT_LEN       = 1500
local BATCH_SIZE = 10
-- BATCH_SIZE 31 gives line rate data but control packet RTTs can be in seconds!? and many packets out of round
-- BATCH_SIZE 15 gives 8.4 Gb/s rate data and more packets in correct round than not

local PKT_LEN2       = 128
local BATCH_SIZE2 = 5

local CHECK_TIME_EVERY = 100

local INIT_GAP = 20
local INIT_RATE = 177 --5000 --3333 --1000
local INIT_DEMAND = 20000
local LINK_CAPACITY = 8000

local MAC_FROM_SENDER = 0xaabbccddeeff
local MAC_FROM_RECEIVER = 0x112233445566
local FROM_SENDER = 5
local FROM_RECEIVER = 6

local kind = {}
kind[200] = "ip.PROTO_CONTROL_1"
kind[201] = "ip.PROTO_CONTROL_2_rvs"
kind[202] = "ip.PROTO_CONTROL_2_fwd"
kind[203] = "ip.PROTO_CONTROL_3"
kind[204] = "ip.PROTO_CONTROL_4"
kind[205] = "ip.PROTO_CONTROL_STATS"
kind[206] = "ip.PROTO_DATA"

	
-- the configure function is called on startup with a pre-initialized command line parser
function configure(parser)
	parser:description("Edit the source to modify constants like IPs and ports.")
	parser:argument("dev", "Devices to use."):args("+"):convert(tonumber)
	parser:option("-t --threads", "Number of threads per device."):args(1):convert(tonumber):default(1)
	return parser:parse()
end

function get_round_task(round_info, start_time)
   local prev_cpg_round = -100
   local cpg_round = 0
   local cpg_type = -1

   round_info:write(0, 1)
   
   local lm_time = getTimeFast() 
   if (started == false) then
      while (lm_time < start_time) do
	 lm_time = getTimeFast()	    
      end
      started = true
   end

   while lm.running() do
      lm_time = getTimeFast()
      cpg_round = ceil((lm_time - start_time)/ROUND)
      
      -- check if cpg round changed
      if cpg_round > prev_cpg_round then
	 --log:info("Task set round to " .. cpg_round)
         round_info:write(cpg_round, 1)	 
      	 prev_cpg_round = cpg_round
      end
  end
  
end

function get_round_from_task(round_info, prev_cpg_round)   
   local cpg_round = round_info:read(1)
   assert(cpg_round ~= nil)
   assert(prev_cpg_round ~= nil)
   if cpg_round > prev_cpg_round or prev_cpg_round == 0 then
      cpg_type = cpg_round%10 + 1
      -- 1 : time to send CONTROL_1 -- can start receiving CONTROL_1
      -- 2 : last time to receive CONTROL_1
      -- 3 : time to send CONTROL_2_rvs
      -- 4 : time to receive CONTROL_2_rvs
      -- 5 : time to send CONTROL_2_fwd -- can start receiving CONTROL_2_fwd
      -- 6 : last time to receive CONTROL_2_fwd
      -- 7 : time to  send CONTROL_3
      -- 8 : last time to receive CONTROL_3
      -- 9 : time to send CONTROL_4
      -- 10 : last_time to receive CONTROL_4
      return cpg_round, cpg_type
   else
      return prev_cpg_round, 0
   end
end

function master(args,...)   
   for i, dev in ipairs(args.dev) do
      local dev = device.config{
	 port = dev,
	      txQueues = 4,
	      rxQueues = 5,
      }
      args.dev[i] = dev
      
   end
   device.waitForLinks()

   -- print statistics
   -- stats.startStatsTask{devices = args.dev}


   local lm_time_start = lm.getTime()
   local mt_start = getMonotonicTime()
   local offset_start = lm_time_start-mt_start
   log_info("current clock time " .. mt_start .. " seconds")
   log_info("current+1 time " .. lm_time_start .. " seconds")
   log_info("offset " .. offset_start .. " seconds")

   local my_filename = {}
   local their_filename = {}
   my_filename[1] = "examples/flows-small.txt"
   my_filename[2] = ""
   their_filename[1] = ""
   their_filename[2] = "examples/flows-small.txt"

   math.randomseed(os.time())
   run_id = math.ceil(math.random()*UINT32_MAX)
   local start_time = getTimeFast() + 1e-3
   log_info("Starting simulation at ", start_time)

   shared_round_info = array32:new(4, 0)
   lm.startTask("get_round_task", shared_round_info, start_time)

   shared_demands = array64:new(100, INIT_DEMAND)
   shared_gaps = array632new(100, INIT_GAP)
   shared_stable = array32:new(100, 0)
   
   for i = 1, 2 do
      local dev = args.dev[i]
      
      local queueData = dev:getTxQueue(0) -- sending data packets with possible control info
      local queue1Demand = dev:getTxQueue(1) -- send control for my flows' round 1/3 (demand, bottleneck)
      local queue2FairShare = dev:getTxQueue(2) -- sending control packets for my flows round 2 (fairShareRate)
      local rxQueue4Stable = dev:getRxQueue(1) -- receiving control packets for my flows' round 4 (stable)
      local rxQueue2SumSat = dev:getRxQueue(2) -- receiving control packets for my flows' round 2 (sumsat etc.)

      local queue2And4 = dev:getTxQueue(3) -- sending control packets for their flows' rounds 2/ 4 by rx
      local rxQueue1Hops = dev:getRxQueue(4) -- recv control packets for flows' their flows' round 1 (hops)

      local rxQueueData = dev:getRxQueue(0) -- receiving data packets
      local rxQueueControl = dev:getRxQueue(3) -- receiving other control packets (sync stats)


      
      assert(UINT32_MAX ~= nil)
      ok1 = dev:fiveTupleFilter({dstIp=bswap(SRC_IP_BASE+FROM_RECEIVER*256+ip.PROTO_CONTROL_4),
				 dstIpMask=UINT32_MAX}, rxQueue4Stable)
      ok2 = dev:fiveTupleFilter({dstIp=bswap(SRC_IP_BASE+FROM_RECEIVER*256+ip.PROTO_CONTROL_2_rvs),
				 dstIpMask=UINT32_MAX}, rxQueue2SumSat)
      ok3 = dev:fiveTupleFilter({dstIp=bswap(SRC_IP_BASE+FROM_SENDER*256+ip.PROTO_CONTROL_1),
				 dstIpMask=UINT32_MAX}, rxQueue1Hops)
      -- just receives control packets and checks if they're in sync
      ok4 = dev:fiveTupleFilter({dstIp=bswap(SRC_IP_BASE+FROM_SENDER*256+ip.PROTO_CONTROL_2_fwd),
				 dstIpMask=UINT32_MAX}, rxQueueControl)
      ok5 = dev:fiveTupleFilter({dstIp=bswap(SRC_IP_BASE+FROM_SENDER*256+ip.PROTO_CONTROL_3),
				 dstIpMask=UINT32_MAX}, rxQueueControl)

      -- can put lm_time in sent control packet and check RTT in receiver thread
      assert(ok1)
      assert(ok2)
      assert(ok3)
      assert(ok4)
      assert(ok5)
      -- all other packets received go to default queue 0, tobe handled
      --  by rx slave (their flows' control packets for round 1, 2, 3 [ignore] and their flows' data packets)
 
      log_info("Run_id " .. run_id)
      if (i==1) then
	 -- Separate data from control and share only gaps?
	 lm.startTask("txData", my_filename[i], shared_gaps,
		      queueData, DST_MAC[3-i], DST_MAC_NO[3-i], DST_MAC_NO[i], ("txData-" .. i) , run_id)
		      
	 lm.startTask("txTask", my_filename[i],
		      start_time, shared_round_info,  shared_demands, shared_gaps, shared_stable,
		      rxQueue4Stable, rxQueue2SumSat,
		      queueData, queue1Demand, dueue2FairShare,
		      DST_MAC[3-i], DST_MAC_NO[3-i], DST_MAC_NO[i], ("txTask-" .. i) , run_id)

      end

      if (i==2) then
	 -- This thread receives CONTROL_1 packets, gets number of hops and flow id
	 -- so it can send empty packets per flow per hop in round type 2
	 -- and per flow in round type 4
	 lm.startTask("rxControlProcess", start_time, shared_round_info, rxQueue1Hops, queue2And4,
		      DST_MAC[3-i], DST_MAC_NO[3-i], DST_MAC_NO[i], ("rxControlProcess-"..i), run_id)

	 -- This thread receives data packets only
	 lm.startTask("rxData", their_filename[i], rxQueueData, ("rxData-"..i), run_id)

	 -- This thread just exists to receive control packets that the receiver
	 -- doesn't have to process and check that they finished in the same
	 -- round they were sent
	 lm.startTask("rxControlStats", start_time, shared_round_info,
		      rxQueueControl, ("rxControlStats-"..i), run_id)
      end
   end

   lm.waitForTasks()
end

function log_info(s)
   return
end


function show_sync_stats(incorrect_round, correct_round, max_async, sample_out_sync,
			 num_my_packets, num_their_packets, num_unknown_packets,
			 task, expected_protos)
   expected_protos = expected_protos or "" 
   log:info(task .. ": Sync stats for " .. expected_protos)
   for k, num in pairs(incorrect_round) do
      local tmp = max_async[k] or "n/a"
      log:info(task ..
		  " got " .. (correct_round[k] or 0) .. " " .. (kind[k] or k) .. " packets in the correct round"
		  .. " and " .. num ..  " packets in the incorrect round with max_async " .. tmp)
   end
   
   for k, num in pairs(correct_round) do
      if incorrect_round[k] == nil then
	 local tmp = max_async[k] or "n/a"
	 log:info(task ..
		     " got " .. (correct_round[k] or 0) .. " " .. (kind[k] or k) .. " packets in the correct round")
      end
   end
   log:info(task .. ": Sample out of sync stats")
   local tmp = sample_out_of_sync
   while (tmp) do
      log:info(task .. ": Round " .. (kind[tmp.proto] or tmp.proto)
		  .. " out of sync packet was sent in " ..  tonumber(tmp.sent_cpg_round)
		  .. ", recvd in " .. tonumber(tmp.now) .. " off by " .. tonumber(tmp.now-tmp.sent_cpg_round))
      tmp = tmp.next
   end

   log:info(task .. ": Packet counts by proto")
   for k, num in pairs(num_my_packets) do
      local kind_desc = kind[k] or k
      log:info(task .. ": " .. num .. " packets of my flows of type " .. kind_desc)
   end
   for k, num in pairs(num_their_packets) do
      local kind_desc = kind[k] or k
      log:info(task .. ": " .. num .. " packets of their flows of type " .. kind_desc)
   end
   for k, num in pairs(num_unknown_packets) do
      local kind_desc = kind[k] or k
      log:info(task .. ": " .. num .. " packets of unknown flows of type " .. kind_desc)
   end
end

function update_sync_stats(buf, run_id, cpg_round, correct_round, incorrect_round, max_async,
			   num_their_packets, num_my_packets, num_unknown_packets, task)
   local pkt = buf:getIP4Packet()
   local proto = pkt.ip4:getProtocol()
   local src_mac = pkt.eth:getSrc()
   
   if src_mac == MAC_FROM_SENDER then
      num_their_packets[proto] = (num_their_packets[proto] or 0) +1
   elseif src_mac == MAC_FROM_RECEIVER then
      num_my_packets[proto] = (num_my_packets[proto] or 0) + 1
   else
      num_unknown_packets[proto] = (num_unknown_packets[proto] or 0) + 1
   end

   -- update stats about out of sync packets
   if (proto ~= ip.PROTO_DATA
	  and (pkt.payload.uint32[5] ~= CPG_ROUND_FOLLOWS
	       or pkt.payload.uint32[7] ~= run_id)) then	 	 
      log:error(task .. " received round "
		   .. (kind[proto] or proto) .. " control packets with run_id "
		   .. pkt.payload.uint32[7] .. " and CPG_ROUND_FOLLOWS " .. pkt.payload.uint32[5]
		   .. " and sent_cpg_round " .. tonumber(pkt.payload.uint32[6])
		   .. " in round " .. cpg_round)
   else
      local sent_cpg_round = tonumber(pkt.payload.uint32[6])
      if (sent_cpg_round == cpg_round) then
	 correct_round[proto] = (correct_round[proto] or 0) + 1
      else
	 incorrect_round[proto] = (incorrect_round[proto] or 0) + 1	       		     
	 local tmp = max_async[proto] or 0
	 local diff = (cpg_round - sent_cpg_round)
	 max_async[proto] = max(max(tmp, diff), -diff)
	 if (diff ~= 0) then
	    log:error((kind[proto] or proto) .. " packet from " .. tonumber(sent_cpg_round)
		  .. " recvd in " .. tonumber(cpg_round))
	 end
      end      	    	    
   end
end

function getTimeFast()
   return getMonotonicTime()
end

function loadWorkload(filename)
   local wl = {}
   wl["name"] = {}
   wl["size"] = {}
   wl["start"] = {}
   wl["nsent"] = {}
   wl["nflows"] = 0
   if filename == nil then
      log_info("Filename was nil")
      return wl
   end
   
   local file = open(filename)

   local prev_start = 0
   if file then
      for line in file:lines() do
	 local words = {}
	 word_no = 1
	 for w in  string.gmatch(line, "%S+") do
	    words[word_no] = w
	    word_no = word_no + 1
	    end
	 local size_mtus = ceil(words[1]/1500.0)
	 local index = wl.nflows + 1
	 wl.name[index] = index
	 wl.size[index] = size_mtus
	 wl.start[index] = tonumber(words[2])
	 wl.nsent[index] = 0
	 wl.nflows = wl.nflows + 1
	 log_info("flow " .. index .. " has size " .. size_mtus .. " starts at " .. wl.start[index] .. " ("
		     .. (wl.start[index]-prev_start) .. " us after previous flow)")
	 prev_start = wl.start[index]
      end
   end
   return wl
end

function setupTimingWheel(tw_num_slots, default)
   local tw = {}
   tw["num_slots"] = tw_num_slots
   tw["slots"] = {}
   tw["num_free"] = 0
   tw["current_slot"] = 0
   tw["max_rate"] = default["max_rate"] or 10000
   tw["safe_max_rate"] = default["safe_max_rate"] or 8000
   for i = 1, tw.num_slots do
      tw.slots[i] = 0
      tw.num_free = tw.num_free + 1
   end
   return tw
end


function tick(tw)
   tw.current_slot = (tw.current_slot + 1)%tw.num_slots
end

function get_next_free(tw, n)
   --assert(n < tw.num_slots)
   local next_index = (tw.current_slot + n)%tw.num_slots
   while (tw.slots[next_index+1] > 0) do
      next_index = (next_index + 1)%tw.num_slots
   end
   return next_index
end

function cpgResetTheirFlowState(tf)
   tf["num_flows"] = 0
   tf["next_flow"] = 1
   tf["next_hop"] = 0
   tf["last_update"] = 0
   return
end

-- TODO: setupTheirFlowState name, nhops also nflows,
-- and global vars like next_flow, next_hop, last_update
-- all cpg control related state
function setupTheirFlowState()
   local tf = {}
   tf["num_flows"] = 0
   tf["next_flow"] = 1
   tf["next_hop"] = 0
   tf["last_update"] = 0
   tf["flow_id"] = {}
   tf["nhops"] = {}
   return tf
end

-- TODO: add cpg state i.e., demand, stable, last_update
function setupMyFlowState(wl, tw, default)
   local mf = {}
   mf["gap"] = {}
   mf["rate"] = {}
   mf["nsent"] = {}
   mf["name"] = {}
   mf["size"] = {}
   mf["flow_index"] = {}
   
   -- cpg control state
   mf["demand"] = {}
   mf["stable"] = {}
   mf["last_update"] = {}
   mf["next_flow"] = 1
   mf["global_last_update"] = 0
   mf["num_flows_up_to_date"] = 0

   local tw_insert_at = 0
   assert(tw_insert_at == 0)
   for j= 1, wl.nflows do
      assert(tw.num_free > 0)
      mf["gap"][j] = default["gap"]
      mf["rate"][j] = default["rate"]
      mf["nsent"][j] = 0
      mf["name"][j] = wl.name[j]
      mf["size"][j] = wl.size[j]
      mf["flow_index"][wl.name[j]] = j
      mf["demand"][j] = default["demand"]
      mf["stable"][j] = default["stable"]
      mf["last_update"][j] = 0
      assert(tw.slots[tw_insert_at+1] == 0)
      tw.slots[tw_insert_at+1] = j
      tw.num_free = tw.num_free - 1
      tw_insert_at = tw_insert_at+1
      if (tw_insert_at == tw.num_slots) then
	 tw_insert_at = 0
      end
   end

   mf["max_flows"] = wl.nflows
   mf["num_active"] = 0
   return mf
end


-- TO TEST
function fillControlPacketsForRound1(bufs2, mf, cpg_round, run_id)
   local i = 1
   while i < bufs2.size
      and mf.next_flow <= mf.max_flows
   and mf.global_last_update < cpg_round do
      -- get next flow whose round 1 packet needs to be sent
      while(mf.next_flow <= mf.max_flows
	       and (
		  mf.name[mf.next_flow] == -1 or
		  mf.last_update[mf.next_flow] == cpg_round)) do
	 mf.next_flow = mf.next_flow+1
      end
      if mf.next_flow <= mf.max_flows then
	 -- assert(mf.next_flow >= 1)
	 -- assert(mf.name[mf.next_flow] ~= -1)
	 -- assert(mf.name[mf.next_flow]~= nil)
	 -- assert(mf.last_update[mf.next_flow] < cpg_round)

	 -- fill in demand, stable
	 local pkt = bufs2[i]:getIP4Packet()
	 pkt.eth:setSrc(MAC_FROM_SENDER)
	 pkt.ip4:setSrc(SRC_IP_BASE+mf.name[mf.next_flow])
	 pkt.ip4:setDst(SRC_IP_BASE+FROM_SENDER*256+ip.PROTO_CONTROL_1)
	 pkt.ip4:setProtocol(ip.PROTO_CONTROL_1) 
	 pkt.payload.uint64[0] = (mf.demand[mf.next_flow]*(2^32))
	 pkt.payload.uint8[8] = 0 -- hop
	 pkt.payload.uint8[9] = mf.stable[mf.next_flow] -- stable

	 -- Put Cpg round info
	 pkt.payload.uint32[5] = CPG_ROUND_FOLLOWS
	 pkt.payload.uint32[6] = cpg_round	 
	 pkt.payload.uint32[7] = run_id
	 
	 mf.last_update[mf.next_flow] = cpg_round
	 i = i + 1
      else
	 --	 for j = 1, mf.max_flows do
	    -- if (mf.name[j] ~= -1) then
	    --    assert(mf.last_update[j] == cpg_round)
	    -- end
	 -- end
	 mf.next_flow = 1
	 mf.global_last_update = cpg_round
      end      
   end
   return i-1
end

function fillControlPacketsForRound3(bufs2, mf, cpg_round, run_id)
   local i = 1
   while i < bufs2.size and mf.next_flow <= mf.max_flows and mf.global_last_update < cpg_round do
      -- get next flow whose round 1 packet needs to be sent
      while(mf.next_flow <= mf.max_flows
	       and (mf.last_update[mf.next_flow] == cpg_round or
		    mf.name[mf.next_flow] == -1)) do
	 mf.next_flow = mf.next_flow+1
      end
      
      if mf.next_flow <= mf.max_flows then
	 -- assert(mf.next_flow >= 1)
	 -- assert(mf.name[mf.next_flow] ~= -1)
	 -- assert(mf.name[mf.next_flow]~= nil)
	 -- assert(mf.last_update[mf.next_flow] < cpg_round)

	 -- fill in demand, stable
	 local pkt = bufs2[i]:getIP4Packet()
	 pkt.eth:setSrc(MAC_FROM_SENDER)
	 pkt.ip4:setSrc(SRC_IP_BASE+mf.name[mf.next_flow])
	 pkt.ip4:setDst(SRC_IP_BASE+FROM_SENDER*256+ip.PROTO_CONTROL_3)
	 pkt.ip4:setProtocol(ip.PROTO_CONTROL_3)
	 pkt.payload.uint64[0] = (mf.demand[mf.next_flow]*(2^32))
	 pkt.payload.uint8[8] = mf.stable[mf.next_flow]

	 -- Put Cpg round info
	 pkt.payload.uint32[5] = CPG_ROUND_FOLLOWS
	 pkt.payload.uint32[6] = cpg_round
	 pkt.payload.uint32[7] = run_id

	 mf.last_update[mf.next_flow] = cpg_round
	 i = i + 1
      else
	 -- for j = 1, mf.max_flows do
	 --    if (mf.name[j] ~= -1) then
	 --       assert(mf.last_update[j] == cpg_round)
	 --    end
	 -- end
	 mf.next_flow = 1
	 mf.global_last_update = cpg_round
      end      
   end
   return i-1
end

-- TO TEST
function fillControlPacketsForRound2(bufs2, tf, cpg_round, run_id)
   local i = 1
   while i < bufs2.size and tf.last_update < cpg_round and tf.num_flows > 0 and tf.next_flow <= tf.num_flows do
      -- make a packet for tf.flow_id[tf.next_flow],
      -- and tf.next_hop and inc. next_hop
      local pkt = bufs2[i]:getCpgPacket()
      pkt.eth:setSrc(MAC_FROM_RECEIVER)
      pkt.ip4:setSrc(SRC_IP_BASE+tf.flow_id[tf.next_flow])
      pkt.ip4:setDst(SRC_IP_BASE+FROM_RECEIVER*256+ip.PROTO_CONTROL_2_rvs)
      pkt.ip4:setProtocol(ip.PROTO_CONTROL_2_rvs)
      pkt.cpg:setLinkID(tf.next_hop)
      pkt.cpg:setSumSat(0)
      pkt.cpg:setNumSat(0)
      pkt.cpg:setNumFlows(0)

      -- Put Cpg round info
      local ip_pkt = bufs2[i]:getIP4Packet()
      ip_pkt.payload.uint32[5] = CPG_ROUND_FOLLOWS
      ip_pkt.payload.uint32[6] = cpg_round
      ip_pkt.payload.uint32[7] = run_id

      --log_info("Sending rev in round2")
      --bufs2[i]:dump()
      tf.next_hop = tf.next_hop + 1
      if tf.next_hop == tf.nhops[tf.next_flow] then
	 tf.next_flow = tf.next_flow+1
	 tf.next_hop = 0	 
	 if tf.next_flow > tf.num_flows then
	    tf.last_update = cpg_round
	    if cpg_round%5 == 2 then
	       tf.last_update = cpg_round+1
	    end
	    tf.next_flow = 1
	 end	 	 
      end	       
      i = i + 1
   end
   return i-1
end

-- TO TEST
function fillControlPacketsForRound4(bufs2, tf, cpg_round, run_id)
   local i = 1
   while i < bufs2.size and tf.last_update < cpg_round and tf.num_flows > 0 and tf.next_flow <= tf.num_flows do
      -- make a packet for tf.flow_id[tf.next_flow],
      local pkt = bufs2[i]:getIP4Packet()
      pkt.eth:setSrc(MAC_FROM_RECEIVER)
      pkt.ip4:setSrc(SRC_IP_BASE+tf.flow_id[tf.next_flow])
      pkt.ip4:setDst(SRC_IP_BASE+FROM_RECEIVER*256+ip.PROTO_CONTROL_4)
      pkt.ip4:setProtocol(ip.PROTO_CONTROL_4)
      pkt.payload.uint8[0] = 0

      -- Put Cpg round info
      pkt.payload.uint32[5] = CPG_ROUND_FOLLOWS
      pkt.payload.uint32[6] = cpg_round
      pkt.payload.uint32[7] = run_id

      tf.next_flow = tf.next_flow+1
      if tf.next_flow > tf.num_flows then
	 tf.last_update = cpg_round
	 tf.next_flow = 1
      end
      i = i + 1
   end
   return i-1
end

function txTask(workload_file, start_time, round_info, shared_demands,,  shared_gaps, shared_stable,
		 rxQueue1, rxQueue2, queue, queue1, queue2, dstMac, dstMacNo, myMacNo, myName, run_id)
   log_info("txTask myName " .. myName .. " run_id " .. run_id)
   local lm_time_start = lm.getTime()
   local mt_start = getMonotonicTime()
   local offset_start = lm_time_start-mt_start
   log_info("txTask " .. myName .. " current clock time " .. mt_start .. " seconds")
   log_info("txTask " .. myName .. " current+1 clock time " .. lm_time_start .. " seconds")
   log_info("txTask " .. myName .. " offset " .. offset_start .. " seconds")

   
   local mempool = memory.createMemPool(function(buf)
	 buf:getIP4Packet():fill{
	    -- fields not explicitly set here are initialized to reasonable defaults
	    ethSrc = MAC_FROM_SENDER, -- MAC of the tx device
	    ethDst = dstMac,
	    ip4Src = SRC_IP,
	    ip4Dst = (SRC_IP_BASE+FROM_SENDER*256+ip.PROTO_DATA),
	    pktLength = PKT_LEN,
	    ipProtocol = ip.PROTO_DATA
				}	 
   end)

   local mempool2 = memory.createMemPool(function(buf)
	 buf:getIP4Packet():fill{
	    -- fields not explicitly set here are initialized to reasonable defaults
	    ethSrc = MAC_FROM_SENDER, -- MAC of the tx device
	    ethDst = dstMac,
	    ip4Src = SRC_IP,
	    ip4Dst = DST_IP,
	    pktLength = PKT_LEN
				}
   end)

   local bufs = mempool:bufArray(BATCH_SIZE)
   local bufs2 = mempool2:bufArray(BATCH_SIZE2)
   local rxBufs = memory.bufArray()
   local fwdBufs = memory.bufArray()

   -- Workload info, by flow index (name, size in packet,
   --  start time in us). Load from workload file.
   local wl = loadWorkload(workload_file)
   if wl.nflows > 0 then
      log_info("TxSlave on dev " .. queue.id .. ", queue " .. queue.qid .. " loaded workload with " .. wl.nflows .. " flows")
   end
   -- Set up timing wheel one slot = one packet
   local tw = setupTimingWheel(512, {})
   -- Per flow state for active flows that start here
   local mf = setupMyFlowState(wl, tw,
				     {["gap"]= INIT_GAP,
					["rate"] = INIT_RATE,
					["nsent"] = 0,
					["name"] = -1,
					["size"] = 0,
					["demand"] = INIT_DEMAND,
					["stable"] = 0,
					["last_update"] = 0})
   
   local cpg_round = 0
   local cpg_type = -1
   local cpg_left = ROUND
   local prev_cpg_round = -100
   local started = false

   -- for logging configured rates per Cpg round
   local num_sends = 0
   local flow_start = {}
   local flow_finish = {}
   local num_my_packets = {}
   local num_their_packets = {}
   local num_unknown_packets = {}
   local num_over = 0

   local incorrect_round = {}
   local correct_round = {}
   local max_async = {}

   local tmp1, tmp2 = get_round_from_task(round_info, cpg_round)
   while (tmp1 < 180) do
      tmp1, tmp2 = get_round_from_task(round_info, cpg_round)
   end

   (require "jit.p").start("al")   
   while lm.running and num_over < wl.nflows
   do

      local tmp1, tmp2 = get_round_from_task(round_info, cpg_round)
      while (tmp1 < 200) do
	 tmp1, tmp2 = get_round_from_task(round_info, cpg_round)
      end
      if tmp1 > cpg_round then
	 if cpg_round == 0 then
	    log:info(myName .. ": Cpg round start at " .. tmp1)
	 elseif tmp1 > cpg_round + 1 then
	    log:error( myName .. ": Cpg round jumped from "
			 .. cpg_round .. " to "
			 .. tmp1 .. " in one iteration.")
	 end
      	 cpg_round = tmp1
	 cpg_type = tmp2
      else
	 --log:info(myName .. " got round from task " .. tmp1 .. " and type " .. cpg_type)
      end

      local send_control_in_this_batch =  fa(cpg_type == 1 or cpg_type==7) and (mf.global_last_update < cpg_round)

      -- Sending data packets
      if send_control_in_this_batch then
	 bufs2:alloc(PKT_LEN)
      end
      
      bufs:alloc(PKT_LEN)
      local num_bufs2 = 0
      local num_bufs1 = 0
      while num_bufs1 < bufs.size do
	 local next_flow = tw.slots[tw.current_slot+1]
	 if (next_flow == 0) then
	    local pkt = bufs[num_bufs1+1]:getIP4Packet()
	    -- Drop void packets
	    pkt.eth:setDst(dstMacNo)
	    pkt.eth:setSrc(dstMacNo)
	    pkt.ip4:setSrc(SRC_IP_BASE)
	    pkt.ip4:setProtocol(555)
	    num_bufs1 = num_bufs1 + 1
	 else
	    local pkt = nil
	    local send_control = send_control_in_this_batch and num_bufs2 < bufs2.size and (cpg_type==1 or mf.stable[next_flow]==0) and mf.last_update[next_flow] < cpg_round

	    -- Get the if we need to send control packet
	    if send_control == false then
	       pkt = bufs[num_bufs1+1]:getIP4Packet()
	       pkt.ip4:setDst(SRC_IP_BASE+FROM_SENDER*256+ip.PROTO_DATA)
	       pkt.ip4:setProtocol(ip.PROTO_DATA)
	       -- Clear Cpg round info
	       pkt.payload.uint32[5] = 0
	       -- pkt.payload.uint32[6] = 0
	       -- pkt.payload.uint32[7] = 0
	       num_bufs1 = num_bufs1 + 1
	    else -- only if send_control is true and we have room
	       mf.num_flows_up_to_date = mf.num_flows_up_to_date + 1
	       if (mf.num_flows_up_to_date == mf.max_flows) then
		  mf.global_last_update = cpg_round
		  mf.num_flows_up_to_date = 0
	       end
	       pkt = bufs2[num_bufs2+1]:getIP4Packet()
	       num_bufs2 = num_bufs2 + 1
	       pkt.payload.uint64[0] = (mf.demand[next_flow]*(2^32))
	       -- Put Cpg round info
	       pkt.payload.uint32[5] = CPG_ROUND_FOLLOWS
	       pkt.payload.uint32[6] = cpg_round
	       pkt.payload.uint32[7] = run_id
	       mf.last_update[next_flow] = cpg_round
	       -- Put round 1 or 3 specific info
	       if (cpg_type == 1) then
		  pkt.payload.uint8[8] = 0 -- hop
		  pkt.payload.uint8[9] = mf.stable[next_flow]
		  -- log_info("txTask " .. myName .. " adding control info "
		  -- 	      .. " of Type 1 to data packets in round " .. cpg_round
		  -- 	      .. " demand is (64b) " .. tonumber(pkt.payload.uint64[0])
		  -- 	      .. " hop is (8b) " .. tonumber(pkt.payload.uint8[8])
		  -- 	      .. " stable is (8b) " .. tonumber(pkt.payload.uint8[9]))
		  pkt.ip4:setDst(SRC_IP_BASE+FROM_SENDER*256+ip.PROTO_CONTROL_1)
		  pkt.ip4:setProtocol(ip.PROTO_CONTROL_1)
	       elseif (cpg_type == 7) then
		  pkt.payload.uint8[8] = mf.stable[next_flow]
		  -- log_info("txTask " .. myName .. " adding control info "
		  -- 	      .. " of Type 3 to data packets in round " .. cpg_round
		  -- 	      .. " demand is (64b) " .. tonumber(pkt.payload.uint64[0])
		  -- 	      .. " stable is (8b) " .. tonumber(pkt.payload.uint8[8]))
		  pkt.ip4:setDst(SRC_IP_BASE+FROM_SENDER*256+ip.PROTO_CONTROL_3)
		  pkt.ip4:setProtocol(ip.PROTO_CONTROL_3)
	       end
	    end -- piggyback control or no
	    
	    -- Fill in data packet
	    local name = mf.name[next_flow]
	    local num_sent = mf.nsent[next_flow] + 1
	    local size = mf.size[next_flow]
	    -- important to update source MAC
	       -- else will use value in buffer (garbage/ old)
	    -- and possibly drop packet, not clear if it's set to default values on alloc
	    pkt.eth:setDst(dstMacNo)
	    pkt.eth:setSrc(MAC_FROM_SENDER)
	    pkt.ip4:setSrc(SRC_IP_BASE + name)
	    mf.nsent[next_flow] = num_sent --mf.nsent[next_flow] + 1 --num_sent
	    --log_info("We've sent " .. mf.nsent[next_flow] .. " packets of flow " .. mf.name[next_flow])
	    if (num_sent == 1) then
	       flow_start[name] = lm.getTime()
	    end
	    
	    -- Update timing wheel O(1)
	    tw.slots[tw.current_slot+1] = 0
	    if (mf.nsent[next_flow] < mf.size[next_flow]) then	       
	       local next_index = get_next_free(tw, mf.gap[next_flow])		  
	       tw.slots[next_index+1] = next_flow
	    else -- if flow ended
	       num_over = num_over + 1
	       flow_finish[name] = lm.getTime()
	       tw.num_free = tw.num_free+1
	       log_info("Finished sending flow " .. mf.name[next_flow] .. " in round " .. cpg_round)
	    end 	       
	 end -- send data or void
	 -- update current tick per packet
	 tick(tw)
      end -- for i, buf
      -- send out all packets and frees old bufs that have been sent
      if num_bufs1 > 0 then
	 queue:sendN(bufs, num_bufs1)
	 for i=num_bufs1, bufs.size do
	    bufs[i]:free()
	 end
      end
      if num_bufs2 > 0 then
	 queue1:sendN(bufs2, num_bufs2)
	 for i=num_bufs2, bufs2.size do
	    bufs2[i]:free()
	 end
      end

      -- recv control packets for ROUND 4
      if (cpg_type >= 9) then
	 local count = rxQueue1:tryRecv(rxBufs, 0)	    
	 if count >= 1 then
	    log_info("txTask " .. myName
			.. " received " .. (count or 0) .. " control packets of type 0 in round " .. cpg_round)
	 end
	 for i = 1, count do
	    local buf = rxBufs[i]
	    local pkt = buf:getIP4Packet()
	    local src_ip = pkt.ip4:getSrc()
	    local flow_id = src_ip - SRC_IP_BASE
	    
	    --update_sync_stats(buf, run_id, cpg_round, correct_round, incorrect_round, max_async,
	    --		      num_their_packets, num_my_packets, num_unknown_packets, myName)
	    
	    local stable = pkt.payload.uint8[0]
	    mf.stable[mf.flow_index[flow_id]] = stable
	    log_info("txTask " .. myName ..
			" Received control packet of Type 4 in round "
			.. cpg_round .. " with stable (8b) " .. stable)
	    --if stable > 0 then
	    --   log_info("Flow " .. flow_id .. " just got stable")
	    --end
	    buf:free()   
	 end
      elseif cpg_type >= 3 and cpg_type <= 5 then	    -- RECEIVE CONTROL_2_rvs and echo
	 local count = rxQueue2:tryRecv(fwdBufs, 0)
	 if count >= 1  then
	    log_info("txTask " .. myName .. " received "
			.. (count or 0) .. " Type 2 control packets in round " .. cpg_round)
	 end
	 --log_info("tx slave " .. myName .. "  got " .. count .. " packets on rx queue 2")
	 for i = 1, count do
	    local buf = fwdBufs[i]
	    local pkt = buf:getCpgPacket()
	    local ip_pkt = buf:getIP4Packet()
	    local pkt_run_id = ip_pkt.payload.uint32[7]
	    if (pkt_run_id ~= run_id) then
	       log:error("Dropping packet at txTask " .. myName .. " with run id " .. pkt_run_id)
	       pkt.eth:setSrc(dstMacNo)
	       pkt.eth:setDst(dstMacNo)
	    else
	       --buf:dump()
	       update_sync_stats(buf, run_id, cpg_round, correct_round, incorrect_round, max_async,
				 num_their_packets, num_my_packets, num_unknown_packets, task)

	       pkt.eth:setDst(dstMacNo)		
	       local flowId = pkt.ip4:getSrc() - SRC_IP_BASE
	       local proto = pkt.ip4:getProtocol()		     
	       pkt.eth:setSrc(MAC_FROM_SENDER)
	       pkt.ip4:setDst(SRC_IP_BASE+FROM_SENDER*256+ip.PROTO_CONTROL_2_fwd)
	       pkt.ip4:setProtocol(ip.PROTO_CONTROL_2_fwd)		     
	       local rate = LINK_CAPACITY
	       local numFlows = pkt.cpg:getNumFlows()
	       --log_info("numFlows " .. numFlows)
	       local numSat = pkt.cpg:getNumSat()
	       --log_info("numSat " .. numSat)
	       local sumSat =  pkt.cpg:getSumSat()
	       local numUnsat = numFlows - numSat
	       --log_info("sumSat " .. sumSat)
	       if numUnsat > 0 then
		  rate = (LINK_CAPACITY - sumSat)/numUnsat
	       end
	       pkt.cpg:setSumSat(rate)
	       local flow_index = mf.flow_index[flowId]
	       -- update state
	       if (rate < mf.demand[flow_index]) then
		  mf.demand[flow_index] = rate
		  log_info("Demand of flow " .. flowId .. " updated to rate " .. rate .. " Mb/s")
	       end
	       log_info("calculated rate " .. rate
			   .. " from (" .. LINK_CAPACITY .. " - "
			   .. sumSat .. ") / " .. numUnsat ..
			" in cpg round " .. cpg_round .. " for link "
			   .. pkt.cpg:getLinkID())

	    end -- if run_id
	 end -- for i=1, count
	 if count > 0 then
	    queue2:sendN(fwdBufs, count)
	 end
      end  -- checking cpg_type
      num_sends = num_sends + 1      
   end
   (require "jit.p").stop()
   
   log_info("txTask " .. myName .. " finished in round " .. cpg_round)
   log_info("txTask " .. myName .. ": Flow stats")
   for i, name in ipairs(wl.name) do
      size = wl.size[i] or 0
      nsent = wl.nsent[name] or 0
      start = flow_start[name] or 0
      finish = flow_finish[name] or 0
      fct = ((8*nsent*PKT_LEN)/(finish-start))/1e6
      log_info("txTask " .. myName .. ": Flow # " .. i.. ": " .. name .. " 'sent' " .. nsent .. " packet with fct " .. fct .. " Mb/s "
		  .. " from " .. start*1e6 .. " us to " .. finish*1e6 .. " us")
   end

   show_sync_stats(incorrect_round, correct_round, max_async, sample_out_sync,
		   num_my_packets, num_their_packets, num_unknown_packets,
		   myName)

   local lm_time_end = lm.getTime()
   local mt_end = getMonotonicTime()
   local offset_end = lm_time_end-mt_end
   log_info("txTask " .. myName .. " current clock time " .. mt_end .. " seconds")
   log_info("txTask " .. myName .. " TSC clock time " .. lm_time_end .. " seconds")
   log_info("txTask " .. myName .. " offset " .. offset_end .. " seconds")
   log_info("txTask " .. myName .. " offset changed by " .. (offset_end - offset_start)*1e9
	       .. " ns over " .. (mt_end-mt_start) .. " s of system clock.")

   if wl.nflows > 0 then
      lm.sleepMillisIdle(10)
      lm.stop()
   end
   
   
   end

function txData(workload_file, shared_gaps, queue,  dstMac, dstMacNo, myMacNo, myName, run_id)
   log_info("txTask myName " .. myName .. " run_id " .. run_id)
   local lm_time_start = lm.getTime()
   local mt_start = getMonotonicTime()
   local offset_start = lm_time_start-mt_start
   log_info("txTask " .. myName .. " current clock time " .. mt_start .. " seconds")
   log_info("txTask " .. myName .. " current+1 clock time " .. lm_time_start .. " seconds")
   log_info("txTask " .. myName .. " offset " .. offset_start .. " seconds")
   
   local mempool = memory.createMemPool(function(buf)
	 buf:getIP4Packet():fill{
	    -- fields not explicitly set here are initialized to reasonable defaults
	    ethSrc = MAC_FROM_SENDER, -- MAC of the tx device
	    ethDst = dstMac,
	    ip4Src = SRC_IP,
	    ip4Dst = (SRC_IP_BASE+FROM_SENDER*256+ip.PROTO_DATA),
	    pktLength = PKT_LEN,
	    ipProtocol = ip.PROTO_DATA
				}	 
   end)
   local flow_start = {}
   local flow_count = {}
   local flow_end = {}
   
   local bufs = mempool:bufArray(BATCH_SIZE)
   -- Set up timing wheel one slot = one packet
   local tw = setupTimingWheel(512, {})
   -- Per flow state for active flows that start here
   -- TODO: use shared gaps instead of mf.gap
   local mf = setupMyFlowState(wl, tw,
				     {["gap"]= INIT_GAP,
					["rate"] = INIT_RATE,
					["nsent"] = 0,
					["name"] = -1,
					["size"] = 0,
					["demand"] = INIT_DEMAND,
					["stable"] = 0,
					["last_update"] = 0})
   
   (require "jit.p").start("al")   
   while lm.running and num_over < wl.nflows
   do      
      bufs:alloc(PKT_LEN)
      local num_bufs1 = 0
      while num_bufs1 < bufs.size do
	 local next_flow = tw.slots[tw.current_slot+1]
	 if (next_flow == 0) then
	    local pkt = bufs[num_bufs1+1]:getIP4Packet()
	    -- Drop void packets
	    pkt.eth:setDst(dstMacNo)
	    pkt.eth:setSrc(dstMacNo)
	    pkt.ip4:setSrc(SRC_IP_BASE)
	    pkt.ip4:setProtocol(555)
	    num_bufs1 = num_bufs1 + 1
	 else

	    local pkt = bufs[num_bufs1+1]:getIP4Packet()
	    pkt.ip4:setDst(SRC_IP_BASE+FROM_SENDER*256+ip.PROTO_DATA)
	    pkt.ip4:setProtocol(ip.PROTO_DATA)
	    -- Clear Cpg round info
	    pkt.payload.uint32[5] = 0
	    -- pkt.payload.uint32[6] = 0
	    -- pkt.payload.uint32[7] = 0
	    num_bufs1 = num_bufs1 + 1
	    
	    -- Fill in data packet
	    local name = mf.name[next_flow]
	    local num_sent = mf.nsent[next_flow] + 1
	    local size = mf.size[next_flow]
	    -- important to update source MAC
	       -- else will use value in buffer (garbage/ old)
	    -- and possibly drop packet, not clear if it's set to default values on alloc
	    pkt.eth:setDst(dstMacNo)
	    pkt.eth:setSrc(MAC_FROM_SENDER)
	    pkt.ip4:setSrc(SRC_IP_BASE + name)
	    mf.nsent[next_flow] = num_sent --mf.nsent[next_flow] + 1 --num_sent
	    --log_info("We've sent " .. mf.nsent[next_flow] .. " packets of flow " .. mf.name[next_flow])
	    if (num_sent == 1) then
	       flow_start[name] = lm.getTime()
	    end
	    
	    -- Update timing wheel O(1)
	    tw.slots[tw.current_slot+1] = 0
	    if (mf.nsent[next_flow] < mf.size[next_flow]) then	       
	       local next_index = get_next_free(tw, mf.gap[next_flow])		  
	       tw.slots[next_index+1] = next_flow
	    else -- if flow ended
	       num_over = num_over + 1
	       flow_finish[name] = lm.getTime()
	       tw.num_free = tw.num_free+1
	       log_info("Finished sending flow " .. mf.name[next_flow] .. " in round " .. cpg_round)
	    end 	       
	 end -- send data or void
	 -- update current tick per packet
	 tick(tw)
      end -- for i, buf
      
      -- send out all packets and frees old bufs that have been sent
      queue:sendN(bufs, num_bufs1)
   end  -- ends while lm.running

end

function rxControlProcess(start_time, round_info,
		  rxQueue, queue, dstMac, dstMacNo, myMacNo, myName, run_id)
   assert(rxQueue ~= nil)
   assert(queue ~= nil)
   
   local lm_time_start = lm.getTime()
   local mt_start = getMonotonicTime()
   local offset_start = lm_time_start-mt_start
   log_info(myName .. " run_id " .. run_id)
   log_info(myName .. " current clock time " .. mt_start .. " seconds")
   log_info(myName .. " TSC clock time " .. lm_time_start .. " seconds")
   log_info(myName .. " offset " .. offset_start .. " seconds")

   local mempool = memory.createMemPool(function(buf)
	 buf:getIP4Packet():fill{
	    -- fields not explicitly set here are initialized to reasonable defaults
	    ethSrc = MAC_FROM_RECEIVER, -- MAC of the tx device
	    ethDst = dstMac,
	    ip4Src = SRC_IP,
	    ip4Dst = DST_IP,
	    pktLength = PKT_LEN2
				}
   end)
   local bufs = mempool:bufArray(BATCH_SIZE2)
   local rxBufs = memory.bufArray()

   local tf = setupTheirFlowState()
   
   local cpg_round = 0
   local cpg_type = -1
   local cpg_left = ROUND
   local prev_cpg_round = -100
   local started = false

   local num_my_packets = {}
   local num_their_packets = {}
   local num_unknown_packets = {}


   local sample_out_of_sync = nil
   local incorrect_round = {}
   local correct_round = {}
   local max_async = {}

   while lm.running() do -- check if Ctrl+c was pressed
      local tmp1, tmp2 = get_round_from_task(round_info, cpg_round)
      while (tmp1 < 200) do
	 tmp1, tmp2 = get_round_from_task(round_info, cpg_round)
      end

      if tmp1 > cpg_round then
	 if cpg_round == 0 then
	    log:info(myName .. ": Cpg round start at " .. tmp1)
	 elseif tmp1 > cpg_round + 1 then
	    log:error( myName .. ": Cpg round jumped from "
			 .. cpg_round .. " to "
			 .. tmp1 .. " in one iteration.")
	 end
      	 cpg_round = tmp1
	 cpg_type = tmp2
	 cpg_left = tmp3	 
      	 if (cpg_type == 1) then
	    cpgResetTheirFlowState(tf)
	 end
      end
      
      -- receive control packets only, in round 1 only
      if (cpg_type == 1 or cpg_type == 2) then
	 local count = rxQueue:tryRecv(rxBufs, 0) -- ok to spend up to 5us in round 1, nothing else to do
	 for i = 1, count do
	    local buf = rxBufs[i]
	    local pkt = buf:getIP4Packet()
	    local src_ip = pkt.ip4:getSrc()
	    local flow_id = src_ip - SRC_IP_BASE
	    local hop = pkt.payload.uint8[8]

	    log_info(myName ..
			" received a type 1 control packet in round " .. cpg_round
		     .. " setting up " .. hop .. " hops for flow " .. flow_id)

	    tf.num_flows = tf.num_flows + 1
	    tf.flow_id[tf.num_flows] = flow_id
	    tf.nhops[tf.num_flows] = hop

	    update_sync_stats(buf, run_id, cpg_round, correct_round, incorrect_round, max_async,
				    num_their_packets, num_my_packets, num_unknown_packets, myName)
	    buf:free()
	 end -- for i = 1, count
      elseif (cpg_type == 9 and tf.last_update < cpg_round) then
	 --send control packets
	 bufs:alloc(PKT_LEN2)
	 local count = fillControlPacketsForRound4(bufs, tf, cpg_round, run_id)
	 if count > 0 then
	    queue:sendN(bufs, count)
	    for i=count+1, bufs.size do
	       bufs[i]:free()
	    end
	 end
      elseif (cpg_type == 3 and tf.last_update < cpg_round) then
	 --send control packets
	 bufs:alloc(PKT_LEN2)
	 local count = fillControlPacketsForRound2(bufs, tf, cpg_round, run_id)
	 if count > 0 then
	    queue:sendN(bufs, count)
	    for i=count+1, bufs.size do
	       bufs[i]:free()
	    end
	 end
      end
   end -- ends while lm.running()


   log_info(myName .. " finished in round " .. cpg_round)
   show_sync_stats(incorrect_round, correct_round, max_async, sample_out_sync,
		   num_my_packets, num_their_packets, num_unknown_packets,
		   myName)
   local lm_time_end = lm.getTime()
   local mt_end = getMonotonicTime()
   local offset_end = lm_time_end-mt_end
   log_info(myName .. " current clock time " .. mt_end .. " seconds")
   log_info(myName .. " TSC clock time " .. lm_time_end .. " seconds")
   log_info(myName .. " offset " .. offset_end .. " seconds")
   log_info(myName .. " offset changed by " .. (offset_end - offset_start)*1e9
	       .. " ns over " .. (mt_end-mt_start) .. " s of system clock.")

   lm.stop()
end

-- for data only
function rxData(workload_file, rxQueue,  myName, run_id)

   log_info(myName .. " run_id " .. run_id)
   -- to receive data and control packets
   local rxBufs = memory.bufArray()
   local twl = loadWorkload(workload_file)
   if twl.nflows > 0 then
      log_info(myName .. " on dev " .. rxQueue.id ..
		  ", queue " .. rxQueue.qid ..
		  " loaded workload with " .. twl.nflows .. " flows")
   end

   local num_sends = 0
   local flow_start = {}
   local flow_finish = {}
   local flow_count = {}
   
   local num_my_packets = {}
   local num_their_packets = {}
   local num_unknown_packets = {}

   while lm.running() do -- check if Ctrl+c was pressed
      -- receive data and control packets
      local count = rxQueue:recv(rxBufs)

      for i = 1, count do
	 local buf = rxBufs[i]
	 local pkt = buf:getIP4Packet()
	 local src_ip = pkt.ip4:getSrc()
	 local flow_id = src_ip - SRC_IP_BASE
	 local proto = pkt.ip4:getProtocol()

	 -- update packet counts
	 if pkt.eth:getSrc() == MAC_FROM_SENDER then
	    num_their_packets[proto] = (num_their_packets[proto] or 0) +1
	 elseif pkt.eth:getSrc() == MAC_FROM_RECEIVER then
	    num_my_packets[proto] = (num_my_packets[proto] or 0) + 1
	 else num_unknown_packets[proto] = (num_unknown_packets[proto] or 0) + 1
	 end

	 if flow_start[flow_id] == nil then
	    flow_start[flow_id] = getMonotonicTime()
	    flow_count[flow_id] = 1
	 else
	    flow_count[flow_id] = flow_count[flow_id] + 1
	    flow_finish[flow_id] = getMonotonicTime()
	 end
	 buf:free()
      end -- for i = 1, count
   end -- ends while lm.running()

   log_info(myName .. ": Flow stats")
   for name, start in pairs(flow_start) do
      nsent = flow_count[name] or 0
      finish = flow_finish[name] or 0
      fct = ((8*nsent*PKT_LEN)/(finish-start))/1e6
      log_info(myName ..": Flow " .. name
		  .. ", received " .. nsent .. " packets with fct " .. fct .. " Mb/s"
		  .. " from " ..  (start*1e6) .. " us to " .. (finish*1e6))
   end

   show_sync_stats({}, {}, {}, {},
      num_my_packets, num_their_packets, num_unknown_packets,
      myName)

   lm.stop()
end


function rxControlStats(start_time, round_info, rxQueue, myName, run_id)
   assert(rxQueue ~= nil)
   
   local lm_time_start = lm.getTime()
   local mt_start = getMonotonicTime()
   local offset_start = lm_time_start-mt_start
   log_info(myName .. " run_id " .. run_id)
   log_info(myName .. " current clock time " .. mt_start .. " seconds")
   log_info(myName .. " TSC clock time " .. lm_time_start .. " seconds")
   log_info(myName .. " offset " .. offset_start .. " seconds")

   local rxBufs = memory.bufArray()

   local cpg_round = 0
   local cpg_type = -1
   local cpg_left = ROUND
   local prev_cpg_round = -100
   local started = false

   local num_my_packets = {}
   local num_their_packets = {}
   local num_unknown_packets = {}


   local sample_out_of_sync = nil
   local incorrect_round = {}
   local correct_round = {}
   local max_async = {}

   while lm.running() do -- check if Ctrl+c was pressed
      local tmp1, tmp2 = get_round_from_task(round_info, cpg_round)
      while (tmp1 < 200) do
	 tmp1, tmp2 = get_round_from_task(round_info, cpg_round)
      end
      if tmp1 > cpg_round then
      	 cpg_round = tmp1
	 cpg_type = tmp2
	 cpg_left = tmp3
	 
      end

      local count = rxQueue:tryRecv(rxBufs, 0)
      for i = 1, count do
	 local buf = rxBufs[i]
	 update_sync_stats(buf, run_id, cpg_round, correct_round, incorrect_round, max_async,
			   num_their_packets, num_my_packets, num_unknown_packets, myName)
	 buf:free()
      end
   end

   log_info(myName .. " finished in round " .. cpg_round)
   show_sync_stats(incorrect_round, correct_round, max_async, sample_out_sync,
		   num_my_packets, num_their_packets, num_unknown_packets,
		   myName)
end
