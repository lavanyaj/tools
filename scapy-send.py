import copy
import os
from scapy import *
from scapy.all import *
from subprocess import Popen, PIPE
import errno
from socket import error as socket_error

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("--src", help="srcifname", default="eth2")
parser.add_argument("--dst", help="dstifname", default="nf0")
parser.add_argument("--i", help="run in interactive mode", action="store_true")
parser.add_argument("--pps", type=float, help="packets per second", default=0)
parser.add_argument("--burst", type=float, help="number of packets in burst", default=0) 
# just send one packet by default

args = parser.parse_args()

ifaddr = {"nf0": "02:53:55:4d:45:00"}
"""
Benchmarking scapy

Example 1:
1. Make sure eth2 is connected by cable to nf0 (how?)
2. Set up two ssh tabs - one for sending packets, another to start GUI monitor
3. Set up a filter config to capture packets for "1.2.3.4" and record their timestamps in record_ts.txt, the current filter-l1.cfg has
dst ip 1.2.3.4 and mask 255.255.255.255 so filtered packets should have packet dst ip & mask matches 1.2.3.4 & mask
4. Start GUI monitor, load the filter config / Otherwise rest the statistics, reomve the old timstamp file etc.
cd /home/lavanyaj/OSNT-SUME-live/util/app/gui; sudo python monitor_gui.py;
5. Send rate-limited packets using-
cd /home/lavanyaj/tools; sudo python scapy-send.py --pps 0.2; (aside: use strace to syscalls)
6. Once all packets are sent, 

"""

def send_rate_limited(srcifname, dstifname, pps):
    number = 10

    inter = 1.0/pps
    print("send " + str(number) + " packets with inter packet gap " + str(inter) + " s")
    pkt = Ether(dst=ifaddr[dstifname])/IP(dst="1.2.3.4")
    pkt.show()
    pre = ".".join(pkt[IP].src.split('.')[:3])
    
    #pkts = [pkt] * number
    pkts = []
    for i in range(number):
        assert (i < 255)
        src_ip = pre + "." + str(i)
        tmp = copy.deepcopy(pkt)
        tmp[IP].src = src_ip
        pkts.append(tmp)
    print pkts

    try:
        sendp(pkts, inter=inter, iface=srcifname)
    except socket_error as serr:
        print "socket error! %s" % errno.errorcode[serr.errno]
        
    return

def send_burst(srcifname, dstifname, burst):
    number = int(burst)

    print("send " + str(number) + " packets as burst")
    pkt = Ether(dst=ifaddr[dstifname])/IP(dst="1.2.3.4")
    pkt.show()        
    pre = ".".join(pkt[IP].src.split('.')[:3])
    
    pkts = [pkt] * number
    for i in range(number):
        assert (i < 255)
        src_ip = pre + str(i)
        pkts[i][IP].src = src_ip
    print pkts
    
    try:
        sendp(pkts, iface=srcifname)
    except socket_error as serr:
        print "socket error! %s" % errno.errorcode[serr.errno]        
    return

def send_one_packet(srcifname, dstifname):
    try:
        print "send one packet"
        p = Ether(dst=ifaddr[dstifname])/IP(dst="1.2.3.4")
        sendp(p, iface=srcifname)
        p.show()        

        # sends one packet
        # sent as 00:00:00:00:00:00 > 02:53:55:4d:45:00
        # not received at tcpdump
        # sendp(Ether(dst=ifaddr[dstifname]), iface=srcifname)

        # sends one packet, default IP address is 127.0.0.1 MAC source is 000..
        # sent as 00:00:00:00:00:00 > 02:53:55:4d:45:00, 127.0.0.1 > 127.0.0.1
        # received as  00:00:00:00:00:00 > 02:53:55:4d:45:00, 127.0.0.1 > 127.0.0.1
        # sendp(Ether(dst=ifaddr[dstifname])/IP(), iface=srcifname)

        # sends one packet, default source IP is now 172.24.74.26 and MAC source is now 0c:c4:7a:aa:ab:4a
        # sent as c:c4:7a:aa:ab:4a > 02:53:55:4d:45:00 at tcpdump eth2
        # received as 0c:c4:7a:aa:ab:4a > 02:53:55:4d:45:00, 172.24.74.26 > 1.2.3.4 at tcpdump nf0
        # p = Ether(dst=ifaddr[dstifname])/IP(dst="1.2.3.4")
        # sendp(p, iface=srcifname)
        # p.show()
        
        # sends 4 packets, all recevied at tcpdump nf0
        #sendp(Ether(dst=ifaddr[dstifname])/IP(dst="1.2.3.4",ttl=(1,4)), iface=srcifname)
    except socket_error as serr:
        print "socket error! %s" % errno.errorcode[serr.errno]
    return

def interactive():
    ifname = "eth0"
    while (ifname != "quit"):
        ifname = raw_input("please enter ifname ")
        if ifname != "quit":
            print "send one packet on " + str(ifname)
            try:
                sendp(Ether(dst="02:53:55:4d:45:00")/IP(dst="1.2.3.4",ttl=(1,4)), iface=ifname)
            except socket_error as serr:
                print "socket error! %s" % errno.errorcode[serr.errno]

def main():
    if args.i:
        interactive()
    elif args.pps > 0:
        send_rate_limited(args.src, args.dst, args.pps)
    elif args.burst > 0:
        send_burst(args.src, args.dst, args.burst)
    else:
        send_one_packet(args.src, args.dst)

main()
        
