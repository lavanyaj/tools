from math import ceil
min_rate = 100 #* 1e6
max_rate = 10000 #* 1e6
slot = 1200 #* 1e-9

for slot in [120, 600, 1200]:
     actual = {}
     err = {}
     slots = {}
     for rate in range(min_rate, max_rate):
          # one packet * 1e3 / 1200 = max_rate
          # 1200 ns, max_rate Mb/s
          one_packet = 1200 * max_rate
          num_slots = ceil(((1.0*max_rate)/rate)*(1200/slot))
          slots[rate] = num_slots
          actual[rate] = one_packet/(num_slots*slot)
          err[rate] = (rate-actual[rate])/rate
          #print "%d/%d is %d"%(max_rate, rate, num_slots)
          #print "actual rate is %f"%actual[rate]

     f = open("rl-acc-%d.txt"%slot, "w")

     for rate in range(min_rate, max_rate):
          f.write("%d %d %0.2f %0.2f"%(rate, slots[rate], actual[rate], err[rate]))
          f.write("\n")

     f.close()
