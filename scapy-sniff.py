import os
from scapy import *
from scapy.all import *
from subprocess import Popen, PIPE
import argparse

ifnames = ["eth%d"%num for num in range(4)]

while (True):
    for ifname in ifnames:
        sniff(iface=ifname, prn=lambda x: "%s: %s" % (ifname, x.summary()))
