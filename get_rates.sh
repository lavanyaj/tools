#sudo tcpdump -w dump.pcap -nxxi nf0
NOW=$(date +"%I%M%S")
echo "writing pcap file to lines-$NOW.txt"
tcpdump -nxxr dump.pcap | grep : | grep "a80d" | grep -v ethertype | awk -F":" '{print $2;}' > lines-$NOW.txt
echo "parsing lines-$NOW.txt to lines-$NOW-out.txt"
python parse*py lines-$NOW.txt > lines-$NOW-out.txt
echo "number of records"
wc -l lines-$NOW-out.txt
#echo "sorting records in lines-$NOW-out.txt by timestamp"
#cat lines-$NOW-out.txt  | sort -k1,1 -n > lines-$NOW-out-bytime.txt
echo "getting time-diffs in lines-$NOW-out.txt to diff-$NOW.txt"
# assuming diff is in nanoseconds, packets are 1520 MB with framing, 1 pkt/ ns is 12 160 000 Mb/s 
cat lines-$NOW-out.txt | awk 'BEGIN{prev=0;} {diff=$2-prev; prev=$2; if (diff > 0) {print diff" ns --> "12160000/diff" Mb/s";};}' > diff-$NOW.txt
echo "getting rates only from diff-$NOW.txt to rates-$NOW.txt"
cat diff-$NOW.txt | awk '{print $(NF-1);}' > rates-$NOW.txt
cat diff-$NOW.txt | awk '{print $1;}' > gaps-$NOW.txt
echo "getting summary of rates (Mb/s) in rates-$NOW.txt into rates-summary-$NOW.txt"
cat rates-$NOW.txt | Rscript -e 'summary(as.numeric (readLines ("stdin")))' > rates-summary-$NOW.txt
cat rates-summary-$NOW.txt

echo "getting summary of gaps (Mb/s) in gaps-$NOW.txt into gaps-summary-$NOW.txt"
cat gaps-$NOW.txt | Rscript -e 'summary(as.numeric (readLines ("stdin")))' > gaps-summary-$NOW.txt
cat gaps-summary-$NOW.txt

echo "plot rates and save figure.png"
Rscript plot.R < rates-$NOW.txt
